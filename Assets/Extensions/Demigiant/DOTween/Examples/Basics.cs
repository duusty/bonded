using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Basics : MonoBehaviour
{
	public Transform cubeA;

    public float ShakeDuration = 1;
    public int ShakeVibrato = 10;
    public float ShakeStrenght = 90;
    public float ShakeRandomness = 90;

    public Vector3 endscale = new Vector3(50, 50, 50);
    public float ScaleDuration = 1;

    [ContextMenu("ShakeRotation")]
    void ShakeRotation()
    {
        cubeA.DOShakeRotation(ShakeDuration, ShakeStrenght, ShakeVibrato, ShakeRandomness).SetAutoKill(true);
    }

    [ContextMenu("ShakePosition")]
    void ShakePosition()
    {
        cubeA.DOShakePosition(ShakeDuration, ShakeStrenght, ShakeVibrato, ShakeRandomness).SetAutoKill(true);
    }

    [ContextMenu("DoScale")]
    void DoScale()
    {
        cubeA.DOScale(endscale, ScaleDuration).SetLoops(4,LoopType.Yoyo);
    }
}