﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class ScreenSpaceCloudShadow : MonoBehaviour
{
	[Header("Direction")]
	public bool isTopDown = true;

	[Header("Cloud")]
	public Texture cloudTexture = null;
	public Vector2 cloudTiling = new Vector2(2, 2);
	public Vector2 cloudWindSpeed = new Vector2(0.05f, 0.05f);

	[Header("Shadow")]
	public Color shadowColor = new Color(0f,0.04f,0.23f,1f);
	public float shadowIntensity = 0.3f;

    private Renderer _ren = null;
	private Shader _shader = null;

    void Awake()
    {
        _ren = GetComponent<Renderer>();
		_shader = _ren && _ren.sharedMaterial ? _ren.sharedMaterial.shader : null;

        CheckSupport();
    }

    void CheckSupport()
    {
		if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures || !SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.Depth))
        {
            Debug.LogWarning("ScreenSpaceCloudShadow has been disabled as it's not supported on the current platform.");
            enabled = false;
        }

        _ren.enabled = enabled;

		if (!_shader || !_shader.isSupported)
        {
            Debug.LogWarning("ScreenSpaceCloudShadow has been disabled as it's not support shader.");
            _ren.enabled = false;
            enabled = false;
        }
    }

    void Start()
    {
        OnWillRenderObject();
    }

	void OnValidate()
	{
		Refresh();
	}

    void OnEnable()
    {
		Refresh();

        if (Camera.main)
            Camera.main.depthTextureMode |= DepthTextureMode.Depth;
    }

    void OnDisable()
    {
        if (Camera.main)
            Camera.main.depthTextureMode = DepthTextureMode.None;
    }

	void OnWillRenderObject()
	{
		// Adjust Quad position to Farplane
		Camera cam = Camera.main;
		float dist = cam.farClipPlane - 0.1f; // 0.1 is some magic value to avoid culling
		Vector3 campos = cam.transform.position;
		Vector3 camray = cam.transform.forward * dist;
		Vector3 quadpos = campos + camray;
		transform.position = quadpos;
		
		// Adjust Quad size to Farplane
		float h = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * dist * 2f;
		Vector3 scale = transform.parent ? transform.parent.localScale : Vector3.one;
		transform.localScale = new Vector3(h * cam.aspect / scale.x, h / scale.y, 0f);

		// Render cloud shadow at GameView only
		bool isGameView = Camera.current == null || Camera.current == Camera.main;
		if(isGameView)
		{
			transform.rotation = Quaternion.LookRotation(quadpos - campos, cam.transform.up); // align uv with _depthtexture
			
			float t = Time.time;
			_ren.sharedMaterial.SetVector("_CloudFactor", new Vector4(
				cloudWindSpeed.x * t
				, cloudWindSpeed.y * t
				, cloudTiling.x
				, cloudTiling.y));

			

			if(cam.orthographic)
			{
				_ren.sharedMaterial.EnableKeyword("SSCS_ORTHO");
				_ren.sharedMaterial.SetVector("_WorldSpaceCameraRay", camray);
			}
			else
			{
				_ren.sharedMaterial.DisableKeyword("SSCS_ORTHO");
			}

		}
		// If SceneView, backface cloud shadow does not rendered
        else
		{			
            transform.LookAt(campos);
		}		
    }

	public void Refresh()
	{
		if (_ren)
		{
			_ren.sharedMaterial.SetTexture ("_MainTex", cloudTexture);

			// Pre calculate color multiplier at script (not shader)
			// 1 minus for darken blending
			_ren.sharedMaterial.SetVector("_ShadowFactor", new Vector4(
				(1f - shadowColor.r) * shadowIntensity
				,(1f - shadowColor.g) * shadowIntensity
				,(1f - shadowColor.b) * shadowIntensity
				,1f));

			// If cloud is topdown way, world position.xz converted to cloud uv
			if(isTopDown)
			{
				_ren.sharedMaterial.EnableKeyword("SSCS_TOPDOWN");	
			}
			else
			{
				_ren.sharedMaterial.DisableKeyword("SSCS_TOPDOWN");	
			}
		}
	}
}
