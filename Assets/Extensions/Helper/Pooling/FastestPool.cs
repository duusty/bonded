﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

public interface IFastestPoolable
{
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    void OnPoolGet(); //sceneObject.SetActive(true);
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    void OnPoolStore(); //sceneObject.SetActive(false);

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	void Store(); //toStore the Object
}

//TODO custom inspector which shows stack Count
//maybe for editor an additional list which maintains all objects to show complete max Count
//onPoolRelease can also probably move the objects to a different parent to make the pool count easier visible in the editor
public sealed class FastestPool<T> where T : MonoBehaviour, IFastestPoolable
{
	//TODO maybe make a hashset to be sure we dont store the same object multiple times

	private Stack<T> _objectStack;
	private Func<T> _creationMethod; //here we can also our custom methods :)

	public FastestPool(int initialSize, Func<T> creationMethod, T Identifier = null)
	{
		if(Identifier)
			FastestPoolResolver<T>.TryAddPool(Identifier,this);
		_objectStack = new Stack<T>();
		_creationMethod = creationMethod;
		for (int i = 0; i < initialSize; i++)
			Store (createObject());
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	private T createObject()
	{
		T @object = _creationMethod();
		return @object;
	}
	
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public T Get()
	{
		T @object = null;
		if (_objectStack.Count > 0)
            @object = _objectStack.Pop();
		else
        	@object = createObject();
		@object.OnPoolGet();
		return @object;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public T Get(Vector3 position, Quaternion rotation, Transform parentTransform = null)
	{
		T @object = null;
		if (_objectStack.Count > 0)
            @object = _objectStack.Pop();
		else
        	@object = createObject();
		Transform trans = @object.transform;
		trans.SetParent(parentTransform, false);
		trans.SetPositionAndRotation(position,rotation);
		@object.OnPoolGet();
		return @object;
	}
	
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Store(T @object)
	{
        @object.OnPoolStore();
		_objectStack.Push(@object);
	}
}

public sealed class FastestPoolResolver<T> where T : MonoBehaviour, IFastestPoolable
{
	private static Dictionary<T, FastestPool<T>> poolManager = new Dictionary<T,FastestPool<T>>();

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static FastestPool<T> TryGetPool(T key, Func<T> creationMethod = null)
	{
		FastestPool<T> tempPool = null;
		if(poolManager.TryGetValue(key, out tempPool))
			return tempPool;
		if(creationMethod != null)
		{
			tempPool = new FastestPool<T>(5, creationMethod, null);
			poolManager.Add(key, tempPool);
		}
		return tempPool;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void TryAddPool(T key, FastestPool<T> pool)
	{
		if(!poolManager.ContainsKey(key))
			poolManager.Add(key,pool);
	}
}

public static class FastestPoolHelper
{
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static T FastestInstantiate<T>(this T component, Func<T> creationMethod = null)
		where T : MonoBehaviour, IFastestPoolable
	{
		return FastestPoolResolver<T>.TryGetPool(component,creationMethod).Get();
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static T FastestInstantiate<T>(this T component, Vector3 position, Quaternion rotation, Transform parentTransform = null, Func<T> creationMethod = null)
		where T : MonoBehaviour, IFastestPoolable
	{
		return FastestPoolResolver<T>.TryGetPool(component,creationMethod).Get(position, rotation, parentTransform);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void FastestDestroy<T>(this T component)
		where T : MonoBehaviour, IFastestPoolable
	{
		component.Store();
	}

	private static Dictionary<GameObject,FastestGameObjectPool> poolDictionary = new Dictionary<GameObject,FastestGameObjectPool>();

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static GameObject FastestGameObjectInstantiate(this GameObject go, Vector3 position, Quaternion rotation, Transform parentTransform = null)
	{
		FastestGameObjectPool pool = null;
		if(!poolDictionary.TryGetValue(go,out pool))
		{
			pool = new FastestGameObjectPool(5,go);
			poolDictionary.Add(go,pool);
		}
		return pool.Get(position,rotation,parentTransform);
	}
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static GameObject FastestGameObjectInstantiate(this GameObject go)
	{
		FastestGameObjectPool pool = null;
		if(!poolDictionary.TryGetValue(go,out pool))
		{
			pool = new FastestGameObjectPool(5,go);
			poolDictionary.Add(go,pool);
		}
		return pool.Get();
	}
}

public class FastestGameObjectPool
{
	private Stack<GameObject> _objectStack;
	private GameObject _base;

	public FastestGameObjectPool(int initialSize, GameObject @base)
	{
		_base = @base;
		_objectStack = new Stack<GameObject>();
		for (int i = 0; i < initialSize; i++)
			Store (GameObject.Instantiate(_base));
	}

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public GameObject Get()
	{
		if (_objectStack.Count > 0)
            return _objectStack.Pop();
        return GameObject.Instantiate(_base);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public GameObject Get(Vector3 position, Quaternion rotation, Transform parentTransform = null)
	{
		GameObject @object = null;
		if (_objectStack.Count > 0)
		{
            @object = _objectStack.Pop();
			Transform trans = @object.transform;
			trans.SetParent(parentTransform, false);
			trans.SetPositionAndRotation(position,rotation);
		}
		else
        	@object = GameObject.Instantiate(_base,position,rotation,parentTransform);
		return @object;
	}
	
	
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Store(GameObject @object)
	{
		_objectStack.Push(@object);
	}
}


public sealed class FastestMonobehaviorLessDefinitionPoolResolver<T> where T : class, IFastestPoolable
{
	private static Dictionary<Definition, FastestMonobehaviorLessPool<T>> poolManager = new Dictionary<Definition,FastestMonobehaviorLessPool<T>>(DefinitionEqualityComparer.instance);

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static FastestMonobehaviorLessPool<T> TryGetPool(Definition key, Func<T> creationMethod = null)
	{
		FastestMonobehaviorLessPool<T> tempPool = null;
		if(poolManager.TryGetValue(key, out tempPool))
			return tempPool;
		if(creationMethod != null)
		{
			tempPool = new FastestMonobehaviorLessPool<T>(5, creationMethod, null);
			poolManager.Add(key, tempPool);
		}
		return tempPool;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void TryAddPool(Definition key, FastestMonobehaviorLessPool<T> pool)
	{
		if(!poolManager.ContainsKey(key))
			poolManager.Add(key,pool);
	}
}


//TODO custom inspector which shows stack Count
//maybe for editor an additional list which maintains all objects to show complete max Count
//onPoolRelease can also probably move the objects to a different parent to make the pool count easier visible in the editor
public sealed class FastestMonobehaviorLessPool<T> where T : class, IFastestPoolable
{
	//TODO maybe make a hashset to be sure we dont store the same object multiple times

	private Stack<T> _objectStack;
	private Func<T> _creationMethod; //here we can also our custom methods :)

	public FastestMonobehaviorLessPool(int initialSize, Func<T> creationMethod, Definition Identifier)
	{
		if(Identifier)
			FastestMonobehaviorLessDefinitionPoolResolver<T>.TryAddPool(Identifier,this);
		_objectStack = new Stack<T>();
		_creationMethod = creationMethod;
		for (int i = 0; i < initialSize; i++)
			Store (createObject());
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	private T createObject()
	{
		T @object = _creationMethod();
		return @object;
	}
	
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public T Get()
	{
		T @object = null;
		if (_objectStack.Count > 0)
            @object = _objectStack.Pop();
		else
        	@object = createObject();
		@object.OnPoolGet();
		return @object;
	}
	
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Store(T @object)
	{
        @object.OnPoolStore();
		_objectStack.Push(@object);
	}
}