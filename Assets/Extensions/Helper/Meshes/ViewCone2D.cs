﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class ViewCone2D : MonoBehaviour
{
    [Range(3, 30)]
    public int Quality = 10;

    [Range(0,180)]
    public float angle_fov = 40;

    public float minDistance = 2;
    public float maxDistance = 3;

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    private Mesh mesh;

    public int SortingOrder = 0;
    public string SortingLayerName = "UI";

    
    void Start()
    {
        mesh = new Mesh();
        mesh.MarkDynamic();
        mesh.vertices = new Vector3[4 * Quality];   // Could be of size [2 * quality + 2] if circle segment is continuous
        mesh.triangles = new int[3 * 2 * Quality];
        meshFilter.sharedMesh = mesh;

        Vector2[] uv = new Vector2[4 * Quality];

        for (int i = 0; i < uv.Length; i++)
            uv[i] = new Vector2(0, 0);

        mesh.uv = uv;
        Recalc();
    }

    [ContextMenu("Recalc")]
    public void Recalc()
    {
        if (meshRenderer != null)
        {
            meshRenderer.sortingOrder = SortingOrder;
            meshRenderer.sortingLayerName = SortingLayerName;
        }

        float angle_lookat = 90;

        float angle_start = angle_lookat - angle_fov;
        float angle_end = angle_lookat + angle_fov;
        float angle_delta = (angle_end - angle_start) / Quality;

        float angle_curr = angle_start;
        float angle_next = angle_start + angle_delta;

        Vector3 pos_curr_min = Vector3.zero;
        Vector3 pos_curr_max = Vector3.zero;

        Vector3 pos_next_min = Vector3.zero;
        Vector3 pos_next_max = Vector3.zero;

        Vector2[] uv = new Vector2[4 * Quality];
        Vector3[] vertices = new Vector3[4 * Quality];   // Could be of size [2 * quality + 2] if circle segment is continuous
        int[] triangles = new int[3 * 2 * Quality];

        for (int i = 0; i < Quality; i++)
        {
            Vector3 sphere_curr = new Vector3(
            Mathf.Sin(Mathf.Deg2Rad * (angle_curr)), Mathf.Cos(Mathf.Deg2Rad * (angle_curr)),   // Left handed CW
            0);

            Vector3 sphere_next = new Vector3(
            Mathf.Sin(Mathf.Deg2Rad * (angle_next)), Mathf.Cos(Mathf.Deg2Rad * (angle_next)),
            0);

            pos_curr_min = Vector3.zero;
            pos_curr_max = sphere_curr * maxDistance;

            pos_next_min = Vector3.zero;
            pos_next_max = sphere_next * maxDistance;

            int a = 4 * i;
            int b = 4 * i + 1;
            int c = 4 * i + 2;
            int d = 4 * i + 3;


            uv[a] = new Vector2(minDistance / maxDistance, 1);
            uv[b] = new Vector2(minDistance / maxDistance, 0);
            uv[d] = new Vector2(minDistance / maxDistance, 1);
            uv[c] = new Vector2(minDistance / maxDistance, 0);

            if (a == 0)
                uv[b] *= 1.3f;
            else if (a == ((Quality - 1) * 4))
                uv[c] *= 1.3f;


            vertices[a] = pos_curr_min;
            vertices[b] = pos_curr_max;
            vertices[c] = pos_next_max;
            vertices[d] = pos_next_min;

            triangles[6 * i] = a;       // Triangle1: abc
            triangles[6 * i + 1] = b;
            triangles[6 * i + 2] = c;
            triangles[6 * i + 3] = c;   // Triangle2: cda
            triangles[6 * i + 4] = d;
            triangles[6 * i + 5] = a;

            angle_curr += angle_delta;
            angle_next += angle_delta;

        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateBounds();
    }
}
/*
public class FOV2DVisionCone : MonoBehaviour
{
    public enum Status
    {
        Idle,
        Suspicious,
        Alert
    }
    public enum Direction
    {
        Left,
        Right
    }
    public Status status;
    public Direction direction;
    public List<Material> materials;

    Vector3[] newVertices;
    Vector2[] newUV;
    int[] newTriangles;
    Mesh mesh;
    MeshRenderer meshRenderer;
    FOV2DEyes eyes;
    List<RaycastHit> hits;
    List<RaycastHit2D> hits2D; // mine
    int i;
    int v;

    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        meshRenderer = GetComponent<MeshRenderer>();
        eyes = gameObject.GetComponent<FOV2DEyes>();

        meshRenderer.material = materials[0];
    }

    void LateUpdate()
    {
        //UpdateMesh();
        UpdateMesh2D();

        UpdateMeshMaterial();

        switch (direction)
        {
            case Direction.Left:
                transform.rotation = Quaternion.Euler(new Vector3(0, -90, 90));
                break;

            case Direction.Right:
                transform.rotation = Quaternion.Euler(new Vector3(0, 90, -90));
                break;
        }
    }

    void UpdateMesh()
    {
        hits = eyes.hits;

        if (hits == null || hits.Count == 0)
            return;

        if (mesh.vertices.Length != hits.Count + 1)
        {
            mesh.Clear();
            newVertices = new Vector3[hits.Count + 1];
            newTriangles = new int[(hits.Count - 1) * 3];

            i = 0;
            v = 1;
            while (i < newTriangles.Length)
            {
                if ((i % 3) == 0)
                {
                    newTriangles[i] = 0;
                    newTriangles[i + 1] = v;
                    newTriangles[i + 2] = v + 1;
                    v++;
                }
                i++;
            }
        }

        newVertices[0] = Vector3.zero;
        for (i = 1; i <= hits.Count; i++)
        {
            newVertices[i] = transform.InverseTransformPoint(hits[i - 1].point);
        }

        newUV = new Vector2[newVertices.Length];
        i = 0;
        while (i < newUV.Length)
        {
            newUV[i] = new Vector2(newVertices[i].x, newVertices[i].z);
            i++;
        }

        mesh.vertices = newVertices;
        mesh.triangles = newTriangles;
        mesh.uv = newUV;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    void UpdateMesh2D()
    {
        hits2D = eyes.hits2D;

        if (hits2D == null || hits2D.Count == 0)
            return;

        if (mesh.vertices.Length != hits2D.Count + 1)
        {
            mesh.Clear();
            newVertices = new Vector3[hits2D.Count + 1];
            newTriangles = new int[(hits2D.Count - 1) * 3];

            i = 0;
            v = 1;
            while (i < newTriangles.Length)
            {
                if ((i % 3) == 0)
                {
                    newTriangles[i] = 0;
                    newTriangles[i + 1] = v;
                    newTriangles[i + 2] = v + 1;
                    v++;
                }
                i++;
            }
        }

        newVertices[0] = Vector3.zero;
        for (i = 1; i <= hits2D.Count; i++)
        {
            newVertices[i] = transform.InverseTransformPoint(hits2D[i - 1].point);
        }

        newUV = new Vector2[newVertices.Length];
        i = 0;
        while (i < newUV.Length)
        {
            newUV[i] = new Vector2(newVertices[i].x, newVertices[i].z);
            i++;
        }

        mesh.vertices = newVertices;
        mesh.triangles = newTriangles;
        mesh.uv = newUV;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    void UpdateMeshMaterial()
    {
        for (i = 0; i < materials.Count; i++)
        {
            if (i == (int)status && meshRenderer.material != materials[i])
            {
                meshRenderer.material = materials[i];
            }
        }
    }
}*/

