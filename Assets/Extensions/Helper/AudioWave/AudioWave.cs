using UnityEngine;

public class AudioWave : MonoBehaviour {
	
	private float[] samples;
	
	public Material material;
	
	public int offset = 5000;
	public int skipn  = 1;

	public int instanceCount = 1000;
	public int listCount = 10000000;
	
	private ComputeBuffer bufferPoints;
	private ComputeBuffer bufferPos;
	private ComputeBuffer bufferlist;
	private Vector3[] origPos;
	private Vector3[] pos;
	private float[] list;
	
	
	private void ReleaseBuffers() {
		if (bufferPoints != null) bufferPoints.Release();
		bufferPoints = null;
		if (bufferPos != null) bufferPos.Release();
		bufferPos = null;
		if (bufferlist != null) bufferlist.Release();
		bufferlist = null;
	}

	void OnDisable() {
		ReleaseBuffers ();
	}

    public void SetAudioData(AudioClip clip, int Offset, int Skip)
    {
        enabled = true;
        float[] samples = new float[clip.samples * clip.channels];
        clip.GetData(samples, 0);

        float[] AveragedSound = new float[samples.Length / 4];
        var j = 0;
        for (int i = 0; i < samples.Length / 4 - 20; ++i)
        {
            j += 4;
            //   if (j >= samples.Length) print(j + " er " + i);
            //   float average = Mathf.Max(Mathf.Abs(samples[j]), Mathf.Abs(samples[j + 1]));
            //   average = Mathf.Max(average, Mathf.Abs(samples[j + 2]));
            //   average = Mathf.Max(average, Mathf.Abs(samples[j + 3]));
            //    AveragedSound[i] = average * 2;
            AveragedSound[i] = samples[i];
        }

        //make verts
        var verts = new Vector3[3];
        for (var i = 0; i < 3; ++i)
        {
            float phi = i * Mathf.PI * 2.0f / (3 - 1);
            verts[i] = new Vector3(0, Mathf.Cos(phi), 0.0f);
        }

        //make complex array
        var pos = new Vector3[instanceCount];
        for (var i = 0; i < instanceCount; ++i)
        {
            pos[i] = new Vector3(
            i * 0.03f,//xdistance
            0,//amplitude
            0
            );
        }

        //make list array
        var list = AveragedSound;
        for (var i = 0; i < AveragedSound.Length; ++i)
        {
            list[i] = AveragedSound[i];
        }

        ReleaseBuffers();

        bufferPoints = new ComputeBuffer(3, 12);
        bufferPoints.SetData(verts);
        material.SetBuffer("buf_Points", bufferPoints);

        bufferPos = new ComputeBuffer(instanceCount, 12);
        bufferPos.SetData(pos);
        material.SetBuffer("buf_Positions", bufferPos);

        bufferlist = new ComputeBuffer(listCount, 12);
        bufferlist.SetData(list);
        material.SetBuffer("buf_list", bufferlist);

        offset = Offset;
        skipn = Skip;
        if (skipn < 1) skipn = 1;

        material.SetInt("offset", offset);
        material.SetInt("skipn", skipn);
    }

    void OnPostRender()
    {
        offset++;

        material.SetInt("offset", offset);

        material.SetPass(0);
        Graphics.DrawProcedural(MeshTopology.LineStrip, 3, instanceCount);
    }
}