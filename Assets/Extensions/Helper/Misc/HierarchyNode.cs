﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HierarchyNode<T> : BaseHierarchyTreeNode<T> where T : class
{
    private string _Path = "";
    private string _Name = "";


    public HierarchyNode(string Name, T item, HierarchyNode<T> parent = null) :
        base(item, parent)
    {
        _Name = Name;

        System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
        HierarchyNode<T> tempParent = parent;
        while(tempParent != null)
        {
            strBuilder.AppendFormat("/{0}", tempParent._Name);
            tempParent = (HierarchyNode<T>)tempParent.getParent;
        }
        strBuilder.AppendFormat("/{0}", _Name);
        _Path = strBuilder.ToString();
    }

}

public class BaseTreeNode
{

}

public interface IBaseTreeNodeData
{
    string getName();
}

[System.Serializable()]
public class BaseHierarchyTreeNode<T> : BaseTreeNode where T : class
{
    private T _item;
    private BaseHierarchyTreeNode<T> _parentNode;
    private BetterList<BaseHierarchyTreeNode<T>> _children;
    private HashSet<T> _childrenSet;

    public BaseHierarchyTreeNode(T item, BaseHierarchyTreeNode<T> parent = null)
    {
        _item = item;
        _parentNode = parent;
    }

    public bool isVirtualNode { get { return _item == null; } }
    public BaseHierarchyTreeNode<T> getParent { get { return _parentNode; } }

    public T Item
    {
        get { return _item; }
        set { _item = value; }
    }

    public void AddChild(T child)
    {
        if (_childrenSet.Add(child))
        {
            _children.Add(new BaseHierarchyTreeNode<T>(child, this));
        }
    }

    public void RemoveChild(T child)
    {
        if (_childrenSet.Remove(child))
        {
            for (int i = 0, iMax = _children.size; i < iMax; i++)
            {
                if (_children[i].Item == child)
                {
                    _children.RemoveAt(i);
                    return;
                }
            }
        }
    }

    public BetterList<BaseHierarchyTreeNode<T>> getChildNodesShallow()
    {
        return _children;
    }

    public BetterList<BaseHierarchyTreeNode<T>> getChildNodesDeep()
    {
        BetterList<BaseHierarchyTreeNode<T>> tempReturnValue = _children;
        for (int i = 0, iMax = _children.size; i < iMax; i++)
        {
            BetterList<BaseHierarchyTreeNode<T>> tempShallowList = _children[i].getChildNodesDeep();
            for(int i2 = 0, iMax2 = tempShallowList.size; i2 < iMax2; i2++)
            {
                tempReturnValue.Add(tempShallowList[i]);
            }
        }
        return tempReturnValue;
    }
}