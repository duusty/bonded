using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

/// <summary>
/// This improved version of the System.Collections.Generic.List that doesn't release the buffer on Clear(),
/// resulting in better performance and less garbage collection.
/// PRO: BetterList performs faster than List when you Add and Remove items (although slower if you remove from the beginning).
/// CON: BetterList performs worse when sorting the list. If your operations involve sorting, use the standard List instead.
/// </summary>

public sealed class BetterList<T>
{
    public BetterList() {}

    public BetterList(int initialSize)
    {
        this.buffer = new T[initialSize];
        this.size = 0;
    }

    public BetterList(T element)
    {
        this.buffer = new T[1];
        this.size = 1;
        this.buffer[0] = element;
    }

    public BetterList(IEnumerable<T> collection)
    {
        ICollection<T> is2 = collection as ICollection<T>;
        if (is2 != null)
        {
            int count = is2.Count;
            this.buffer = new T[count];
            is2.CopyTo(this.buffer, 0);
            this.size = count;
        }
        else
        {
            this.size = 0;
            this.buffer = new T[4];
            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    this.Add(enumerator.Current);
                }
            }
        }
    }

	public int Capacity
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
		get{return buffer.Length;}
	}

	// Direct access to the buffer. Note that you should not use its 'Length' parameter, but instead use BetterList.size.
	public T[] buffer;
	public int size = 0;

	[DebuggerHidden]
	[DebuggerStepThrough]
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public IEnumerator<T> GetEnumerator ()
	{
		if (buffer != null)
		{
			for (int i = 0; i < size; ++i)
			{
				yield return buffer[i];
			}
		}
	}
	
	[DebuggerHidden]
	public T this[int i]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
		get { return buffer[i]; }
		[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
		set { buffer[i] = value; }
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	private void AllocateMore ()
	{
		T[] newList = (buffer != null) ? new T[Mathf.Max(buffer.Length << 1, 32)] : new T[32];
		if (buffer != null && size > 0) buffer.CopyTo(newList, 0);
		buffer = newList;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	private void Trim ()
	{
		if (size > 0)
		{
			if (size < buffer.Length)
			{
				T[] newList = new T[size];
				for (int i = 0; i < size; ++i) newList[i] = buffer[i];
				buffer = newList;
			}
		}
		else buffer = null;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Clear () { size = 0; }

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Release () { size = 0; buffer = null; }

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void Add (T item)
	{
		if (buffer == null || size == buffer.Length)
			AllocateMore();
		buffer[size++] = item;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void AddRange(BetterList<T> list)
    {
        int completeCount = size + list.size;
        if(completeCount > buffer.Length)
            System.Array.Resize(ref buffer, completeCount);
        list.buffer.CopyTo(list.buffer, size);
    }

    public void Insert (int index, T item)
	{
		if (buffer == null || size == buffer.Length)
			AllocateMore();

		if (index > -1 && index < size)
		{
			for (int i = size; i > index; --i) buffer[i] = buffer[i - 1];
			buffer[index] = item;
			++size;
		}
		else Add(item);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public bool Contains (T item)
	{
		if (buffer == null) return false;
		for (int i = 0; i < size; ++i) if (buffer[i].Equals(item)) return true;
		return false;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public int IndexOf (T item)
	{
		if (buffer == null) return -1;
		for (int i = 0; i < size; ++i) if (buffer[i].Equals(item)) return i;
		return -1;
	}

	// Remove the specified item from the list. Note that RemoveAt() is faster and is advisable if you already know the index.
	public bool Remove (T item)
	{
		if (buffer != null)
		{
			EqualityComparer<T> comp = EqualityComparer<T>.Default;

			for (int i = 0; i < size; ++i)
			{
				if (comp.Equals(buffer[i], item))
				{
					--size;
					buffer[i] = default(T);
					for (int b = i; b < size; ++b) buffer[b] = buffer[b + 1];
					buffer[size] = default(T);
					return true;
				}
			}
		}
		return false;
	}

	// Remove an item at the specified index.
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public void RemoveAt (int index)
	{
		if (buffer != null && index > -1 && index < size)
		{
			--size;
			buffer[index] = default(T);
			for (int b = index; b < size; ++b) buffer[b] = buffer[b + 1];
			buffer[size] = default(T);
		}
	}

	// Remove an item from the end.
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public T Pop ()
	{
		if (buffer != null && size != 0)
		{
			T val = buffer[--size];
			buffer[size] = default(T);
			return val;
		}
		return default(T);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public T[] ToArray () { Trim(); return buffer; }

	//class Comparer : System.Collections.IComparer
	//{
	//    public System.Comparison<T> func;
	//    public int Compare (object x, object y) { return func((T)x, (T)y); }
	//}

	//Comparer mComp = new Comparer();

	/// <summary>
	/// List.Sort equivalent. Doing Array.Sort causes GC allocations.
	/// </summary>

	//public void Sort (System.Comparison<T> comparer)
	//{
	//    if (size > 0)
	//    {
	//        mComp.func = comparer;
	//        System.Array.Sort(buffer, 0, size, mComp);
	//    }
	//}

	/// <summary>
	/// List.Sort equivalent. Manual sorting causes no GC allocations.
	/// </summary>

	[DebuggerHidden]
	[DebuggerStepThrough]
	public void Sort (CompareFunc comparer)
	{
		int start = 0;
		int max = size - 1;
		bool changed = true;

		while (changed)
		{
			changed = false;

			for (int i = start; i < max; ++i)
			{
				// Compare the two values
				if (comparer(buffer[i], buffer[i + 1]) > 0)
				{
					// Swap the values
					T temp = buffer[i];
					buffer[i] = buffer[i + 1];
					buffer[i + 1] = temp;
					changed = true;
				}
				else if (!changed)
				{
					// Nothing has changed -- we can start here next time
					start = (i == 0) ? 0 : i - 1;
				}
			}
		}
	}

	// Comparison function should return -1 if left is less than right, 1 if left is greater than right, and 0 if they match.
	public delegate int CompareFunc (T left, T right);
}
