#if !UNITY_EDITOR
// Extra optimizations when not running in the editor, but less error checking
#define ASTAR_OPTIMIZE_POOLING
#endif

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public interface IPooledObject {
	void OnEnterPool ();
}

/** Lightweight object Pool for IAstarPooledObject.
	* Handy class for pooling objects of type T which implements the IAstarPooledObject interface.
	*
	* Usage:
	* - Claim a new object using \code SomeClass foo = ObjectPool<SomeClass>.Claim (); \endcode
	* - Use it and do stuff with it
	* - Release it with \code ObjectPool<SomeClass>.Release (foo); \endcode
	*
	* After you have released a object, you should never use it again.
	*
	* \since Version 3.2
	* \version Since 3.7.6 this class is thread safe
	* \see Pathfinding.Util.ListPool
	* \see ObjectPoolSimple
	*/
public static class ObjectPool<T> where T : class, IPooledObject, new(){
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static T Claim () {
		return ObjectPoolSimple<T>.Claim();
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void Release (ref T obj) {
		// obj will be set to null so we need to copy the reference
		var tmp = obj;

		ObjectPoolSimple<T>.Release(ref obj);
		tmp.OnEnterPool();
	}
}

/** Lightweight object Pool.
	* Handy class for pooling objects of type T.
	*
	* Usage:
	* - Claim a new object using \code SomeClass foo = ObjectPool<SomeClass>.Claim (); \endcode
	* - Use it and do stuff with it
	* - Release it with \code ObjectPool<SomeClass>.Release (foo); \endcode
	*
	* After you have released a object, you should never use it again.
	*
	* \since Version 3.2
	* \version Since 3.7.6 this class is thread safe
	* \see Pathfinding.Util.ListPool
	* \see ObjectPool
	*/
public static class ObjectPoolSimple<T> where T : class, new(){
	/** Internal pool */
	static BetterList<T> pool = new BetterList<T>();

	static readonly HashSet<T> inPool = new HashSet<T>();

	/** Claim a object.
		* Returns a pooled object if any are in the pool.
		* Otherwise it creates a new one.
		* After usage, this object should be released using the Release function (though not strictly necessary).
		*/
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static T Claim () {
		lock (pool) {
			if (pool.size > 0) {
				T ls = pool[pool.size-1];
				pool.RemoveAt(pool.size-1);
				inPool.Remove(ls);
				return ls;
			} else {
				return new T();
			}
		}
	}

	/** Releases an object.
		* After the object has been released it should not be used anymore.
		* The variable will be set to null to prevent silly mistakes.
		*
		* \throws System.InvalidOperationException
		* Releasing an object when it has already been released will cause an exception to be thrown.
		* However enabling ASTAR_OPTIMIZE_POOLING will prevent this check.
		*
		* \see Claim
		*/
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void Release (ref T obj) {
		lock (pool) {
			if (!inPool.Add(obj)) {
				throw new InvalidOperationException("You are trying to pool an object twice. Please make sure that you only pool it once.");
			}
			pool.Add(obj);
		}
		obj = null;
	}

	/** Clears the pool for objects of this type.
		* This is an O(n) operation, where n is the number of pooled objects.
		*/
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static void Clear () {
		lock (pool) {
			inPool.Clear();
			pool.Clear();
		}
	}

	/** Number of objects of this type in the pool */
	[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
	public static int GetSize () {
		return pool.size;
	}
}