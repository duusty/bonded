﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;

public static class RenderTargetID
{
    static public readonly RenderTargetIdentifier _CameraTargetID = new RenderTargetIdentifier(BuiltinRenderTextureType.CameraTarget);
    static public readonly RenderTargetIdentifier _CurrentActiveID = new RenderTargetIdentifier(BuiltinRenderTextureType.CurrentActive);
}

public static class ShaderPropertyID
{

    #region PUBLIC FIELDS

    // Global
    static public readonly int _GlobalCurseLayerRampTexture = Shader.PropertyToID("_GlobalCurseLayerRampTexture");
    static public readonly int _GlobalCurseLayerTexture = Shader.PropertyToID("_GlobalCurseLayerTexture");
    static public readonly int _GlobalVelocityTexture = Shader.PropertyToID("_GlobalVelocityTexture");


    // Common
    static public readonly int _GlobalCurseLayer = Shader.PropertyToID("_GlobalCurseLayer");
    static public readonly int _CurseLayer = Shader.PropertyToID("_CurseLayer");
    static public readonly int _DepthOffset = Shader.PropertyToID("_DepthOffset");
    static public readonly int _Color = Shader.PropertyToID("_Color");
    static public readonly int _EmissiveColor = Shader.PropertyToID("_EmissiveColor");
    static public readonly int _ZOffset = Shader.PropertyToID("_ZOffset");
    static public readonly int _MainTex = Shader.PropertyToID("_MainTex");

    // HighlightingSystem
    static public readonly int _Outline = Shader.PropertyToID("_Outline");
    static public readonly int _Cutoff = Shader.PropertyToID("_Cutoff");
    static public readonly int _Intensity = Shader.PropertyToID("_Intensity");
    static public readonly int _ZTest = Shader.PropertyToID("_ZTest");
    static public readonly int _StencilRef = Shader.PropertyToID("_StencilRef");
    static public readonly int _Cull = Shader.PropertyToID("_Cull");
    static public readonly int _HighlightingBlur1 = Shader.PropertyToID("_HighlightingBlur1");
    static public readonly int _HighlightingBlur2 = Shader.PropertyToID("_HighlightingBlur2");

    // HighlightingSystem global shader properties. Should be unique!
    static public readonly int _HighlightingBuffer = Shader.PropertyToID("_HighlightingBuffer");
    static public readonly int _HighlightingBlurred = Shader.PropertyToID("_HighlightingBlurred");
    static public readonly int _HighlightingBlurOffset = Shader.PropertyToID("_HighlightingBlurOffset");
    static public readonly int _HighlightingZWrite = Shader.PropertyToID("_HighlightingZWrite");
    static public readonly int _HighlightingOffsetFactor = Shader.PropertyToID("_HighlightingOffsetFactor");
    static public readonly int _HighlightingOffsetUnits = Shader.PropertyToID("_HighlightingOffsetUnits");
    static public readonly int _HighlightingBufferTexelSize = Shader.PropertyToID("_HighlightingBufferTexelSize");

    //Sky Weather System
    static public readonly int _MoonTextureNAME = Shader.PropertyToID("_MoonTexture");
    static public readonly int _SunPosNAME = Shader.PropertyToID("_SunPos");
    static public readonly int _SettingsNAME = Shader.PropertyToID("_Settings");

    static public readonly int _HorizonColorNightNAME = Shader.PropertyToID("_HorizonColorNight");
    static public readonly int _HorizonColorEveningNAME = Shader.PropertyToID("_HorizonColorEvening");
    static public readonly int _HorizonColorDayNAME = Shader.PropertyToID("_HorizonColorDay");
    static public readonly int _ColorNightNAME = Shader.PropertyToID("_ColorNight");
    static public readonly int _ColorEveningNAME = Shader.PropertyToID("_ColorEvening");
    static public readonly int _ColorDayNAME = Shader.PropertyToID("_ColorDay");
    static public readonly int _SunColorNAME = Shader.PropertyToID("_SunColor");
    static public readonly int _MoonColorNAME = Shader.PropertyToID("_MoonColor");

    static public readonly int _WindDirectionStrengthNAME = Shader.PropertyToID("_WindDirectionStrength");
    static public readonly int _FogColorOpacityNAME = Shader.PropertyToID("_FogColorOpacity");
    static public readonly int _GlobalColorNAME = Shader.PropertyToID("_GlobalColor");
    static public readonly int _LightDirectionNAME = Shader.PropertyToID("_LightDirection");
    #endregion

    static public readonly int ForceShaderArrayID = Shader.PropertyToID("ForceShaderArrayID");
    static public readonly int ForceShaderArraySizeID = Shader.PropertyToID("ForceShaderArraySizeID");

    static ShaderPropertyID()
    {
        _GlobalCurseLayerRampTexture = Shader.PropertyToID("_GlobalCurseLayerRampTexture");
        _GlobalCurseLayerTexture = Shader.PropertyToID("_GlobalCurseLayerTexture");
        _GlobalVelocityTexture = Shader.PropertyToID("_GlobalVelocityTexture");

        _GlobalCurseLayer = Shader.PropertyToID("_GlobalCurseLayer");
        _CurseLayer = Shader.PropertyToID("_CurseLayer");
        _Color = Shader.PropertyToID("_Color");
        _EmissiveColor = Shader.PropertyToID("_EmissiveColor");
        _MainTex = Shader.PropertyToID("_MainTex");
        _ZOffset = Shader.PropertyToID("_ZOffset");

        _Outline = Shader.PropertyToID("_Outline");
        _Cutoff = Shader.PropertyToID("_Cutoff");
        _Intensity = Shader.PropertyToID("_Intensity");
        _ZTest = Shader.PropertyToID("_ZTest");
        _StencilRef = Shader.PropertyToID("_StencilRef");
        _Cull = Shader.PropertyToID("_Cull");
        _HighlightingBlur1 = Shader.PropertyToID("_HighlightingBlur1");
        _HighlightingBlur2 = Shader.PropertyToID("_HighlightingBlur2");

        _HighlightingBuffer = Shader.PropertyToID("_HighlightingBuffer");
        _HighlightingBlurred = Shader.PropertyToID("_HighlightingBlurred");
        _HighlightingBlurOffset = Shader.PropertyToID("_HighlightingBlurOffset");
        _HighlightingZWrite = Shader.PropertyToID("_HighlightingZWrite");
        _HighlightingOffsetFactor = Shader.PropertyToID("_HighlightingOffsetFactor");
        _HighlightingOffsetUnits = Shader.PropertyToID("_HighlightingOffsetUnits");
        _HighlightingBufferTexelSize = Shader.PropertyToID("_HighlightingBufferTexelSize");

        ForceShaderArrayID = Shader.PropertyToID("_ForceShaderArray");
        ForceShaderArraySizeID = Shader.PropertyToID("_ForceShaderArraySize");
    }
}
