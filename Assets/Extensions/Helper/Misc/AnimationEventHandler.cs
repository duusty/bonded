﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimatorHelper
{
    public class Layer
    {
        public static int UpperBody = Animator.StringToHash("Upper Body");
    }
    public class Properties
    {
        public static int SpeedID = Animator.StringToHash("Speed");
        public static int DeadID = Animator.StringToHash("Dead");
        public static int TookDamageID = Animator.StringToHash("TookDamage");
        public static int StunnedID = Animator.StringToHash("Stunned");
        public static int JumpStateID = Animator.StringToHash("JumpState");
        public static int IdleRandomizerID = Animator.StringToHash("IdleRandomizer");
    }
    public class States
    {
        public static int IdleBlendTree = Animator.StringToHash("IdleBlendTree");
        public static int WalkBlendTree = Animator.StringToHash("WalkBlendTree");
        public static int JumpBlendTree = Animator.StringToHash("JumpBlendTree");
        public static int WallSlideBlendTree = Animator.StringToHash("WallSlideBlendTree");

        public static int Stunned = Animator.StringToHash("Stunned");
        public static int TakeDamage = Animator.StringToHash("TakeDamage");
        public static int Die = Animator.StringToHash("Die");
        public static int Decaying = Animator.StringToHash("Decaying");


        public static int Defend = Animator.StringToHash("Die");

        public static int Attack1 = Animator.StringToHash("Attack1");
        public static int Attack2 = Animator.StringToHash("Attack2");
        public static int Attack3 = Animator.StringToHash("Attack3");

        public static int SpecialAttack1 = Animator.StringToHash("SpecialAttack1");
        public static int SpecialAttack2 = Animator.StringToHash("SpecialAttack2");
        public static int SpecialAttack3 = Animator.StringToHash("SpecialAttack3");

        public static int Channeling = Animator.StringToHash("Channeling");

        public static int CastDirectional = Animator.StringToHash("CastDirectional");
        public static int CastAOE = Animator.StringToHash("CastAOE");
        public static int CastSelf = Animator.StringToHash("CastSelf");
        public static int CastTarget = Animator.StringToHash("CastTarget");
    }
}

public class AnimationEventHandler : MonoBehaviour {

    //public PlayerWorldTile owner;
    public Animator animator;
    public Animation animation;

    public BetterList<GenericStateMachine> stateMachines = new BetterList<GenericStateMachine>();

    public event System.Action<int> OnAnimationEventIntTriggered;
    public event System.Action<string> OnAnimationEventStringTriggered;
    public event System.Action OnAnimationEventTriggered;

    public void Initialize(/*PlayerWorldTile worldTile*/)
    {
        if (animation == null)
            animation = GetComponent<Animation>();
        if (animator == null)
            animator = GetComponent<Animator>();
        if (animator != null)
            stateMachines = animator.GetBehaviours<GenericStateMachine>().ToBetterList();

       // owner = worldTile;
       // for (int i = 0, iMax = stateMachines.size; i < iMax; i++)
        //    stateMachines[i].Initialize(owner);
    }

    public void AnimationEventInt(int Index)
    {
        if (OnAnimationEventTriggered != null)
            OnAnimationEventIntTriggered(Index);
    }

    public void AnimationEvent()
    {
        if (OnAnimationEventTriggered != null)
            OnAnimationEventTriggered();
    }

    public void AnimationEventString(string Index)
    {
        if (OnAnimationEventStringTriggered != null)
            OnAnimationEventStringTriggered(Index);
    }
}
