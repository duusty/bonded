﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

//TODO add bitwise operation helpers for this type. Check if we have the flags attribute
//also add dictionary comparer
//https://www.codeproject.com/Articles/33528/Accelerating-Enum-Based-Dictionaries-with-Generic
public static class CachedEnumTypes<T>
    where T : struct, IComparable, IConvertible, IFormattable
{
    public static readonly BetterList<string> stringValues = new BetterList<string>();
    public static readonly BetterList<T> Values = new BetterList<T>();

    public static readonly Dictionary<T, string> valueToString = new Dictionary<T, string>(comparer);
    public static readonly Dictionary<string, T> stringToValue = new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);
    private static readonly HashSet<T> valuesSet = new HashSet<T>();

    public static readonly Dictionary<T, int> valueToIndex = new Dictionary<T, int>(comparer);

    public static readonly EnumComparer<T> comparer = EnumComparer<T>.Instance;

    static CachedEnumTypes()
    {
        T[] _values = (System.Enum.GetValues(typeof(T)) as T[]);
        Values = _values.ToBetterList();
        stringValues = System.Enum.GetNames(typeof(T)).ToBetterList();
        valuesSet = new HashSet<T>(_values);

        for (int i = 0, iMax = Values.size; i < iMax; i++)
        {
            valueToString.Add(Values[i], stringValues[i]);
            stringToValue.Add(stringValues[i],Values[i]);
        }

        for (int i = 0, iMax = Values.size; i < iMax; i++)
            valueToIndex.Add(Values[i], i);
    }

    public static bool isDefined(T @value)
    {
        return valuesSet.Contains(@value);
    }

    public static bool HasFlags(T value, T flag)
    {
        return EnumExtensionsInternal<T>.HasFlagsDelegate(value, flag);
    }

    private static class EnumExtensionsInternal<TEnum>
        where TEnum : struct, IComparable, IConvertible, IFormattable
    {
        public static readonly Func<TEnum, TEnum, bool> HasFlagsDelegate = CreateHasFlagDelegate();
        private static Func<TEnum, TEnum, bool> CreateHasFlagDelegate()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException(string.Format("{0} is not an Enum", typeof(TEnum)), typeof(EnumExtensionsInternal<>).GetGenericArguments()[0].Name);
            }
            ParameterExpression valueExpression = Expression.Parameter(
                    typeof(TEnum),
                    typeof(TEnum).Name
            );
            ParameterExpression flagExpression = Expression.Parameter(
                    typeof(TEnum),
                    typeof(TEnum).Name
            );
            var targetType = Type.GetTypeCode(typeof(TEnum)) == TypeCode.UInt64 ? typeof(ulong) : typeof(long);
            Expression<Func<TEnum, TEnum, bool>> lambdaExpression = Expression.Lambda<Func<TEnum, TEnum, bool>>(
                            Expression.Equal(
                                    Expression.And(
                                            Expression.Convert(
                                                    valueExpression,
                                                    targetType
                                            ),
                                            Expression.Convert(
                                                flagExpression,
                                                targetType
                                            )
                                    ),
                                    Expression.Convert(
                                        flagExpression,
                                        targetType
                                    )
                            ),
                    valueExpression,
                    flagExpression
            );
            return lambdaExpression.Compile();
        }
    }

    public sealed class EnumComparer<TEnum> : IEqualityComparer<TEnum>
         where TEnum : struct, IComparable, IConvertible, IFormattable
    {
        private static readonly Func<TEnum, TEnum, bool> equals;
        private static readonly Func<TEnum, int> getHashCode;

        public static readonly EnumComparer<TEnum> Instance;

        static EnumComparer()
        {
            getHashCode = generateGetHashCode();
            equals = generateEquals();
            Instance = new EnumComparer<TEnum>();
        }
        private EnumComparer()
        {
            assertTypeIsEnum();
            assertUnderlyingTypeIsSupported();
        }

        public bool Equals(TEnum x, TEnum y)
        {
            // call the generated method
            return equals(x, y);
        }

        public int GetHashCode(TEnum obj)
        {
            // call the generated method
            return getHashCode(obj);
        }

        private static void assertTypeIsEnum()
        {
            if (typeof(TEnum).IsEnum)
                return;

            var message =
                string.Format("The type parameter {0} is not an Enum. LcgEnumComparer supports Enums only.",
                              typeof(TEnum));
            throw new NotSupportedException(message);
        }

        private static void assertUnderlyingTypeIsSupported()
        {
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            ICollection<Type> supportedTypes =
                new[]
                    {
                        typeof (byte), typeof (sbyte), typeof (short), typeof (ushort),
                        typeof (int), typeof (uint), typeof (long), typeof (ulong)
                    };

            if (supportedTypes.Contains(underlyingType))
                return;

            var message =
                string.Format("The underlying type of the type parameter {0} is {1}. " +
                              "LcgEnumComparer only supports Enums with underlying type of " +
                              "byte, sbyte, short, ushort, int, uint, long, or ulong.",
                              typeof(TEnum), underlyingType);
            throw new NotSupportedException(message);
        }

        private static Func<TEnum, TEnum, bool> generateEquals()
        {
            var xParam = Expression.Parameter(typeof(TEnum), "x");
            var yParam = Expression.Parameter(typeof(TEnum), "y");
            var equalExpression = Expression.Equal(xParam, yParam);
            return Expression.Lambda<Func<TEnum, TEnum, bool>>(equalExpression, new[] { xParam, yParam }).Compile();
        }

        private static Func<TEnum, int> generateGetHashCode()
        {
            var objParam = Expression.Parameter(typeof(TEnum), "obj");
            var underlyingType = Enum.GetUnderlyingType(typeof(TEnum));
            var convertExpression = Expression.Convert(objParam, underlyingType);
            var getHashCodeMethod = underlyingType.GetMethod("GetHashCode");
            var getHashCodeExpression = Expression.Call(convertExpression, getHashCodeMethod);
            return Expression.Lambda<Func<TEnum, int>>(getHashCodeExpression, new[] { objParam }).Compile();
        }
    }
}
