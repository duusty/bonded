﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable()]
public class UnityDateTime
{
    public UnityDateTime()
    {
        Date = DateTime.Now;
    }

    public DateTime Date
    {
        get { return DateTime.Parse(_DateAsString); }
        set
        {
            if (_DateAsString != value.ToString())
            {
                _DateAsString = value.ToString();
                _DateAsShortString = value.ToShortDateString();
            }
        }
    }

    [SerializeField()]
    private string _DateAsString, _DateAsShortString;
    public string DateAsString
    {
        get { return _DateAsString; }
    }

    public string DateAsShortString
    {
        get { return _DateAsShortString; }
    }
}
