using System;
using System.Collections.Generic;

public enum MessengerMode
{
    DONT_REQUIRE_LISTENER,
    REQUIRE_LISTENER,
}

#region MessengerEvent
/// <summary>
/// A delegate Optimized for Gameplay Usage and our Messenger System
/// Most parameters depend on the Eventtype, so make sure to check if an object is the one wanted.
/// </summary>
/// <param name="e">This should contain commonly used variables for example an integer since the achievment system can attach to every event, also additional Parameter</param>
/// <param name="sender">For the most times should contain the Instegator</param>
/// <returns>returns results different for each event type</returns>
public delegate void MessengerEvent(object sender, MessengerEventArgs e);

public class MessengerEventArgs : EventArgs
{
    public MessengerEventArgs(int ChangeAchievment = 0)
    {
        AchievmentModifier = ChangeAchievment;
    }

    public MessengerEventArgs(BetterList<object> Params, int ChangeAchievment = 0)
    {
        AchievmentModifier = ChangeAchievment;
        AdditionalParameter = Params;
    }

    public BetterList<object> AdditionalParameter;
    public int AchievmentModifier = 0;
}
#endregion

#region MessengerEvents
public enum MessengerEvents
{
    Game_Started,
    Game_Loaded,
    Game_Saved,
    Game_Left,

    Level_Started,
    Level_Completed,            //Level ID or name

    Player_Died,                //enum Reason, DamageInfo, Enemy
    Player_TakeDamage,          //DamageInfo Amount
    Player_Farted,              //DamageInfo Target Enemy Class Amount
    Player_Landed,              //Fall Distance, Speed
    Player_Jumped,
    Player_Moved,               //Distance (Vector3)

    Enemy_Died,                 //Enemy Object, DamageInfo, bool byPlayer
    Enemy_TakeDamage,
    Enemy_Healed,
    Enemy_Attacked,

    Achievment_Raised,           //AchievmentID, NewStage

    Mumins_Detected,
    Why_Im_so_Happy_Detected,
    Food_Collected,              // FoodType, Amount
    Disney_Detected,
    Nyan_Mode_Activated,         // Type
    Nyan_Pocked,                 // ScoreAddition
    Tamagotchi_Feature_Activated,//Object, TamagotchiInteractionType Type
    Narwhals_Killed,
    Minutes_Played,              //Minute Addition
    Color_Collected,             //Colortype
    Soundtrack_Changed,          //string = "ID"
    Special_Event_Started,       //SpecialEventType
    Account_Created,             //Name
    NyanCatScore_Change          //Value
}
#endregion

static internal class Messenger
{
    static public Dictionary<string, MessengerEvent> eventTable = new Dictionary<string, MessengerEvent>();

    static public void AddListener(string eventType, MessengerEvent handler)
    {
        if (!eventTable.ContainsKey(eventType)) eventTable.Add(eventType, null);

        if (handler != null && handler.GetType() != handler.GetType())
            throw new ListenerException(string.Format("Attempting to add listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being added has type {2}", eventType, handler.GetType().Name, handler.GetType().Name));
        eventTable[eventType] = (MessengerEvent)eventTable[eventType] + handler;
    }

    static public void RemoveListener(string eventType, MessengerEvent handler)
    {
        if (eventTable.ContainsKey(eventType))
        {
            MessengerEvent d = eventTable[eventType];
            if (d == null)
                throw new ListenerException(string.Format("Attempting to remove listener with for event type {0} but current listener is null.", eventType));
            else if (d.GetType() != handler.GetType())
                throw new ListenerException(string.Format("Attempting to remove listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being removed has type {2}", eventType, d.GetType().Name, handler.GetType().Name));
        }
        else
            throw new ListenerException(string.Format("Attempting to remove listener for type {0} but Messenger doesn't know about this event type.", eventType));
        eventTable[eventType] = (MessengerEvent)eventTable[eventType] - handler;
        if (eventTable[eventType] == null) eventTable.Remove(eventType);
    }

    static public void Broadcast(string eventType, object sender, MessengerEventArgs e, MessengerMode mode = MessengerMode.REQUIRE_LISTENER)
    {
        if(e == null)
            throw new BroadcastException(string.Format("Broadcasting message {0} but no arguments found.", eventType));
        if (mode == MessengerMode.REQUIRE_LISTENER && !eventTable.ContainsKey(eventType))
            throw new BroadcastException(string.Format("Broadcasting message {0} but no listener found.", eventType));
        MessengerEvent callback;
        if (eventTable.TryGetValue(eventType, out callback))
        {
            if (callback != null)
                callback(sender, e);
            else
                throw new BroadcastException(string.Format("Broadcasting message {0} but listeners have a different signature than the broadcaster.", eventType));
        }
    }

    public class BroadcastException : Exception
    {
        public BroadcastException(string msg)
            : base(msg)
        {
        }
    }

    public class ListenerException : Exception
    {
        public ListenerException(string msg)
            : base(msg)
        {
        }
    }
}

