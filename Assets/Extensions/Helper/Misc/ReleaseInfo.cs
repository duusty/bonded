﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Diagnostics;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable()]
public class UpdateUnityGetVersionString { }


[System.Serializable()]
public class BuildDetails
{
    public enum ProductionType
    {
        None = -1,
        PreProduction = 0,
        Alpha = 1,
        FamilyFriends = 2,
        ClosedBeta = 10,
        OpenBeta = 50,
        Release = 100,
        Gold = 200
    }

    public string ProductionTypeAsString()
    {
        switch (Type)
        {
            case ProductionType.PreProduction: return "PreProduction.0";
            case ProductionType.Alpha: return "Alpha.0";
            case ProductionType.FamilyFriends: return "FamilyFriends.0";
            case ProductionType.ClosedBeta: return "ClosedBeta.0";
            case ProductionType.OpenBeta: return "OpenBeta.0";
            case ProductionType.Release: return "Release.1";
            case ProductionType.Gold: return "Gold.2";
            default: return "";
        }
    }

    public UnityDateTime BuildDate = new UnityDateTime();
    public int BuildNumber = 1;
    public int MaintenanceNumber = 0;
    public int MinorNumber = 0;
    public string CommitHash = "";
    public string BuildPrefix = "TestTest";
    public ProductionType Type = ProductionType.PreProduction;
    public string GUID = "Undidentified";
    [Multiline(15)]
    public string Patchnotes = "";
    public string ProductName = "Im[Boss]ible";

    public UpdateUnityGetVersionString UpdateVersionNumber = new UpdateUnityGetVersionString();

#if UNITY_EDITOR
    public BuildDetails()
    {
        GUID = UnityEditor.VisualStudioIntegration.SolutionGuidGenerator.GuidForProject(ProductName);
    }
#endif
}

[System.Serializable()]
public class DeveloperDetails
{
    public string Developer = "Synoptic-Entertainment";
    public Texture2D DeveloperSplashImage;
    public string Website = "www.google.com";
    public string SupportWebsite = "support.google.com";
}


[System.Serializable()]
public class ReleaseInfo {

#if UNITY_EDITOR
    public static void UpdateVersion()
    {
        _instance = getInstance();
        if(_instance == null)
            return;
        _instance.BuildDetails.BuildNumber++;
        _instance.BuildDetails.BuildDate.Date = DateTime.Now;
        _instance.DeveloperDetails.Developer = PlayerSettings.companyName;
        _instance.BuildDetails.ProductName = PlayerSettings.productName;
        _instance.BuildDetails.MinorNumber = (Mathf.Abs((short)DateTime.Now.Ticks));

        Process p = new Process();
        p.StartInfo.FileName = "git";
        p.StartInfo.Arguments = @"rev-parse HEAD";
        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.RedirectStandardInput = true;
        p.StartInfo.UseShellExecute = false;
       // p.StartInfo = startInfo;
        p.Start();
        _instance.BuildDetails.CommitHash = p.StandardOutput.ReadToEnd().Substring(0,8);
        p.WaitForExit();


        UnityEngine.Debug.Log(GetVersion);
    }
#endif

    public BuildDetails BuildDetails = new BuildDetails();
    public DeveloperDetails DeveloperDetails = new DeveloperDetails();

    public void Initialize()
    {
        _instance = this;
    }

    public static ReleaseInfo getInstance()
    {
        return _instance;
    }

    private static ReleaseInfo _instance;

    public ReleaseInfo ()
    {
        _instance = this;
    }


    private string VersionCache = string.Empty;

    public static string GetVersion
    {
        get
        {
             _instance = getInstance();
            if(_instance == null)
                return "Undefined Database Not Found";
            return getInstance().GetVersionNonStatic;
        }
    }

    public string GetVersionNonStatic
    {
        get
        {
#if !UNITY_EDITOR
            if (string.IsNullOrEmpty(VersionCache))
#endif
                VersionCache = string.Format("{0}.{1}.{2}.{3}({4}[{5}])",
                                             BuildDetails.ProductionTypeAsString(),
                                             BuildDetails.BuildNumber,
                                             BuildDetails.MinorNumber,
                                             BuildDetails.CommitHash,
                                             BuildDetails.BuildDate.DateAsShortString.Replace("/", "."),
                                             BuildDetails.BuildPrefix);
            return VersionCache;
        }
    }
}
