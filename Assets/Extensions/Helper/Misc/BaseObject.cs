﻿using UnityEngine;
using System.Collections;

public class BaseObject : MonoBehaviour
{
    [HideInInspector()] public Transform myTransform;
    [HideInInspector()] public GameObject myGameObject;

    protected virtual void Awake()
    {
        myTransform = transform;
        myGameObject = gameObject;
    }
}
