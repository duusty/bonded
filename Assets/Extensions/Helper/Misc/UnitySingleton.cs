﻿using System;
using UnityEngine;
using System.Runtime.CompilerServices;

#pragma warning disable 1591 // undocumented XML code warning

public static class UnitySingleton<T>
    where T : SingletonMonoBehaviour<T>
{
    static T _instance = null;
    static internal T _autoCreatePrefab;
    static private int _GlobalInstanceCount = 0;

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public T GetSingleton(bool throwErrorIfNotFound, bool autoCreate)
    {
        if (!_instance)
        {
            _instance = GameObject.FindObjectOfType<T>();
            if (!_instance)
            {
                if (autoCreate && _autoCreatePrefab != null)
                {
                    _instance = MonoBehaviour.Instantiate<T>(_autoCreatePrefab);
                    _instance.gameObject.name = _autoCreatePrefab.name;
                    Debug.Log( "No singleton component " + typeof(T).Name + " found in the scene. Instance Created" );
                }
                else if (throwErrorIfNotFound)
                {
                    Debug.LogError( "No singleton component " + typeof(T).Name + " found in the scene." );
                    return null;
                }
            }
        }
        return _instance;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static internal void _Awake(T instance)
    {
        _GlobalInstanceCount++;
        if (_GlobalInstanceCount > 1)
            Debug.LogError("More than one instance of SingletonMonoBehaviour " + typeof(T).Name);
        else 
            _instance = instance;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static internal void _Destroy()
    {
        if ( _GlobalInstanceCount > 0 )
        {
            _GlobalInstanceCount--;
            if ( _GlobalInstanceCount == 0 )
                _instance = null;
        }
    }
}

public abstract class SingletonMonoBehaviour<T> : BaseObject
    where T : SingletonMonoBehaviour<T>
{
    static public T Instance
    { 
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get { return UnitySingleton<T>.GetSingleton( true, true ); }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public T DoesInstanceExist()
    {
        return UnitySingleton<T>.GetSingleton( false, false );
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public void ActivateSingletonInstance() // 
    {
        UnitySingleton<T>.GetSingleton( true, true );
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public void SetSingletonAutoCreate(T autoCreatePrefab)
    {
        UnitySingleton<T>._autoCreatePrefab = autoCreatePrefab;
    }

    protected override void Awake()
    {
        base.Awake();
        UnitySingleton<T>._Awake((T)this);
    }

    protected virtual void OnDestroy()
    {
        UnitySingleton<T>._Destroy();
    }
}



