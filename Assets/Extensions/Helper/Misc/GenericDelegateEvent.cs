﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#region RefComboFunc<T1,T2>

public class GenericDelegateRefComboFuncList<T, T2>
{
    private BetterList<GenericDelegateRefComboFunc<T, T2>> list = new BetterList<GenericDelegateRefComboFunc<T, T2>>();

    public void Execute(ref T value, T2 value2)
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateRefComboFunc<T, T2> del = list[i];

                if (del != null)
                {
                    del.Execute(ref value, value2);

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefComboFunc<T, T2> del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set(RefComboFunc<T, T2> callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateRefComboFunc<T, T2>(callback));
        }
    }

    public void Add(RefComboFunc<T, T2> callback) { Add(callback, false); }


    public void Add(RefComboFunc<T, T2> callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefComboFunc<T, T2> del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateRefComboFunc<T, T2> ed = new GenericDelegateRefComboFunc<T, T2>(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }


    public bool Remove(RefComboFunc<T, T2> callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefComboFunc<T, T2> del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}

public class GenericDelegateRefComboFunc<T, T2>
{
    public bool oneShot = false;
    RefComboFunc<T, T2> mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return (mCachedCallback != null); } }

    public GenericDelegateRefComboFunc() { }
    public GenericDelegateRefComboFunc(RefComboFunc<T, T2> call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is RefComboFunc<T, T2>)
        {
            RefComboFunc<T, T2> callback = obj as RefComboFunc<T, T2>;
            return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateRefFunc<T, T2>)
        {
            GenericDelegateRefComboFunc<T, T2> del = obj as GenericDelegateRefComboFunc<T, T2>;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = ("GenericDelegateRefFunc" + typeof(T).ToString()).GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    RefComboFunc<T, T2> Get()
    {
        return mCachedCallback;
    }

    public bool Execute(ref T value, T2 value2)
    {
        RefComboFunc<T, T2> call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call(ref value,  value2);
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call(ref value, value2);
            }
#else
			call(ref value, value2);
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]" + typeof(T).ToString();
    }
}
#endregion

#region RefFunc<T1,T2>

public class GenericDelegateRefFuncList<T, T2>
{
    private BetterList<GenericDelegateRefFunc<T, T2>> list = new BetterList<GenericDelegateRefFunc<T, T2>>();

    public void Execute(ref T value,ref  T2 value2)
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateRefFunc<T, T2> del = list[i];

                if (del != null)
                {
                    del.Execute(ref value,ref value2);

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T, T2> del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set(RefFunc<T, T2> callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateRefFunc<T, T2>(callback));
        }
    }

    public void Add(RefFunc<T, T2> callback) { Add(callback, false); }


    public void Add(RefFunc<T, T2> callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T, T2> del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateRefFunc<T, T2> ed = new GenericDelegateRefFunc<T, T2>(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }


    public bool Remove(RefFunc<T, T2> callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T, T2> del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}

public class GenericDelegateRefFunc<T, T2>
{
    public bool oneShot = false;
    RefFunc<T, T2> mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return (mCachedCallback != null); } }

    public GenericDelegateRefFunc() { }
    public GenericDelegateRefFunc(RefFunc<T, T2> call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is RefFunc<T, T2>)
        {
            RefFunc<T, T2> callback = obj as RefFunc<T, T2>;
            return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateRefFunc<T, T2>)
        {
            GenericDelegateRefFunc<T, T2> del = obj as GenericDelegateRefFunc<T, T2>;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = ("GenericDelegateRefFunc" + typeof(T).ToString()).GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    RefFunc<T, T2> Get()
    {
        return mCachedCallback;
    }

    public bool Execute(ref T value,ref T2 value2)
    {
        RefFunc<T, T2> call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call(ref value,ref  value2);
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call(ref value,ref value2);
            }
#else
			call(ref value,ref value2);
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]" + typeof(T).ToString();
    }
}
#endregion

#region RefFunc<T1>

public class GenericDelegateRefFuncList<T>
{
    private BetterList<GenericDelegateRefFunc<T>> list = new BetterList<GenericDelegateRefFunc<T>>();

    public void Execute(ref T value)
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateRefFunc<T> del = list[i];

                if (del != null)
                {
                    del.Execute(ref value);

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T> del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set(RefFunc<T> callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateRefFunc<T>(callback));
        }
    }

    public void Add(RefFunc<T> callback) { Add(callback, false); }


    public void Add(RefFunc<T> callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T> del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateRefFunc<T> ed = new GenericDelegateRefFunc<T>(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }


    public bool Remove(RefFunc<T> callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateRefFunc<T> del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}

public class GenericDelegateRefFunc<T>
{
    public bool oneShot = false;
    RefFunc<T> mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return (mCachedCallback != null); } }

    public GenericDelegateRefFunc() { }
    public GenericDelegateRefFunc(RefFunc<T> call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is RefFunc<T>)
        {
            RefFunc<T> callback = obj as RefFunc<T>;
            return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateRefFunc<T>)
        {
            GenericDelegateRefFunc<T> del = obj as GenericDelegateRefFunc<T>;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = ("GenericDelegateRefFunc" + typeof(T).ToString()).GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    RefFunc<T> Get()
    {
        return mCachedCallback;
    }

    public bool Execute(ref T value)
    {
        RefFunc<T> call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call(ref value);
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call(ref value);
            }
#else
			call(ref value);
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]" + typeof(T).ToString();
    }
}
#endregion

#region Action<T1,T2>

public class GenericDelegateActionList<T, T2>
{
    private BetterList<GenericDelegateAction<T, T2>> list = new BetterList<GenericDelegateAction<T, T2>>();

    public void Execute(T value, T2 value2)
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateAction<T, T2> del = list[i];

                if (del != null)
                {
                    del.Execute(value, value2);

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T, T2> del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set( Action<T, T2> callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateAction<T, T2>(callback));
        }
    }

    public void Add(Action<T, T2> callback) { Add(callback, false); }


    public void Add(Action<T, T2> callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T, T2> del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateAction<T, T2> ed = new GenericDelegateAction<T, T2>(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }


    public bool Remove(Action<T, T2> callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T, T2> del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}

public class GenericDelegateAction<T, T2>
{
    public bool oneShot = false;
    Action<T, T2> mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return ( mCachedCallback != null) ; } }

    public GenericDelegateAction () { }
    public GenericDelegateAction(Action<T, T2> call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is Action<T, T2>)
        {
            Action<T, T2> callback = obj as Action<T, T2>;
			return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateAction<T, T2>)
        {
            GenericDelegateAction<T, T2> del = obj as GenericDelegateAction<T, T2>;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = ("GenericDelegateAction" + typeof(T).ToString()).GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    System.Action<T, T2> Get()
    {
        return mCachedCallback;
    }

    public bool Execute(T value, T2 value2)
    {
        System.Action<T, T2> call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call(value,value2);
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call(value,value2);
            }
#else
			call(value,value2);
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]" +typeof(T).ToString();
    }
}
#endregion

#region Action<T1>

public class GenericDelegateActionList<T>
{
    private BetterList<GenericDelegateAction<T>> list = new BetterList<GenericDelegateAction<T>>();


    public void Execute(T value)
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateAction<T> del = list[i];

                if (del != null)
                {
                    del.Execute(value);

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T> del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set(Action<T> callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateAction<T>(callback));
        }
    }

    public void Add(Action<T> callback) { Add(callback, false); }


    public void Add(Action<T> callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T> del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateAction<T> ed = new GenericDelegateAction<T>(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }


    public bool Remove(Action<T> callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction<T> del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}

public class GenericDelegateAction<T>
{
    public bool oneShot = false;
    Action<T> mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return ( mCachedCallback != null) ; } }

    public GenericDelegateAction () { }
    public GenericDelegateAction(Action<T> call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is Action<T>)
        {
            Action<T> callback = obj as Action<T>;
			return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateAction<T>)
        {
            GenericDelegateAction<T> del = obj as GenericDelegateAction<T>;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = ("GenericDelegateAction" + typeof(T).ToString()).GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    System.Action<T> Get()
    {
        return mCachedCallback;
    }

    public bool Execute(T value)
    {
        System.Action<T> call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call(value);
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call(value);
            }
#else
			call(value);
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]" +typeof(T).ToString();
    }
}
#endregion

#region Action

public class GenericDelegateActionList
{
    private BetterList<GenericDelegateAction> list = new BetterList<GenericDelegateAction>();

    public void Execute()
    {
        if (list != null)
        {
            for (int i = 0; i < list.size; )
            {
                GenericDelegateAction del = list[i];

                if (del != null)
                {
                    del.Execute();

                    if (i >= list.size) break;
                    if (list[i] != del) continue;

                    if (del.oneShot)
                    {
                        list.RemoveAt(i);
                        continue;
                    }
                }
                ++i;
            }
        }
    }

    public bool IsValid()
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction del = list[i];
                if (del != null && del.isValid)
                    return true;
            }
        }
        return false;
    }

    public void Set(Action callback)
    {
        if (list != null)
        {
            list.Clear();
            list.Add(new GenericDelegateAction(callback));
        }
    }

    public void Add(Action callback) { Add(callback, false); }

    public void Add(Action callback, bool oneShot)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction del = list[i];
                if (del != null && del.Equals(callback))
                    return;
            }

            GenericDelegateAction ed = new GenericDelegateAction(callback);
            ed.oneShot = oneShot;
            list.Add(ed);
        }
        else
            Debug.LogWarning("Attempting to add a callback to a list that's null");
    }

    public bool Remove(Action callback)
    {
        if (list != null)
        {
            for (int i = 0, imax = list.size; i < imax; ++i)
            {
                GenericDelegateAction del = list[i];

                if (del != null && del.Equals(callback))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }

    public void Clear()
    {
        list.Clear();
    }
}

public class GenericDelegateAction
{
    public bool oneShot = false;
    Action mCachedCallback;

    public bool isEnabled { get { return (mCachedCallback != null); } }
    public bool isValid { get { return (mCachedCallback != null); } }

    public GenericDelegateAction() { }
    public GenericDelegateAction(Action call) { mCachedCallback = call; }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return !isValid;
        if (obj is Action)
        {
            Action callback = obj as Action;
            return callback.Equals(mCachedCallback);
        }
        if (obj is GenericDelegateAction)
        {
            GenericDelegateAction del = obj as GenericDelegateAction;
            return (Get() == del.Get());
        }
        return false;
    }
    static int s_Hash = "GenericDelegateAction".GetHashCode();
    public override int GetHashCode() { return s_Hash; }

    System.Action Get()
    {
        return mCachedCallback;
    }

    public bool Execute()
    {
        System.Action call = Get();

        if (call != null)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                call();
            else if (call.Target != null)
            {
                System.Type type = call.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) call();
            }
#else
			call();
#endif
            return true;
        }
        return false;
    }

    public void Clear()
    {
        mCachedCallback = null;
    }

    public override string ToString()
    {
        return "[delegate]".ToString();
    }

}
#endregion
