﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class EditorConstants
{
    //[MenuItem(EditorHelper.AssetCreatorPathStringPrefix+"Effects/Effect")]
    public const string AssetCreatorPathStringPrefix = "Assets/_Imbossible/";
}

public class Constants {

    public const int MaxItemStacks = 20;
    public const int MonsterMaxLevel = 100;
    public const int MaxLevel = 40;

    public const float CachedStatUpdateRate = 1; //seconds
}

[System.Serializable()]
public static class GamePaths
{
    public static readonly DirectoryInfo ApplicationPath;
    public static readonly DirectoryInfo ScreenshotsPath;
    public static readonly DirectoryInfo SaveGames;

    public static readonly DirectoryInfo ProjectPath;
    public static readonly DirectoryInfo ProjectPrefabsPath;

    public static readonly DirectoryInfo ModDirectory;

    static GamePaths()
    {
        string tempApplicationPath = Application.dataPath;
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.OSXEditor:
                tempApplicationPath += "/WebPlayerTemplates"; break;
            case RuntimePlatform.OSXPlayer:
                tempApplicationPath += "/../.."; break;
            case RuntimePlatform.WindowsPlayer:
                tempApplicationPath += "/.."; break;
        }
        ApplicationPath = HelperParent.CreateDirectoryPathIfNotExists(tempApplicationPath);
        ProjectPath = HelperParent.CreateDirectoryPathIfNotExists(ApplicationPath + "/_LeroyJenkins");
        ProjectPrefabsPath = HelperParent.CreateDirectoryPathIfNotExists(ProjectPath + "/Prefabs");
        ScreenshotsPath = HelperParent.CreateDirectoryPathIfNotExists(ApplicationPath + "/Screenshots");
        SaveGames = HelperParent.CreateDirectoryPathIfNotExists(ApplicationPath + "/SaveGames");
        ModDirectory = HelperParent.CreateDirectoryPathIfNotExists(ApplicationPath + "/Mods");
    }
}