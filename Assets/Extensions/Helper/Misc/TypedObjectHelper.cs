﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using System.Linq;
using Vexe.Fast.Reflection;

public class TypeObjectGetterSetter
{
    private Vexe.Fast.Reflection.MemberGetter<object, object> getter;
    private Vexe.Fast.Reflection.MemberSetter<object, object> setter;

    public MemberInfo memberInfo;

    public TypeObjectGetterSetter(
        Vexe.Fast.Reflection.MemberGetter<object, object> _getter,
        Vexe.Fast.Reflection.MemberSetter<object, object> _setter,
        MemberInfo _memberInfo)
    {
        getter = _getter;
        setter = _setter;
        memberInfo = _memberInfo;
    }

    public void setValue(object @target, object @value)
    {
        setter(ref target, value);
    }

    public object getValue(object @target)
    {
        return getter(@target);
    }
}

public class TypedObjectFormatterHelper
{
    public readonly Dictionary<string, Vexe.Fast.Reflection.MemberGetter<object, object>> getterDictionary;
    public readonly Dictionary<string, Vexe.Fast.Reflection.MemberSetter<object, object>> setterDictionary;

    public readonly BetterList<TypeObjectGetterSetter> TypedObjectProperties;

    public TypedObjectFormatterHelper(System.Type @type)
    {
        getterDictionary = new Dictionary<string, MemberGetter<object, object>>();
        setterDictionary = new Dictionary<string, MemberSetter<object, object>>();
        TypedObjectProperties = new BetterList<TypeObjectGetterSetter>();

        BetterList<PropertyInfo> properties = @type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).ToBetterList();

        foreach (PropertyInfo prop in properties)
        {
            if (!prop.CanWrite || prop.GetSetMethod(false) == null)
                continue;

            getterDictionary.Add(prop.Name, prop.DelegateForGet());
            //if not writeable we should be able to replace the object that way
            setterDictionary.Add(prop.Name, prop.DelegateForSet());
            TypedObjectProperties.Add(new TypeObjectGetterSetter(prop.DelegateForGet(), prop.DelegateForSet(), prop));
        }

        BetterList<FieldInfo> fields = @type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).ToBetterList();

        foreach (FieldInfo field in fields)
        {
            getterDictionary.Add(field.Name, field.DelegateForGet());
            setterDictionary.Add(field.Name, field.DelegateForSet());
            TypedObjectProperties.Add(new TypeObjectGetterSetter(field.DelegateForGet(), field.DelegateForSet(), field));
        }
    }

    public void ToObject<T>(IDictionary<string, object> source, ref T @object)
    {
        object refObject = @object;
        foreach (KeyValuePair<string, object> item in source)
            setterDictionary[item.Key](ref refObject, item.Value);
    }

    public void ToObject(IDictionary<string, object> source, ref object @object)
    {
        foreach (KeyValuePair<string, object> item in source)
            setterDictionary[item.Key](ref @object, item.Value);
    }

    public IDictionary<string, object> AsDictionary<T>(T @object, BetterList<string> filter)
    {
        IDictionary<string, object> dic = new Dictionary<string, object>(filter.size);
        foreach (string str in filter)
            dic.Add(str, getterDictionary[str](@object));
        return dic;
    }

    public void AsDictionary<T>(ref IDictionary<string, object> dic, T @object)
    {
        BetterList<string> keys = dic.Keys.ToBetterList();
        string key = "";
        for (int i = 0, iMax = keys.size; i < iMax; i++)
        {
            key = keys[i];
            dic[key] = getterDictionary[key](@object);
        }
    }
}

public static class TypedObjectHelperResolver
{
    private static Dictionary<System.Type, TypedObjectFormatterHelper> formatterMapping = new Dictionary<System.Type, TypedObjectFormatterHelper>()
    {
    };

    public static TypedObjectFormatterHelper getFormatter(System.Type @type)
    {
        TypedObjectFormatterHelper returnFormatter = null;
        if (!formatterMapping.TryGetValue(@type, out returnFormatter))
        {
            returnFormatter = new TypedObjectFormatterHelper(@type);
            formatterMapping.Add(@type, returnFormatter);
        }
        return returnFormatter;
    }
}

public static class TypedObjectHelperResolver<T> where T : class, new()
{
    public readonly static TypedObjectFormatterHelper Instance;

    static TypedObjectHelperResolver()
    {
        Instance = TypedObjectHelperResolver.getFormatter(typeof(T));
    }
}
