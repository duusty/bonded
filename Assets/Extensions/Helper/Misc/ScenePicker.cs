﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScenePicker 
{
	[SerializeField]
    public string scenePath;

	[SerializeField]
    public string sceneName;

	[SerializeField]
    public int sceneBuildID;
}
