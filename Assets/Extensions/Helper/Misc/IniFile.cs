﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;

public class INIEditorVariable<T> : INIVariable<T>
{
    public INIEditorVariable(string Section, string Key, T defaultValue, bool Load = true)
        : base(Section, Key, defaultValue, Load)
    { }

    public override void Load()
    {
        Loaded = true;
        _value = INIFile.IniReadValue<T>(section, key, _value, INIFile.EditorSettingsPath);
    }

    public override void Save()
    {
        INIFile.WriteIniValue(section, key, _value, INIFile.EditorSettingsPath);
    }
}

public class INIEditorVariableCapacity<T> : INIEditorVariable<T>
{
    public INIEditorVariableCapacity(string Section, string Key, T defaultValue, int Capacity = 255, bool Load = true)
        : base(Section, Key, defaultValue, Load)
    {
        capacity = Capacity;
    }

    public override void Load()
    {
        Loaded = true;
        _value = INIFile.IniReadValue<T>(section, key, _value, INIFile.EditorSettingsPath, capacity);
    }

    public override void Save()
    {
        INIFile.WriteIniValue(section, key, _value, INIFile.EditorSettingsPath);
    }

    private int capacity;
}

public class INIVariableCapacity<T> : INIVariable<T>
{
    public INIVariableCapacity(string Section, string Key, T defaultValue, int Capacity = 255, bool Load = true)
        : base(Section, Key, defaultValue, Load)
    {
        capacity = Capacity;
    }

    public override void Load()
    {
        Loaded = true;
        _value = INIFile.IniReadValue<T>(section, key, _value, INIFile.GameSettingsPath, capacity);
    }

    private int capacity;
}

public class INIVariable<T>
{
    public INIVariable(string Section, string Key, T defaultValue, bool Load = true)
    {
        key = Key;
        _value = defaultValue;
        section = Section;
        if(Load) this.Load();
    }

    public T ValueSaved
    {
        get {
            if (!Loaded) Load();
            return _value; 
        }
        set
        {
            if (!value.Equals(this._value))
            {
                this._value = value;
                if (OnValueChanged != null) OnValueChanged(_value);
                Save();
            }

        }
    }

    public T Value
    {
        get {
            if (!Loaded) Load();
            return _value; 
        }
        set
        {
            if (!value.Equals(this._value))
            {
                this._value = value;
                FireOnValueChanged();
            }

        }
    }

    public virtual void Save()
    {
        INIFile.WriteIniValue(section, key, _value, INIFile.GameSettingsPath);
    }

    public virtual void Load()
    {
        Loaded = true;
        _value = INIFile.IniReadValue<T>(section, key, _value, INIFile.GameSettingsPath);
    }

    public static implicit operator T(INIVariable<T> value)
    {
        return value.Value;
    }

    public void FireOnValueChanged(bool save = false)
    {
        if (OnValueChanged != null) OnValueChanged(_value);
        if(save)
            Save();
    }

    public event Action<T> OnValueChanged;

    protected bool Loaded = false;
    protected string key;
    protected string section;
    protected T _value;
}

public static class INIFile
{
    static INIFile()
    {
        //We could create our save files here if needed
    }

    public static string GameSettingsPath = GamePaths.ApplicationPath + "\\Game.settings";
    public static string EditorSettingsPath = GamePaths.ApplicationPath + "\\Editor.settings";

    [DllImport("kernel32")]
    private static extern bool WritePrivateProfileString(string section,
      string key, string val, string filePath);

    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(string section,
      string key, string def, StringBuilder retVal,
      int size, string filePath);

    public static T IniReadValue<T>(string Section, string Key, T Default, string filePath, int Capacity = 255)
    {
        StringBuilder temp = new StringBuilder(Capacity);
        if (GetPrivateProfileString(Section, Key, Default.ToString(), temp, Capacity, filePath) != 0)
            temp.ToString().TryParseString(ref Default);
        return Default;
    }

    public static bool WriteIniValue<T>(string section, string key, T value, string filePath)
    {
        return WritePrivateProfileString(section, key, value.ToString(), filePath);
    }
}

