﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif
public class AdvancedScriptAbleObject<T> : ScriptableObject where T : ScriptableObject
{
    #if UNITY_EDITOR



    public static T CreateSubAsset<T>(string AddToWhichAsset) where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        AssetDatabase.AddObjectToAsset(asset, AddToWhichAsset);

        AssetDatabase.SaveAssets();
        return asset;
    }


    public static T CreateAsset<T>(string AdditionalSubPath = "") where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + AdditionalSubPath + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;
    }
    #endif
}
