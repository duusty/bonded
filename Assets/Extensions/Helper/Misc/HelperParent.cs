using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Security.Cryptography;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

#region DelegateExtensions Generic Reference
public delegate void RefFunc<T1>(ref T1 a);
public delegate void RefFunc<T1, T2>(ref T1 a, ref T2 b);
public delegate void RefFunc<T1, T2, T3>(ref T1 a, ref T2 b, ref T3 c);

public delegate void RefComboFunc<T1, T2>(ref T1 a, T2 b);
public delegate void RefComboFunc<T1, T2, T3>(ref T1 a, T2 b, T3 c);
public delegate void RefComboFunc<T1, T2, T3, T4>(ref T1 a, T2 b, T3 c, T4 d);
#endregion

public static class HelperParent
{
    public const string AssetMenuPathStringPrefix = "_Imbossible/";
    public const string AssetCreatorPathStringPrefix = "Assets/_Imbossible/";

    #region Extension methods for: List<T>

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static List<T> Sorted<T>(this List<T> theList)
    {
        List<T> aList = new List<T>(theList);
        aList.Sort();
        return aList;
    }

    public static T FirstOrDefault<T>(this List<T> collection)
    {
        if (collection.Count > 0)
            return collection[0];
        return default(T);
    }

    public static T FirstOrDefault<T>(this List<T> collection, System.Predicate<T> predicate)
    {
        for (var enumerator = collection.GetEnumerator(); enumerator.MoveNext();)
        {
            if (predicate(enumerator.Current))
                return enumerator.Current;
        }
        return default(T);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool ContainsItem<T>(this IEnumerable<T> theList, T theNeedle) where T : class
    {
        foreach (T anElement in theList)
        {
            if (theNeedle.Equals(anElement))
            {
                return true;
            }
        }
        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string JoinToString<T>(this IEnumerable<T> theList, string theSeparator)
    {
        if (theList == null)
            return "";
        return string.Join(theSeparator, theList);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> InsertItem<T>(this IEnumerable<T> theList, T theItem, int thePosition)
    {
        int i = 0;
        bool anInserted = false;
        foreach (T anElement in theList)
        {
            if (i == thePosition)
            {
                yield return theItem;
                anInserted = true;
            }
            yield return anElement;
            i++;
        }

        if (!anInserted)
        {
            yield return theItem;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> AppendItem<T>(this IEnumerable<T> theList, T theItem)
    {
        foreach (T anElement in theList)
        {
            yield return anElement;
        }
        yield return theItem;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> Distinct<T>(this IEnumerable<T> theList)
    {
        List<T> aDistinctList = new List<T>();

        foreach (T anElement in theList)
        {
            if (!aDistinctList.Contains(anElement))
            {
                aDistinctList.Add(anElement);
                yield return anElement;
            }
        }

        yield break;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> Remove<T>(this IEnumerable<T> theMainList, T[] theListToRemove)
    {
        List<T> aListToRemove = new List<T>(theListToRemove);

        foreach (T anElement in theMainList)
        {
            if (!aListToRemove.Contains(anElement))
                yield return anElement;
        }
        yield break;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> Sorted<T>(this IEnumerable<T> theList)
    {
        List<T> aList = new List<T>(theList);
        aList.Sort();
        foreach (T aT in aList)
            yield return aT;
        yield break;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static IEnumerable<T> Sorted<T>(this IEnumerable<T> theList, Comparison<T> theComparison)
    {
        List<T> aList = new List<T>(theList);
        aList.Sort(theComparison);
        foreach (T aT in aList)
            yield return aT;
        yield break;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static List<T> ToDynList<T>(this IEnumerable<T> theList)
    {
        return new List<T>(theList);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static BetterList<T> ToBetterList<T>(this IEnumerable<T> theList)
    {
        return new BetterList<T>(theList);
    }
    #endregion

    #region Extension methods for: string

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static BetterList<string> split(this string stringToSplit, params char[] delimiters)
    {
        BetterList<string> results = new BetterList<string>();

        bool inQuote = false;
        StringBuilder currentToken = new StringBuilder();
        for (int index = 0; index < stringToSplit.Length; ++index)
        {
            char currentCharacter = stringToSplit[index];
            if (currentCharacter == '"')
            {
                // When we see a ", we need to decide whether we are
                // at the start or send of a quoted section...
                inQuote = !inQuote;
            }
            else if (delimiters.Contains(currentCharacter) && inQuote == false)
            {
                // We've come to the end of a token, so we find the token,
                // trim it and add it to the collection of results...
                string result = currentToken.ToString().Trim();
                if (result != "") results.Add(result);

                // We start a new token...
                currentToken = new StringBuilder();
            }
            else
            {
                // We've got a 'normal' character, so we add it to
                // the curent token...
                currentToken.Append(currentCharacter);
            }
        }

        // We've come to the end of the string, so we add the last token...
        string lastResult = currentToken.ToString().Trim();
        if (lastResult != "") results.Add(lastResult);

        return results;
    }

    public static bool IsValidFileName(this string expression, bool platformIndependent)
    {
        string sPattern = @"^(?!^(PRN|AUX|CLOCK\$|NUL|CON|COM\d|LPT\d|\..*)(\..+)?$)[^\x00-\x1f\\?*:\"";|/]+$";
        if (platformIndependent)
        {
            sPattern = @"^(([a-zA-Z]:|\\)\\)?(((\.)|(\.\.)|([^\\/:\*\?""\|<>\. ](([^\\/:\*\?""\|<>\. ])|([^\\/:\*\?""\|<>]*[^\\/:\*\?""\|<>\. ]))?))\\)*[^\\/:\*\?""\|<>\. ](([^\\/:\*\?""\|<>\. ])|([^\\/:\*\?""\|<>]*[^\\/:\*\?""\|<>\. ]))?$";
        }
        return (System.Text.RegularExpressions.Regex.IsMatch(expression, sPattern, System.Text.RegularExpressions.RegexOptions.CultureInvariant));
    }


    public static string RemoveSpecialCharacters(this string str)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        foreach (char c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }

    public static string Shortened(this string theString, int theMaxLength)
    {
        if (theString.Length > theMaxLength)
            return theString.Substring(0, theMaxLength - 2) + "..";
        return theString;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string Join(this string theSeparator, params string[] theItems)
    {
        return string.Join(theSeparator, theItems);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string Join(this string theSeparator, IEnumerable<string> theItems)
    {
        return string.Join(theSeparator, new List<string>(theItems).ToArray());
    }

    public static string RemoveRight(this string theString, char theSeparator)
    {
        string aStringCopy = "" + theString;

        while (aStringCopy.Length > 0 && aStringCopy[aStringCopy.Length - 1] != theSeparator)
        {
            aStringCopy = aStringCopy.Remove(aStringCopy.Length - 1);
        }
        return aStringCopy;
    }

    public static string GetFileName(this string c)
    {
        return Path.GetFileNameWithoutExtension(c);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string GetSubstringByString(this string c, string a, string b)
    {
        return c.Substring((c.IndexOf(a) + a.Length), (c.IndexOf(b) - c.IndexOf(a) - a.Length));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string GetSubstringByString(this string c, string a)
    {
        int index = (c.IndexOf(a) + a.Length);
        return c.Substring(index, c.Length - index);
    }

    public static string GetLastPart(this string theString, char theSeparator)
    {
        string[] aSplit = theString.Split(theSeparator);
        return aSplit[aSplit.Length - 1];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool TryParseString<T>(this string stringValue, ref T value)
    {
        Type typeT = typeof(T);

        if (typeT.IsPrimitive || typeT == typeof(string))
        {
            value = (T)Convert.ChangeType(stringValue, typeT);
            return true;
        }
        else if (typeT.IsEnum)
        {
            value = (T)System.Enum.Parse(typeT, stringValue); // Yeah, we're making an assumption
            return true;
        }
        else
        {
            IConvertible convertible = value as IConvertible;
            if (convertible != null)
            {
                value = (T)convertible.ToType(typeT, null);
                return true;
            }
        }

        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static System.DateTime ToDateTime(this string self)
    {
        try
        {
            return System.DateTime.ParseExact(self, "yyyy-MM-dd HH:mm:ss zzz", CultureInfo.InvariantCulture);
        }
        catch (System.Exception)
        {
            return System.DateTime.MinValue;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static string SeparateCamelCase(this string self, string separator = " ")
    {
        return Regex.Replace(self, "(?<=[a-z])([A-Z])", separator + "$1").Trim();
    }

    #endregion		

    #region Math Helper

    public const float PI2 = 6.28318530717958f;

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static byte Clamp(this byte value, byte min, byte max)
    {
        if (value < min)
            value = min;
        if (value > max)
            value = max;
        return value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float OptimizedSmoothStep(float from, float to, float t)
    {
        t = -2.0f * t * t * t + 3.0f * t * t;
        return to * t + from * (1.0f - t);
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float CalculateScaleForPerspectiveCamera(float fov, float zdist)
    {
        return Mathf.Abs(Mathf.Tan(Mathf.Deg2Rad * fov * 1) * zdist);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static uint ColorToUInt(this Color32 color)
    {
        return (uint)((color.a << 24) | (color.r << 16) |
                      (color.g << 8) | (color.b << 0));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static uint ColorRGBToUInt(this Color32 color)
    {
        return (uint)((255 << 24) | ((byte)color.r << 16) | ((byte)color.g << 8) | ((byte)color.b << 0));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static Color32 ToColor32(this uint color)
    {
        byte a = (byte)(color >> 24);
        byte r = (byte)(color >> 16);
        byte g = (byte)(color >> 8);
        byte b = (byte)(color >> 0);
        return new Color32(r, g, b, a);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static Color32 ToColor32Fast(this uint color)
    {
        byte[] values = BitConverter.GetBytes(color);
        return new Color32(values[0], values[1], values[2], values[3]);
    }
    

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float HorizontalAngle(this Vector3 direction)
    {
        return Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float GetFloatAsBetween(float knownFloat, float knownMin, float knownMax, float newMin, float newMax)
    {
        return (newMin + (((knownFloat - knownMin) * (newMax - newMin)) / (knownMax - knownMin)));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float ClampAngle(float angle, float min, float max)
    {
        do
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
        } while (angle < -360 || angle > 360);

        return Mathf.Clamp(angle, min, max);
    }

    public static string FormatFileSize(double size)
    {
        string unit = "B";
        if (size > 921) // 1024 * 90%
        {
            unit = "KB";
            size /= 1024;
        }
        if (size > 921) // 1024 * 1024 * 90%
        {
            unit = "MB";
            size /= 1024;
        }
        if (size > 921) // 1024 * 1024 * 1024 * 90%
        {
            unit = "GB";
            size /= 1024;
        }

        return Math.Round(size, 2) + unit;
    }

    public static int IndexOf(this StringBuilder sb, char value, int startIndex)
    {            
        int maxSearchLength = sb.Length + 1;

        for (int i = startIndex; i < maxSearchLength; ++i)
        {
            if (sb[i] == value)
                return i;
        }
        return -1;
    }

    public static string GetGoldString(this int _Gold)
    {
        int Gold = (int)(_Gold * 0.0001f);
        int Silver = (int)(_Gold * 0.01) - Gold * 100;
        int Copper = _Gold - ((Gold * 10000) + (_Gold * 100));
        return string.Format("[GOLD] {0} [SILVER] {1} [COPPER] {2}",Gold,Silver,Copper);
    }

    #endregion

    #region Serialized PropertyHelper (Reflection Get Object)
#if UNITY_EDITOR
    public static object GetObject(SerializedProperty prop)
    {
        string path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        string[] elements = path.Split('.');
        foreach (string element in elements.Take(elements.Length))
        {
            if (element.Contains("["))
            {
                string elementName = element.Substring(0, element.IndexOf("["));
                int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue(obj, elementName, index);
            }
            else
            {
                obj = GetValue(obj, element);
            }
        }
        return obj;
    }

    public static object GetParent(SerializedProperty prop)
    {
        string path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        string[] elements = path.Split('.');
        foreach (string element in elements.Take(elements.Length - 1))
        {
            if (element.Contains("["))
            {
                string elementName = element.Substring(0, element.IndexOf("["));
                int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = GetValue(obj, elementName, index);
            }
            else
            {
                obj = GetValue(obj, element);
            }
        }
        return obj;
    }

    public static object GetValue(object source, string name)
    {
        if (source == null)
            return null;
        Type type = source.GetType();
        FieldInfo f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            PropertyInfo p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }
        return f.GetValue(source);
    }

    public static object GetValue(object source, string name, int index)
    {
        System.Collections.IEnumerable enumerable = GetValue(source, name) as System.Collections.IEnumerable;
        System.Collections.IEnumerator enm = enumerable.GetEnumerator();
        while (index-- >= 0)
            enm.MoveNext();
        return enm.Current;
    }
#endif
    #endregion

    #region Helper ScriptableObject
#if UNITY_EDITOR
    public static T CreateScritptableObjectAsset<T>(string name = "") where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        if(string.IsNullOrEmpty(name))
            name = "New " + typeof(T).ToString();

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + name + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;
    }
#endif
    #endregion

    #region EnumHelper
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool TryParse<T>(this Enum theEnum, string valueToParse, out T returnValue)
        where T : struct, IComparable, IConvertible, IFormattable
    {
        return CachedEnumTypes<T>.stringToValue.TryGetValue(valueToParse, out returnValue);
    }

    #endregion

    #region AddComponentHelper
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public T GetOrAddComponent<T>(this Component child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
            result = child.gameObject.AddComponent<T>();
        return result;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    static public T GetOrAddComponent<T>(this GameObject child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
            result = child.AddComponent<T>();
        return result;
    }
    #endregion

    #region PerformanceHelper (IsVisible)

    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }
    #endregion

    #region Color
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static int EncodeColor(this Color self)
    {
        Color32 color32 = self;

        int c = 0;

        c |= color32.a << 24;
        c |= color32.r << 16;
        c |= color32.g << 8;
        c |= color32.b;

        return c;
    }

    /// <summary>
    /// Decompress a int into a color object.
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    /// [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static Color DecodeColor(int color)
    {
        byte colorAlpha = (byte)((color >> 24) & 0xFF);
        byte colorRed = (byte)((color >> 16) & 0xFF);
        byte colorGreen = (byte)((color >> 8) & 0xFF);
        byte colorBlue = (byte)(color & 0xFF);

        return new Color32(colorRed, colorGreen, colorBlue, colorAlpha);
    }
    #endregion

    #region Reflection

    public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
    {
        var type = enumVal.GetType();
        var memInfo = type.GetMember(enumVal.ToString());
        var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
        return (attributes.Length > 0) ? (T)attributes[0] : null;
    }

    public static TField[] GetFieldValuesOfType<TField>(object instance) where TField : class
    {
        return GetFieldValuesOfType<TField>(instance, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
    }

    public static TField[] GetFieldValuesOfType<TField>(object instance, BindingFlags bindingFlags) where TField : class
    {
        FieldInfo[] fields = instance.GetType().GetFields(bindingFlags);
        int size = fields.Count(f => typeof(TField).IsAssignableFrom(f.FieldType));
        TField[] values = new TField[size];

        int i = 0;
        foreach (var field in fields)
        {
            if (typeof(TField).IsAssignableFrom(field.FieldType))
            {
                values[i++] = field.GetValue(instance) as TField;
            }
        }

        return values;
    }

    public static BetterList<Type> GetTypesWithAttribute(this Type attributeType)
    {
        return attributeType.Assembly.GetTypesWithAttribute(attributeType);
    }

    public static BetterList<Type> GetTypesWithAttribute(this Assembly assembly, Type attributeType)
    {
       // System.Attribute.GetCustomAttributes()

        BetterList<Type> returnList = new BetterList<Type>();
        Type[] types = assembly.GetTypes();
        for(int i = 0, iMax = types.Length; i < iMax; i++)
        {
            if (types[i].GetCustomAttributes(attributeType,false).Length > 0)
                returnList.Add(types[i]);
        }
        return returnList;
    }

    public static Type GetUnderlyingType(this MemberInfo member)
    {
        switch (member.MemberType)
        {
            case MemberTypes.Event:
                return ((EventInfo)member).EventHandlerType;
            case MemberTypes.Field:
                return ((FieldInfo)member).FieldType;
            case MemberTypes.Method:
                return ((MethodInfo)member).ReturnType;
            case MemberTypes.Property:
                return ((PropertyInfo)member).PropertyType;
            default:
                throw new ArgumentException
                (
                 "Input MemberInfo must be if type EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
                );
        }
    }

    #endregion

    #region Handles
    #endregion

    #region GameObject

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool IsTagged(this Component component, ref string[] tags)
    {
        if (tags.Length <= 0)
            return true;
        foreach (string str in tags)
        {
            if (component.CompareTag(str))
                return true;
        }
        return false;
    }

    public static GameObject CreateGameObjectWithHideFlags(string name, HideFlags flags, params Type[] components)
    {
        GameObject gameObject = new GameObject(name);
        gameObject.hideFlags = flags;
        // gameObject.AddComponent(typeof(Transform));
        for (int i = 0; i < components.Length; i++)
        {
            Type componentType = components[i];
            gameObject.AddComponent(componentType);
        }
        return gameObject;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void DestroyMeSafe(this UnityEngine.Object o)
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
            UnityEngine.Object.DestroyImmediate(o);
        else
#endif
            UnityEngine.Object.Destroy(o);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void SetLayerRecursively(this UnityEngine.GameObject parent, int Layer)
    {
        if (null == parent)
            return;
        parent.layer = Layer;

        foreach (Transform trans in parent.transform.GetComponentsInChildren<Transform>(true))
            trans.gameObject.layer = Layer;
    }

    #endregion
    
    #region Animator
    public static AnimationClip GetAnimationClip(this Animator animator, string AnimationName)
    {
        for (int i = 0, iMax = animator.runtimeAnimatorController.animationClips.Length; i < iMax; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == AnimationName)
                return animator.runtimeAnimatorController.animationClips[i];
        }
        return null;
    }
    #endregion

    #region Bounds 

    public static Rect GetGUIBounds(this Bounds b, Camera cam)
    {
        Vector3[] pts = new Vector3[8];

        // Vector3 ScreenPoints = BaseCamera.Instance._camera.WorldToViewportPoint(b.center);

        // BoundsVisible = ScreenPoints.z > 0 && ScreenPoints.x < 1 && ScreenPoints.x > 0 && ScreenPoints.y < 1 && ScreenPoints.y > 0;
        //  if (!BoundsVisible) return;

        pts[0] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y + b.extents.y, b.center.z + b.extents.z));
        pts[1] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y + b.extents.y, b.center.z - b.extents.z));
        pts[2] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y - b.extents.y, b.center.z + b.extents.z));
        pts[3] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y - b.extents.y, b.center.z - b.extents.z));
        pts[4] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y + b.extents.y, b.center.z + b.extents.z));
        pts[5] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y + b.extents.y, b.center.z - b.extents.z));
        pts[6] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y - b.extents.y, b.center.z + b.extents.z));
        pts[7] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y - b.extents.y, b.center.z - b.extents.z));

        pts[0].y = Screen.height - pts[0].y;
        Vector3 min = pts[0];
        Vector3 max = pts[0];
        for (int i = 1; i < pts.Length; i++)
        {
            pts[i].y = Screen.height - pts[i].y;
            min = Vector3.Min(min, pts[i]);
            max = Vector3.Max(max, pts[i]);
        }

        Rect r = GUIUtility.ScreenToGUIRect(Rect.MinMaxRect(min.x, min.y, max.x, max.y));

        return r;
    }

    //http://answers.unity3d.com/questions/49943/is-there-an-easy-way-to-get-on-screen-render-size.html
    public static Rect GetGUIBoundsSimple(this Bounds b, Camera cam)
    {
        Vector3 origin = cam.WorldToScreenPoint(new Vector3(b.min.x, b.max.y, b.center.z));
        Vector3 extents = cam.WorldToScreenPoint(new Vector3(b.max.x, b.min.y, b.center.z));

        return new Rect(origin.x, Screen.height - origin.y, extents.x - origin.x, origin.y - extents.y);
    }

    public static Rect GetGUIBoundsSimpleUGUI(this Bounds b, Camera cam)
    {
        Vector3 origin = cam.WorldToScreenPoint(new Vector3(b.min.x, b.max.y, b.center.z));
        Vector3 extents = cam.WorldToScreenPoint(new Vector3(b.max.x, b.min.y, b.center.z));

        return new Rect(origin.x, origin.y, extents.x - origin.x, origin.y - extents.y);
    }

    #endregion

    #region SpriteHelper
    public static bool GetSpritePixelColorUnderMousePointer(this SpriteRenderer spriteRenderer, Vector2 mousePos, Camera cam, out Color color)
    {
        color = new Color();
        Vector2 viewportPos = cam.ScreenToViewportPoint(mousePos);
        if (viewportPos.x < 0.0f || viewportPos.x > 1.0f || viewportPos.y < 0.0f || viewportPos.y > 1.0f) return false; // out of viewport bounds
                                                                                                                        // Cast a ray from viewport point into world
        Ray ray = cam.ViewportPointToRay(viewportPos);

        // Check for intersection with sprite and get the color
        return IntersectsSprite(spriteRenderer, ray, out color);
    }
    //TODO if to imperformant and we need write/read, then render the sprite firstly to a second camera and can simply convert the mousepos to bounds pos
    public static bool IntersectsSprite(this SpriteRenderer spriteRenderer, Ray ray, out Color color)
    {
        color = new Color();
        Sprite sprite = spriteRenderer.sprite;

        Texture2D texture = sprite.texture;
        // Check atlas packing mode
        if (sprite.packed && sprite.packingMode == SpritePackingMode.Tight)
        {
            // Cannot use textureRect on tightly packed sprites
            Debug.LogError("SpritePackingMode.Tight atlas packing is not supported!");
            // TODO: support tightly packed sprites
            return false;
        }
        // Craete a plane so it has the same orientation as the sprite transform
        Plane plane = new Plane(spriteRenderer.transform.forward, spriteRenderer.transform.position);
        // Intersect the ray and the plane
        float rayIntersectDist; // the distance from the ray origin to the intersection point
        if (!plane.Raycast(ray, out rayIntersectDist)) return false; // no intersection
                                                                     // Convert world position to sprite position
                                                                     // worldToLocalMatrix.MultiplyPoint3x4 returns a value from based on the texture dimensions (+/- half texDimension / pixelsPerUnit) )
                                                                     // 0, 0 corresponds to the center of the TEXTURE ITSELF, not the center of the trimmed sprite textureRect
        Vector3 spritePos = spriteRenderer.worldToLocalMatrix.MultiplyPoint3x4(ray.origin + (ray.direction * rayIntersectDist));
        Rect textureRect = sprite.textureRect;
        float pixelsPerUnit = sprite.pixelsPerUnit;
        float halfRealTexWidth = texture.width * 0.5f; // use the real texture width here because center is based on this -- probably won't work right for atlases
        float halfRealTexHeight = texture.height * 0.5f;
        // Convert to pixel position, offsetting so 0,0 is in lower left instead of center
        int texPosX = (int)(spritePos.x * pixelsPerUnit + halfRealTexWidth);
        int texPosY = (int)(spritePos.y * pixelsPerUnit + halfRealTexHeight);
        // Check if pixel is within texture
        if (texPosX < 0 || texPosX < textureRect.x || texPosX >= Mathf.FloorToInt(textureRect.xMax)) return false; // out of bounds
        if (texPosY < 0 || texPosY < textureRect.y || texPosY >= Mathf.FloorToInt(textureRect.yMax)) return false; // out of bounds
                                                                                                                   // Get pixel color
        color = texture.GetPixel(texPosX, texPosY);
        return true;
    }

    #endregion

    #region Vector2
    [MethodImpl(MethodImplOptions.AggressiveInlining)] [System.Runtime.TargetedPatchingOptOut("")]
    public static Vector2 SetValueInPercent(this Vector2 vec, float percent)
    {
        vec.x = vec.y * percent;
        return vec;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)] [System.Runtime.TargetedPatchingOptOut("")]
    public static float GetValueInPercent(this Vector2 vec)
    {
        return vec.x/vec.y;
    }

    public static Vector2 UnitRotateTowards(Vector2 current, Vector2 target, float maxRadiansDelta)
    {
        float currentAngle = Mathf.Atan2(current.y, current.x) * Mathf.Rad2Deg;
        float targetAngle = Mathf.Atan2(target.y, target.x) * Mathf.Rad2Deg;

        float angle = Mathf.MoveTowardsAngle(currentAngle, targetAngle, maxRadiansDelta * Mathf.Rad2Deg) * Mathf.Deg2Rad;

        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
    }

    public static bool IsBetweenVectors(Vector3 direction, Vector3 lowerBound, Vector3 upperBound)
    {
        return IsOnVectorSide(direction, lowerBound, upperBound) && IsOnVectorSide(direction, upperBound, lowerBound);
    }

    public static bool IsOnVectorSide(Vector3 direction, Vector3 vector, Vector3 sidePivot)
    {
        return Vector3.Dot(Vector3.Cross(vector, direction), Vector3.Cross(vector, sidePivot)) > 0.0f;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static Vector2 Rotate(this Vector2 vec, float degrees)
    {
        return Quaternion.Euler(0, 0, degrees) * vec;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static Vector3 RotateThisVector(this Vector2 v, float angle)
    {
        float sin = Mathf.Sin(angle);
        float cos = Mathf.Cos(angle);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (cos * ty) + (sin * tx);

        return v;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool IsZero(this Vector3 vec)
    {
        return vec.sqrMagnitude <= 0.001f;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static bool IsZero(this Vector2 vec)
    {
        return vec.sqrMagnitude <= 0.001f;
    }
    #endregion

    #region FileSystemHelper
    public static void CopyDirectory(string fromDirectory, string toDirectory)
    {
        Directory.CreateDirectory(toDirectory);

        fromDirectory = Path.GetFullPath(fromDirectory);
        string[] files = Directory.GetFiles(fromDirectory, "*.*", SearchOption.AllDirectories);
        string[] directories = Directory.GetDirectories(fromDirectory, "*.*", SearchOption.AllDirectories);

        foreach (string directory in directories)
        {
            string directoryPath = Path.GetFullPath(directory);
            string newDirectoryPath = directoryPath.Replace(fromDirectory, toDirectory);

            Directory.CreateDirectory(newDirectoryPath);
        }

        foreach (string file in files)
        {
            string filePath = Path.GetFullPath(file);
            string newFilePath = filePath.Replace(fromDirectory, toDirectory);

            File.Copy(filePath, newFilePath);
        }
    }

    public static DirectoryInfo CreateDirectoryPathIfNotExists(string path)
    {
        if (path.Contains("/"))
            path = path.Replace("/", "\\");

        string[] pathParts = path.Split('\\');


        for (int i = 0; i < pathParts.Length; i++)
        {
            if (i > 0)
                pathParts[i] = Path.Combine(pathParts[i - 1], pathParts[i]);

            if (!Directory.Exists(pathParts[i]))
                Directory.CreateDirectory(pathParts[i]);
        }

        return new DirectoryInfo(path);
    }
    #endregion

    #region Rigidbody / Physics
    public static Vector3 predictVelocity(this Rigidbody2D rigid, int cycles = 100, float gravityModifier = 1)
    {
        return predictRigidbodyVelocity(rigid.velocity, cycles, rigid.drag, true, gravityModifier);
    }

    public static Vector3 predictPosition(this Rigidbody2D rigid, int cycles = 100, float gravityModifier = 1)
    {
        return predictRigidbodyPosition(rigid.position, rigid.velocity, cycles, rigid.drag, true, gravityModifier);
    }

    public static PredictedPositionData predictPositions(this Rigidbody2D rigid, int cycles = 100, float gravityModifier = 1)
    {
        return predictAllRigidbodyPositionsTillCycle(rigid.position, rigid.velocity, cycles, rigid.drag, true, gravityModifier);
    }

    ///<summary>A small amount of time faster than predictRigidbodyVelocityWithoutLoop in the benchmark with cycles=100 (this method uses a lot of simple calculations which are easy and fast to calculate for your CPU).
    public static Vector3 predictRigidbodyVelocity(Vector3 startVelocity, int cycles, float dragValue, bool _2DGravity, float gravityModifier)
    {
        //timestep
        float t = Time.fixedDeltaTime;
        //drag how it is used by the unity physics
        float d = Mathf.Clamp01(1.0f - (dragValue * t));
        //gravity with modifier
        Vector3 g;
        if (_2DGravity) g = Physics2D.gravity * gravityModifier;
        else g = Physics.gravity * gravityModifier;

        Vector3 currentVelocity = startVelocity;

        for (int currentCycle = 0; currentCycle < cycles; currentCycle++)
        {
            currentVelocity = d * (currentVelocity + t * g);
        }

        return currentVelocity;
    }

    ///<summary>A lot faster than predictRigidbodyPositionWithoutLoop in the benchmark with cycles=100 (this method uses a lot of simple calculations which are easy and fast to calculate for your CPU, incredibly fast method (like 10x faster)).
    public static Vector3 predictRigidbodyPosition(Vector3 startPosition, Vector3 startVelocity, int cycles, float dragValue, bool _2DGravity, float gravityModifier)
    {
        //timestep
        float t = Time.fixedDeltaTime;
        //drag how it is used by the unity physics
        float d = Mathf.Clamp01(1.0f - (dragValue * t));
        //gravity with modifier
        Vector3 g;
        if (_2DGravity) g = Physics2D.gravity * gravityModifier;
        else g = Physics.gravity * gravityModifier;

        Vector3 currentPosition = startPosition;
        Vector3 currentVelocity = startVelocity;


        for (int currentCycle = 0; currentCycle < cycles; currentCycle++)
        {
            currentPosition = currentPosition + t * d * currentVelocity + t * d * t * g;
            //update velocity AFTER the position to use the velocity from last cycle
            currentVelocity = d * (currentVelocity + t * g);
        }

        return currentPosition;
    }

    ///<summary>PredictedPositionData wraps all position and velocity values till the input cycle! (uses the same algorithm as predictRigidbodyPosition but saves all temporary values in the wrapper class)
    public static PredictedPositionData predictAllRigidbodyPositionsTillCycle(Vector3 startPosition, Vector3 startVelocity, int cycles, float dragValue, bool _2DGravity, float gravityModifier)
    {
        //timestep
        float t = Time.fixedDeltaTime;
        //drag how it is used by the unity physics
        float d = Mathf.Clamp01(1.0f - (dragValue * t));
        //gravity with modifier
        Vector3 g;
        if (_2DGravity) g = Physics2D.gravity * gravityModifier;
        else g = Physics.gravity * gravityModifier;

        Vector3 currentPosition = startPosition;
        Vector3 currentVelocity = startVelocity;
        PredictedPositionData data = new PredictedPositionData(cycles, _2DGravity);
        data.predictedPosition[0] = currentPosition;
        data.predictedVelocity[0] = currentVelocity;


        for (int currentCycle = 0; currentCycle < cycles; currentCycle++)
        {
            currentPosition = currentPosition + t * d * currentVelocity + t * d * t * g;
            //update velocity AFTER the position to use the velocity from last cycle
            currentVelocity = d * (currentVelocity + t * g);

            data.predictedPosition[currentCycle + 1] = currentPosition;
            data.predictedVelocity[currentCycle + 1] = currentVelocity;
        }

        return data;
    }

    public static PredictedPositionData predict2DTrajectory(Vector2 startPos, Vector2 direction, float force, float deltaTime, LayerMask collisionMask, int precision = 15, int pointDistance = 1)
    {
        PredictedPositionData data = new PredictedPositionData(precision, true);
        data.predictedPosition[0] = startPos;
        Vector3 pointVelocity = direction * force * deltaTime;

        for (int i = 1; i < precision; i++)
        {
            float pointTime;
            if (pointVelocity.sqrMagnitude != 0)
                pointTime = pointDistance / pointVelocity.magnitude;
            else
                pointTime = 0;

            pointVelocity = pointVelocity + (Vector3)Physics2D.gravity * pointTime;

            RaycastHit2D hit = Physics2D.Raycast(data.predictedPosition[i - 1], pointVelocity, pointDistance, collisionMask.value);
            if (hit.collider)
            {
                data.hitCollider = hit.collider;

                data.predictedPosition[i] = data.predictedPosition[i - 1] + pointVelocity.normalized * hit.distance * 0.92f;
                pointVelocity = pointVelocity - (Vector3)Physics2D.gravity * (pointDistance - hit.distance) / pointVelocity.magnitude;
                pointVelocity = Vector3.Reflect(pointVelocity, hit.normal);
            }
            else
            {
                data.predictedPosition[i] = data.predictedPosition[i - 1] + pointVelocity * pointTime;
            }
            data.predictedVelocity[i] = pointVelocity;
        }
        return data;
    }

    public class PredictedPositionData
    {
        public bool is2DData;

        ///<summary>Predicted data (EXAMPLE: predictedPosition[4] means predicted position after 4 cycles, [0] is current position)
        public Vector3[] predictedPosition;
        ///<summary>Predicted data (EXAMPLE: predictedVelocity[4] means predicted velocity after 4 cycles, [0] is current velocity)
        public Vector3[] predictedVelocity;

        public Collider2D hitCollider;

        public PredictedPositionData(int predictedCycles, bool is2DData)
        {
            this.is2DData = is2DData;
            predictedPosition = new Vector3[predictedCycles + 1];
            predictedVelocity = new Vector3[predictedCycles + 1];
        }
    }


    #endregion //Rigidbody

    #region ShaderHelper
    // Graphics device version identifiers
    public const int OGL = 0;
    public const int D3D9 = 1;
    public const int D3D11 = 2;

    // Current graphics device version: 0 = OpenGL or unknown (default), 1 = Direct3D 9, 2 = Direct3D 11
    private static int graphicsDeviceVersion = -1;

    public static int DetermineGraphicsVersion()
    {
        if (graphicsDeviceVersion != -1)
            return graphicsDeviceVersion;

        // Determine graphics device version
        string version = SystemInfo.graphicsDeviceVersion.ToLower();
        if (version.Contains("direct3d") || version.Contains("directx"))
        {
            if (version.Contains("direct3d 11") || version.Contains("directx 11")) { graphicsDeviceVersion = D3D11; }
            else { graphicsDeviceVersion = D3D9; }
        }
#if UNITY_EDITOR_WIN && (UNITY_ANDROID || UNITY_IOS)
			else if (version.Contains("emulated"))
			{
				graphicsDeviceVersion = D3D9;
			}
#endif
        else
        {
            graphicsDeviceVersion = OGL;
        }
        return graphicsDeviceVersion;
    }

    public static bool CheckImageEffectSupported()
    {
        // Image Effects supported?
        if (!SystemInfo.supportsImageEffects)
        {
            Debug.LogError("PostProcess : Image effects is not supported on this platform!");
            return false;
        }

        // Required Render Texture Format supported?
        if (!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB32))
        {
            Debug.LogError("PostProcess : RenderTextureFormat.ARGB32 is not supported on this platform!");
            return false;
        }
        //Shader.WarmupAllShaders

        return true;
    }

    public static int GetAA(this Camera cam)
    {
        int aa = QualitySettings.antiAliasing;
        if (aa == 0) { aa = 1; }

        // Reset aa value to 1 in case camera is in DeferredLighting or DeferredShading Rendering Path
        if (cam.actualRenderingPath == RenderingPath.DeferredLighting || cam.actualRenderingPath == RenderingPath.DeferredShading) { aa = 1; }

        return aa;
    }

    private static Mesh quad;

    public static readonly Matrix4x4 identityMatrix = Matrix4x4.identity;

    public static Mesh GetQuad(Color clearColor)
    {
        if (quad != null)
            return quad;
        quad = new Mesh();

        float y1 = 1f;
        float y2 = -1f;

        if (HelperParent.DetermineGraphicsVersion() == HelperParent.OGL)
        {
            y1 = -1f;
            y2 = 1f;
        }

        quad.vertices = new Vector3[]
        {
                new Vector3(-1f, y1, 0f), // Bottom-Left
				new Vector3(-1f, y2, 0f), // Upper-Left
				new Vector3( 1f, y2, 0f), // Upper-Right
				new Vector3( 1f, y1, 0f)  // Bottom-Right
        };

        quad.uv = new Vector2[]
        {
                new Vector2(0f, 0f),
                new Vector2(0f, 1f),
                new Vector2(1f, 1f),
                new Vector2(1f, 0f)
        };

        quad.colors = new Color[]
        {
                clearColor,
                clearColor,
                clearColor,
                clearColor
        };

        quad.triangles = new int[] { 0, 1, 2, 2, 3, 0 };
        return quad;
    }

    #endregion

    #region Texture

    public static Color32[] GetPixels32WithRenderTexture(this Texture2D texture, int mipLevel = 0)
    {
        int mipWidth = Mathf.Max(1, texture.width >> mipLevel);
        int mipHeight = Mathf.Max(1, texture.height >> mipLevel);

        Texture2D tempTexture = new Texture2D(mipWidth, mipHeight, TextureFormat.ARGB32, false);

        GL.PushMatrix();
        Rect textureRect = new Rect(0f, 0f, mipWidth, mipHeight);
        RenderTexture rt = RenderTexture.GetTemporary(mipWidth, mipHeight, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
        Graphics.Blit(texture, rt);
        RenderTexture.active = rt;
        tempTexture.ReadPixels(textureRect, 0, 0, false);
        RenderTexture.active = null;
        RenderTexture.ReleaseTemporary(rt);
        GL.PopMatrix();

        Color32[] pixelsBuffer = tempTexture.GetPixels32(0);
        UnityEngine.Object.DestroyImmediate(tempTexture);

        return pixelsBuffer;
    }

    #endregion

    #region RectTransform
   
    public enum RectTransformEdge
    {
        Pivot,
        BottomLeft,
        TopLeft,
        TopRight,
        BottomRight,
        TopCenter
    }

    public static void SetRect(this RectTransform trs, float left, float top, float right, float bottom)
    {
        trs.offsetMin = new Vector2(left, bottom);
        trs.offsetMax = new Vector2(-right, -top);
    }

    public static void SetRect(this RectTransform trs, Rect rectangle)
    {
        trs.offsetMin = new Vector2(rectangle.xMin, rectangle.yMax);
        trs.offsetMax = new Vector2(-rectangle.xMax, -rectangle.yMin);
    }

    public static Vector3 GetWorldSpaceEdge(this RectTransform trs, RectTransformEdge AnchorPoint)
    {
        switch(AnchorPoint)
        {
            case RectTransformEdge.Pivot:
                return trs.position;
            case RectTransformEdge.BottomLeft:
                return new Vector3(trs.position.x - trs.offsetMin.x,trs.position.y - trs.offsetMin.y,trs.position.z);
            case RectTransformEdge.TopLeft:
                return new Vector3(trs.position.x - trs.offsetMin.x,trs.position.y + trs.offsetMax.y,trs.position.z);
            case RectTransformEdge.TopCenter:
                return new Vector3(trs.position.x ,trs.position.y + trs.GetWorldRect(FastVectorAccess.Vector2One).yMin,trs.position.z);
            case RectTransformEdge.TopRight:
                return new Vector3(trs.position.x + trs.offsetMax.x,trs.position.y + trs.offsetMax.y,trs.position.z);
            case RectTransformEdge.BottomRight:
                return new Vector3(trs.position.x + trs.offsetMax.x,trs.position.y - trs.offsetMin.y,trs.position.z);
        }
        return trs.position;
    }

    /// <summary>
     /// Converts RectTransform.rect's local coordinates to world space
     /// Usage example RectTransformExt.GetWorldRect(myRect, Vector2.one);
     /// </summary>
     /// <returns>The world rect.</returns>
     /// <param name="rt">RectangleTransform we want to convert to world coordinates.</param>
     /// <param name="scale">Optional scale pulled from the CanvasScaler. Default to using Vector2.one.</param>
     static public Rect GetWorldRect (this RectTransform rt, Vector2 scale) {
         // Convert the rectangle to world corners and grab the top left
         Vector3[] corners = new Vector3[4];
         rt.GetWorldCorners(corners);
         Vector3 topLeft = corners[0];
 
         // Rescale the size appropriately based on the current Canvas scale
         Vector2 scaledSize = new Vector2(scale.x * rt.rect.size.x, scale.y * rt.rect.size.y);
 
         return new Rect(topLeft, scaledSize);
     }
    #endregion
}

#region InputHelper

//can be used for button hotkeys like saving or opening a new map:)
[System.Serializable()]
public class InputCombo
{
    private static BetterList<InputCombo> inputsToCheck = new BetterList<InputCombo>();
    static InputCombo()
    {
        inputsToCheck.Clear();
    }

    public static void CheckInputs()
    {
        if (Input.anyKeyDown)
        {
            for (int i = 0, iMax = inputsToCheck.size; i < iMax; i++)
                inputsToCheck[i].FireEvent();
        }
    }

    public static void AddCheck(InputCombo combo)
    {
        inputsToCheck.Add(combo);
    }


    public KeyCode keyCode = KeyCode.None;
    public CommandKey commandKeys = CommandKey.None;
    public System.Action eventToFire;

    private void FireEvent()
    {
        if (Input.GetKeyDown(keyCode) && commandKeys.isPressed() && eventToFire != null)
            eventToFire();
    }
}

public static class CommandKeyInput
{
    public static CommandKey currentCommandKeys = CommandKey.None;

    public static bool Has(CommandKey value)
    {
        return currentCommandKeys.Has(value);
    }

    public static bool isPressed(this CommandKey value)
    {
        return currentCommandKeys.Has(value);
    }

    public static bool Has(this CommandKey type, CommandKey value)
    {
        try
        {
            return ((type & value) == value);
        }
        catch
        {
            return false;
        }
    }

    public static void UpdateCommandKeys()
    {
        CommandKey tempCommandKey = CommandKey.None;
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            tempCommandKey |= CommandKey.Control;
        }
        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        {
            tempCommandKey |= CommandKey.Alt;
        }
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            tempCommandKey |= CommandKey.Shift;
        }
        currentCommandKeys = tempCommandKey;
    }
}

[Flags()]
public enum CommandKey
{
    None = 0,
    Control = 1 << 2,
    Shift = 1 << 4,
    Alt = 1 << 8,
    All = Control | Shift | Alt
}

[System.Serializable()]
public struct EditorInput
{
    public CommandKey CommandKeys;
    public KeyCode KeyPress;
}

#endregion

#region BiDicionary
public class UnityObjectComparer : IEqualityComparer<UnityEngine.Object>
{
    public readonly static UnityObjectComparer instance;

    static UnityObjectComparer()
    {
        instance = new UnityObjectComparer();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)] [System.Runtime.TargetedPatchingOptOut("")]
    public bool Equals(UnityEngine.Object x, UnityEngine.Object y)
    {
        return x.GetInstanceID() == y.GetInstanceID();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)] [System.Runtime.TargetedPatchingOptOut("")]
    public int GetHashCode(UnityEngine.Object obj)
    {
        return obj.GetInstanceID();
    }
}

public class IntEqalityComparer : IEqualityComparer<int>
{
    public readonly static IntEqalityComparer instance;

    static IntEqalityComparer()
    {
        instance = new IntEqalityComparer();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    [System.Runtime.TargetedPatchingOptOut("")]
    public bool Equals(int x, int y)
    {
        return x == y;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    [System.Runtime.TargetedPatchingOptOut("")]
    public int GetHashCode(int obj)
    {
        return obj;
    }
}

public class UIntEqalityComparer : IEqualityComparer<uint>
{
    public readonly static UIntEqalityComparer instance;

    static UIntEqalityComparer()
    {
        instance = new UIntEqalityComparer();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    [System.Runtime.TargetedPatchingOptOut("")]
    public bool Equals(uint x, uint y)
    {
        return x == y;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    [System.Runtime.TargetedPatchingOptOut("")]
    public int GetHashCode(uint obj)
    {
        return (int)obj;
    }
}

public class BiDictionary<TFirst, TSecond>
{
    IDictionary<TFirst, TSecond> firstToSecond = new Dictionary<TFirst, TSecond>();
    IDictionary<TSecond, TFirst> secondToFirst = new Dictionary<TSecond, TFirst>();

    public void Add(TFirst first, TSecond second)
    {
        if (firstToSecond.ContainsKey(first) ||
            secondToFirst.ContainsKey(second))
        {
            throw new ArgumentException("Duplicate first or second");
        }
        firstToSecond.Add(first, second);
        secondToFirst.Add(second, first);
    }

    public bool TryGetByFirst(TFirst first, out TSecond second)
    {
        return firstToSecond.TryGetValue(first, out second);
    }

    public bool TryGetBySecond(TSecond second, out TFirst first)
    {
        return secondToFirst.TryGetValue(second, out first);
    }
}

#endregion

#region FastRandom
public static class FastRandom {
    //new DateTime.now().millisecondsSinceEpoch & 0x7FFF;
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static float GetRandomValue(int seed, float min, float max)
    {
        seed = (214013 * seed + 2531011) & 0x7FFFFFFF;
        seed = (seed >> 16) & 0x7FFF;
        float temp = seed / 0x7FFF;
        return min + temp * (max-min);
    }
}
#endregion

#region FastVectorAccess
public static class FastVectorAccess {
    public static Vector3 Vector3One = new Vector3(1,1,1);
    public static Vector3 Vector3Zero = new Vector3(0,0,0);

    public static Vector2 Vector2One = new Vector2(1,1);
    public static Vector2 Vector2Zero = new Vector2(0,0);

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void SetZeroFast(this Vector3 vec)
    {
        vec = Vector3Zero;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void SetOneFast(this Vector3 vec)
    {
        vec = Vector3One;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void SetZeroFast(this Vector2 vec)
    {
        vec = Vector2Zero;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void SetOneFast(this Vector2 vec)
    {
        vec = Vector2One;
    }
}
#endregion

#region StringToFormula

public static class StringToFormula
{
    private static Dictionary<char, Func<float, float, float>> operationDictionary = new Dictionary<char, Func<float, float, float>>()
    {
        {'-',  (a1, a2) => a1 - a2 },
        {'+',  (a1, a2) => a1 + a2 },
        {'/',  (a1, a2) => a1 / a2 },
        {'*',  (a1, a2) => a1 * a2 },
        {':',  (a1, a2) => a1 > a2 ? a1 : a2},
        {'?',  (a1, a2) => a1 > a2 ? a1 : 0 },
        {'^',  (a1, a2) => Mathf.Pow(a1, a2)},
        {'>',  (a1, a2) => a1 > a2 ? 1 : 0 },
        {'<',  (a1, a2) => a1 < a2 ? 1 : 0 },
        {'%',  (a1, a2) => a1 % a2 },
        {'R',  (a1, a2) => UnityEngine.Random.Range(a1,a2) }
    };
    private static Func<float, float, float>[] _operations;
    private static string _operators2;
    private static string operators = "()";

    static StringToFormula()
    {
        _operations = operationDictionary.Values.ToArray();
        _operators2 = string.Concat(operationDictionary.Keys);
        operators += _operators2;
    }

    public static float Evaluate(string expression)
    {
        List<string> tokens = getTokens(expression);
        Stack<float> operandStack = new Stack<float>();
        Stack<char> operatorStack = new Stack<char>();
        int tokenIndex = 0;

        while (tokenIndex < tokens.Count)
        {
            string token = tokens[tokenIndex];
            switch (token)
            {
                case "(":
                    string subExpr = getSubExpression(tokens, ref tokenIndex);
                    operandStack.Push(Evaluate(subExpr));
                    continue;
                case ")":
                    throw new ArgumentException("Mis-matched parentheses in expression");
                default:
                    break;
            }

            //If this is an operator  
            int index = _operators2.IndexOf(token[0]);
            if (index >= 0)
            {
                int index2 = 0;
                while (operatorStack.Count > 0 && index < (index2 = _operators2.IndexOf(operatorStack.Peek())))
                {
                    char op = operatorStack.Pop();
                    float arg2 = operandStack.Pop();
                    float arg1 = operandStack.Pop();
                    operandStack.Push(_operations[index2](arg1, arg2));
                }
                operatorStack.Push(token[0]);
            }
            else
            {
                operandStack.Push(float.Parse(token));
            }
            tokenIndex += 1;
        }

        while (operatorStack.Count > 0)
        {
            char op = operatorStack.Pop();
            float arg2 = operandStack.Pop();
            float arg1 = operandStack.Pop();
            operandStack.Push(_operations[_operators2.IndexOf(op)](arg1, arg2));
        }
        return operandStack.Pop();
    }

    private static string getSubExpression(List<string> tokens, ref int index)
    {
        StringBuilder subExpr = new StringBuilder();
        int parenlevels = 1;
        index += 1;
        while (index < tokens.Count && parenlevels > 0)
        {
            string token = tokens[index];
            switch (token)
            {
                case "(":
                    parenlevels += 1;
                    break;
                case ")":
                    parenlevels -= 1;
                    break;
            }
            if (parenlevels > 0)
                subExpr.Append(token);
            index += 1;
        }
        if ((parenlevels > 0))
            throw new ArgumentException("Mis-matched parentheses in expression");
        return subExpr.ToString();
    }

    private static List<string> getTokens(string expression)
    {
        List<string> tokens = new List<string>();
        StringBuilder sb = new StringBuilder();
        string trimmedExpression = expression.Replace(" ", string.Empty);

        for (int i = 0, iMax = trimmedExpression.Length; i < iMax; i++)
        {
            char c = trimmedExpression[i];
            if (operators.IndexOf(c) >= 0)
            {
                if ((sb.Length > 0))
                {
                    tokens.Add(sb.ToString());
                    sb.Length = 0;
                }
                tokens.Add(c.ToString());
            }
            else
                sb.Append(c);
        }

        if ((sb.Length > 0))
            tokens.Add(sb.ToString());
        return tokens;
    }
}
#endregion StringToFormula