﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(AudioDefinitionResourceItem))]
public class AudioDefinitionResourceItemResourceInspector : ResourceItemInspector
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DrawBaseContent<AudioDefinitionResourceItem,AudioDefinition>(position, HelperParent.GetObject(property));
    }
}

public static class EditorCache<T,T2>
        where T : BaseResourceItem<T2>
        where T2 : Definition
{
    private static Dictionary<object,Editor> editorMap = new Dictionary<object,Editor>();
    public static HashSet<object> isOpen = new HashSet<object>();
    

    public static Editor getRefForObject(object obj)
    {
        Editor returnEditor = null;
        if(editorMap.TryGetValue(obj,out returnEditor))
           return returnEditor;
        return returnEditor;
    }

    public static void ClearEditor(object obj)
    {
        editorMap.Remove(obj);
    }

    public static void AddEditor(object obj, Editor editor)
    {
        editorMap.Add(obj,editor);
    }
}

public abstract class ResourceItemInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
		EditorGUI.TextField(position,label,"Not Implemented");
      //  DrawBaseContent<CursorRessourceItem,CursorDefinition>(position, HelperParent.GetObject(property));
    }
    public static UnityEngine.Object DrawBaseContent<T,T2>(Rect position, object target, UnityEngine.Object refObjectEditor = null, object refObject = null, GUIContent label = null)
        where T : BaseResourceItem<T2>
        where T2 : Definition
    {
        position.height -= 1;
        Rect indentposition = EditorGUI.IndentedRect(position);

        T referenceobject = (T)target;
        bool OldGUIEnabled = GUI.enabled;

        if(refObject != null)
        {  
            Rect realpos = indentposition;
            if (referenceobject == null)
                GUI.color = Color.grey;
            else
                GUI.color = referenceobject.isLoaded ? new Color(0.17f,0.75f,0.17f,1f) : (referenceobject._Object == null ? Color.red : Color.yellow);
            GUI.Box(indentposition, "");
            indentposition.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PrefixLabel(indentposition, label);
            GUI.color  = new Color(0.9f,0.9f,0.9f,1f);
            refObjectEditor = EditorGUI.ObjectField(indentposition, " ", refObjectEditor, typeof(T2),false);
            if (referenceobject == null)
                GUI.color = Color.grey;
            else
                GUI.color = referenceobject.isLoaded ? Color.green : (referenceobject._Object == null ? Color.red : Color.yellow);
            indentposition.y += EditorGUIUtility.singleLineHeight;
            GUI.enabled = false;
            EditorGUI.TextField(indentposition, new GUIContent(referenceobject?.DefinitionID.ToString()),referenceobject?.Path);  
            GUI.enabled = OldGUIEnabled;
            if(refObject != null)
            {
                if(GUI.Button(realpos,"","LargeLabel"))
                {
                    if(EditorCache<T,T2>.isOpen.Contains(refObject))
                        EditorCache<T,T2>.isOpen.Remove(refObject);
                    else
                        EditorCache<T,T2>.isOpen.Add(refObject);
                }
            }
        }
        else
        {
            Rect realpos = indentposition;
            GUI.color = referenceobject.isLoaded ? Color.green : (referenceobject._Object == null ? Color.red : Color.yellow);  
            GUI.Box(indentposition, "");
            indentposition.height = EditorGUIUtility.singleLineHeight;
            GUI.enabled = false;
            EditorGUI.ObjectField(indentposition, new GUIContent(referenceobject.InstanceName), referenceobject._Object, typeof(T2),false);
            indentposition.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.TextField(indentposition, new GUIContent(referenceobject.DefinitionID.ToString()),referenceobject.Path);
            GUI.enabled = OldGUIEnabled;
            if(GUI.Button(realpos,"","LargeLabel"))
            {
                if (referenceobject._Object == null)
                {
                    UnityEngine.Object obj = Resources.Load(referenceobject.Path);
                    EditorGUIUtility.PingObject(Resources.Load(referenceobject.Path));
                    if(!EditorApplication.isPlaying)
                        Resources.UnloadAsset(obj);
                }
                else
                    EditorGUIUtility.PingObject((UnityEngine.Object)referenceobject._Object);
            }
        }
        

        
        GUI.color = Color.white;
        // Draw foldout properties
        if (refObject != null && referenceobject != null && referenceobject.Object != null)
        {
            // Draw object properties
            EditorGUIUtility.hierarchyMode = true;
            Editor tempEditor = EditorCache<T,T2>.getRefForObject(referenceobject);
            if(!EditorGUI.Foldout(position, EditorCache<T,T2>.isOpen.Contains(refObject), GUIContent.none))
            {
                EditorCache<T,T2>.isOpen.Remove(refObject);
                EditorCache<T,T2>.ClearEditor(referenceobject);
            }
            else 
            {
                EditorCache<T,T2>.isOpen.Add(refObject);
                if(tempEditor == null)
                {
                    Editor.CreateCachedEditor(referenceobject.Object, null, ref tempEditor);
                    EditorCache<T,T2>.AddEditor(referenceobject,tempEditor);
                }
                else if(tempEditor.target != referenceobject.Object)
                {
                    EditorCache<T,T2>.ClearEditor(referenceobject);
                    Editor.CreateCachedEditor(referenceobject.Object, null, ref tempEditor);
                    EditorCache<T,T2>.AddEditor(referenceobject,tempEditor);
                }

                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(EditorGUI.indentLevel * 16);
                EditorGUI.indentLevel++;
                EditorGUILayout.BeginVertical("box");
                tempEditor.OnInspectorGUI();
                tempEditor.serializedObject.ApplyModifiedProperties();
                EditorGUILayout.EndVertical();
                EditorGUI.indentLevel--;
                EditorGUILayout.EndHorizontal();
                
            }
        }
        return refObjectEditor;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return (EditorGUIUtility.singleLineHeight+1) * 2;
    }
}