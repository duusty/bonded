﻿using UnityEngine;
using UnityEditor;
	
/// https://forum.unity.com/threads/better-scriptableobjects-inspector-editing-editor-tool.484392/
///see ScriptableObjectDrawer
[CanEditMultipleObjects]
[CustomEditor(typeof(ScriptableObject), true)]
public class ScriptableObjectEditor : Editor
{
}


[CanEditMultipleObjects]
[CustomEditor(typeof(MonoBehaviour), true)]
public class MonoBehaviourEditor : Editor
{
}