﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;

[CustomPropertyDrawer(typeof(AudioDefinitionResourceItemReference))]
public class AudioDefinitionResourceItemReferenceInspector : ResourceItemReferenceInspector
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DrawBaseContent<AudioDefinitionResourceItem, AudioDefinition>(position, property, label, HelperParent.GetObject(property));
    }
}

public class ResourceItemReferenceInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
		EditorGUI.TextField(position,label,"Not Implemented");
      //  DrawBaseContent<CursorRessourceItem, CursorDefinition>(position, property, label, HelperParent.GetObject(property));
    }

    public const float SearchWidth = 30;
    public static void DrawBaseContent<T,T2>(Rect position, SerializedProperty property, GUIContent label, object target)
        where T : BaseResourceItem<T2>
        where T2 : Definition
    {
        BaseResourceItemReference<T,T2> referenceobject = (BaseResourceItemReference<T,T2>)target;

        ResourceDatabase<T, T2> database = referenceobject.getDatabase();
        T[] objects = referenceobject.getDatabase().Objects;

        if (database.RealObjectCount <= 0)
        {
            bool oldGUIEnabled = GUI.enabled;
            GUI.enabled = false;
            EditorGUI.TextField(position,label,"DatabaseHasNoEntries");
            GUI.enabled = oldGUIEnabled;
            return;
        }

        SerializedProperty ItemID = property.FindPropertyRelative("_definitionID");
        position.height -= 1;
        
        T2 tempObject = null;
        T targetObject = null;
        if (ItemID.intValue >= 0)
        {
            targetObject = objects[ItemID.intValue];
            if (targetObject != null)
                tempObject = targetObject.Object;
        }
        
        T2 newTempObject = (T2)ResourceItemInspector.DrawBaseContent<T,T2>(position,targetObject,tempObject,referenceobject,label);
        if (tempObject != newTempObject)
        {
          //  Debug.LogFormat("Changed from {0} to {1} with id {2}", tempObject != null ? tempObject.DefinitionName : "null", newTempObject != null ? newTempObject.DefinitionName : "null", targetObject != null ? targetObject?.DefinitionID.ToString() : "null");
            if (newTempObject != null && objects[newTempObject.DefinitionID] != null)
                ItemID.intValue = newTempObject.DefinitionID;
            else
                ItemID.intValue = -1;
            property.serializedObject.ApplyModifiedProperties();
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty ItemID = property.FindPropertyRelative("_definitionID");
        if (ItemID.intValue == -1)
            return (EditorGUIUtility.singleLineHeight + 1);
        return (EditorGUIUtility.singleLineHeight+1) * 2;
    }
}