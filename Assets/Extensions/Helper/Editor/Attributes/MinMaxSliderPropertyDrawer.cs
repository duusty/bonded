﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
public class MinMaxSliderPropertyDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		MinMaxSliderAttribute minMaxSliderAttribute = attribute as MinMaxSliderAttribute;
        int previoudIndent = EditorGUI.indentLevel;
        
        if (property.propertyType == SerializedPropertyType.Vector2)
		{
            
            
			Rect controlRect = EditorGUILayout.GetControlRect();
			float labelWidth = EditorGUIUtility.labelWidth;
			float floatFieldWidth = EditorGUIUtility.fieldWidth;
			float sliderWidth = controlRect.width - labelWidth - 2f * floatFieldWidth;
			float sliderPadding = 5f;
           // Debug.Log(controlRect.width +" ____ " + labelWidth + " ___ " + floatFieldWidth);

            Rect sliderRect = new Rect(
				controlRect.x + labelWidth + floatFieldWidth + sliderPadding,
				controlRect.y,
				sliderWidth - 2f * sliderPadding,
				controlRect.height);

			Rect minFloatFieldRect = new Rect(
				controlRect.x + labelWidth,
				controlRect.y,
				floatFieldWidth,
				controlRect.height);


            Rect maxFloatFieldRect = new Rect(
				controlRect.x + labelWidth + floatFieldWidth + sliderWidth,
				controlRect.y,
				floatFieldWidth,
				controlRect.height);


            // Draw the slider
            EditorGUI.BeginChangeCheck();

			Vector2 sliderValue = property.vector2Value;
            EditorGUI.indentLevel = previoudIndent;
            EditorGUI.PrefixLabel(position,label);
            EditorGUI.indentLevel = 0;
            EditorGUI.MinMaxSlider(sliderRect, ref sliderValue.x, ref sliderValue.y, minMaxSliderAttribute.MinValue, minMaxSliderAttribute.MaxValue);

			sliderValue.x = EditorGUI.FloatField(minFloatFieldRect, sliderValue.x);
			sliderValue.x = Mathf.Clamp(sliderValue.x, minMaxSliderAttribute.MinValue, Mathf.Min(minMaxSliderAttribute.MaxValue, sliderValue.y));

			sliderValue.y = EditorGUI.FloatField(maxFloatFieldRect, sliderValue.y);
			sliderValue.y = Mathf.Clamp(sliderValue.y, Mathf.Max(minMaxSliderAttribute.MinValue, sliderValue.x), minMaxSliderAttribute.MaxValue);
            EditorGUI.indentLevel = previoudIndent;
            if (EditorGUI.EndChangeCheck())
				property.vector2Value = sliderValue;
		}
		else if (property.propertyType == SerializedPropertyType.Vector2Int)
		{
			Rect controlRect = EditorGUILayout.GetControlRect();
			float labelWidth = EditorGUIUtility.labelWidth;
			float floatFieldWidth = EditorGUIUtility.fieldWidth;
			float sliderWidth = controlRect.width - labelWidth - 2f * floatFieldWidth;
			float sliderPadding = 5f;
			
			Rect sliderRect = new Rect(
				controlRect.x + labelWidth + floatFieldWidth + sliderPadding,
				controlRect.y,
				sliderWidth - 2f * sliderPadding,
				controlRect.height);

			Rect minFloatFieldRect = new Rect(
				controlRect.x + labelWidth,
				controlRect.y,
				floatFieldWidth,
				controlRect.height);

			Rect maxFloatFieldRect = new Rect(
				controlRect.x + labelWidth + floatFieldWidth + sliderWidth,
				controlRect.y,
				floatFieldWidth,
				controlRect.height);

			// Draw the slider
			EditorGUI.BeginChangeCheck();

			Vector2 sliderValue = property.vector2IntValue;
            EditorGUI.indentLevel = previoudIndent;
            EditorGUI.PrefixLabel(position, label);
            EditorGUI.indentLevel = 0;
            EditorGUI.MinMaxSlider(sliderRect, ref sliderValue.x, ref sliderValue.y, minMaxSliderAttribute.MinValue, minMaxSliderAttribute.MaxValue);

			sliderValue.x = EditorGUI.IntField(minFloatFieldRect, (int)sliderValue.x);
			sliderValue.x = Mathf.Clamp(sliderValue.x, minMaxSliderAttribute.MinValue, Mathf.Min(minMaxSliderAttribute.MaxValue, sliderValue.y));

			sliderValue.y = EditorGUI.IntField(maxFloatFieldRect, (int)sliderValue.y);
			sliderValue.y = Mathf.Clamp(sliderValue.y, Mathf.Max(minMaxSliderAttribute.MinValue, sliderValue.x), minMaxSliderAttribute.MaxValue);
            EditorGUI.indentLevel = previoudIndent;
            if (EditorGUI.EndChangeCheck())
				property.vector2IntValue = new Vector2Int((int)sliderValue.x,(int)sliderValue.y);
		}
		else
		{
            string warning = minMaxSliderAttribute.GetType().Name + " can be used only on Vector2 and Vector2Int fields";
			EditorGUILayout.HelpBox(warning, MessageType.Warning);
			EditorGUI.LabelField(position, warning);
		}

    }

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return 0;
	}
}