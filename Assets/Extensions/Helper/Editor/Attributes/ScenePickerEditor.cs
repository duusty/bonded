﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.SceneManagement;

[CustomPropertyDrawer(typeof(ScenePicker), true)]
public class ScenePickerPropertyDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		SerializedProperty scenePath = property.FindPropertyRelative("scenePath");
		SerializedProperty sceneName = property.FindPropertyRelative("sceneName");
		SerializedProperty sceneBuildID = property.FindPropertyRelative("sceneBuildID");

		SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath.stringValue);
		EditorGUI.BeginChangeCheck();
        SceneAsset newScene = EditorGUILayout.ObjectField("Scene", oldScene, typeof(SceneAsset), false) as SceneAsset;

        if (EditorGUI.EndChangeCheck())
        {
            string newPath = AssetDatabase.GetAssetPath(newScene);
            sceneName.stringValue = System.IO.Path.GetFileNameWithoutExtension(newPath);
            sceneBuildID.intValue = SceneUtility.GetBuildIndexByScenePath(newPath);
            scenePath.stringValue = newPath;
        }
       // property.ApplyModifiedProperties();
	}

}