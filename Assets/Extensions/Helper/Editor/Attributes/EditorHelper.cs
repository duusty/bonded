﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Security.Cryptography;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

#if UNITY_EDITOR
public class EditorHelper
{
    public const string AssetCreatorPathStringPrefix = "Assets/IamBob/";


    private static bool bIntialized = false;

    private static void Initilize()
    {
        GUISkin GUISKinPro = EditorGUIUtility.GetBuiltinSkin(EditorGUIUtility.isProSkin ? EditorSkin.Scene : EditorSkin.Inspector);
        GUISkin GUISkin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
        _FlowOverflowLeft = new GUIStyle(GUISKinPro.FindStyle("flow overlay header upper left"));
        _FlowOverflowRight = new GUIStyle(GUISKinPro.FindStyle("flow overlay header upper right"));

        _FlowOverflowLeft.active.background = GUISkin.FindStyle("flow overlay header upper left").normal.background;
        _FlowOverflowLeft.active.textColor = EditorGUIUtility.isProSkin ? Color.black : Color.gray;
        _FlowOverflowRight.active.background = GUISkin.FindStyle("flow overlay header upper right").normal.background;
        _FlowOverflowRight.active.textColor = EditorGUIUtility.isProSkin ? Color.black : Color.gray;

        _LockButtonIcon = GUISkin.FindStyle("IN LockButton").normal.background;

        _ConfigIcon = EditorGUIUtility.FindTexture("d__Popup");
    }

    private static GUIStyle _FlowOverflowLeft;
    public static GUIStyle FlowOverflowLeft
    {
        get
        {
            if (!bIntialized) Initilize();
            return _FlowOverflowLeft;
        }
    }

    private static Texture2D _ConfigIcon;
    public static Texture2D ConfigIcon
    {
        get
        {
            if (!bIntialized) Initilize();
            return _ConfigIcon;
        }
    }


    private static Texture2D _LockButtonIcon;
    public static Texture2D LockButtonIcon
    {
        get
        {
            if (!bIntialized) Initilize();
            return _LockButtonIcon;
        }
    }

    private static GUIStyle _FlowOverflowRight;
    public static GUIStyle FlowOverflowRight
    {
        get
        {
            if (!bIntialized) Initilize();
            return _FlowOverflowRight;
        }
    }

    private static Dictionary<Type, Texture2D> IconDictionary = new Dictionary<Type, Texture2D>();

    public static Texture2D GetIconForType<V>()
    {
        Type tempType = typeof(V);
        if (IconDictionary.ContainsKey(tempType))
            return IconDictionary[tempType];
        Texture2D tempTexture = AssetPreview.GetMiniTypeThumbnail(tempType);

        tempTexture = AssetPreview.GetMiniTypeThumbnail(tempType);
        if (!tempTexture)
        {
            if (tempType.IsEnum) tempTexture = EditorGUIUtility.FindTexture("d_UnityEditor.HierarchyWindow");
            if (tempType == typeof(string)) tempTexture = EditorGUIUtility.FindTexture("d_UnityEditor.ConsoleWindow");
            if (tempType == typeof(Color)) tempTexture = EditorGUIUtility.FindTexture("ColorField");
        }
        IconDictionary.Add(tempType, tempTexture);
        return tempTexture;
    }

    public static void MinMaxSliderFields(GUIContent content, Rect position, ref float minValue, ref float maxValue, float minLimit, float maxLimit, float fieldWidth = 25)
    {
        EditorGUI.MinMaxSlider(content, new Rect(position.x, position.y, position.width - (fieldWidth * 2 + 5), position.height), ref minValue, ref maxValue, minLimit, maxLimit);
        minValue = EditorGUI.FloatField(new Rect(position.x + position.width - (fieldWidth * 2), position.y, fieldWidth, position.height), (float)Mathf.Round(minValue));
        maxValue = EditorGUI.FloatField(new Rect(position.x + position.width - fieldWidth, position.y, fieldWidth, position.height), (float)Mathf.Round(maxValue));
    }

    public static GameObject PrefabObject<T>(Rect position, GameObject Object, string Name, bool AllowSceneObjects = false) where T : UnityEngine.Component
    {
        Object = EditorGUI.ObjectField(position, new GUIContent(Name), Object, typeof(GameObject), AllowSceneObjects) as GameObject;
        if (Object != null && Object.GetComponentsInChildren<T>(true).Length <= 0) Object = null;
        return Object;
    }

    public static void DragAblePrefabFieldList<T>(Rect position, ref List<GameObject> List, string ObjectName = "Objects", int MaxCount = 6, bool LerpColor = false, bool AllowSceneObjects = false) where T : UnityEngine.Component
    {
        Event evt = Event.current;
        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!position.Contains(evt.mousePosition)) return;
                if (List.Count >= MaxCount) return;
                if (!AllowSceneObjects && !AssetDatabase.Contains(DragAndDrop.objectReferences[0])) return;
                if ((DragAndDrop.objectReferences[0] as GameObject) == null) return;
                if ((DragAndDrop.objectReferences[0] as GameObject).GetComponentsInChildren<T>(true).Length <= 0) return;
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
                    {
                        if ((dragged_object as GameObject) != null && (DragAndDrop.objectReferences[0] as GameObject).GetComponentsInChildren<T>(true).Length > 0)
                            List.Add(dragged_object as GameObject);
                    }
                }
                break;
        }

        if (LerpColor) GUI.color = Color.Lerp(Color.green, Color.red, (float)List.Count / MaxCount);
        if (List.Count >= MaxCount)
            GUI.Box(position, "", "dockarea");
        else
        {
            GUI.Box(position, "", "dockarea");
            if (List.Count == 0)
                GUI.Label(new Rect(position.width - 100, position.y, 115, position.height), "Drag in " + ObjectName);
            else
                GUI.Label(new Rect(position.width - 100, position.y, 115, position.height), (MaxCount - List.Count) + " more " + ObjectName);
        }
        GUI.color = Color.white;
        for (int i = 0; i < List.Count; i++)
        {
            List[i] = EditorGUI.ObjectField(new Rect(position.x + (position.width / MaxCount) * i, position.y - 1, position.width / MaxCount, position.height - 1), List[i], typeof(GameObject), AllowSceneObjects) as GameObject;
            if ((i + 1) > MaxCount || List[i] == null) List.RemoveAt(i);
        }
    }

    public static void DragAbleFieldList<T>(Rect position, ref List<T> List, string ObjectName = "Objects", int MaxCount = 6, bool LerpColor = false, bool AllowSceneObjects = false) where T : UnityEngine.Object
    {
        Event evt = Event.current;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!position.Contains(evt.mousePosition)) return;
                if (List.Count >= MaxCount) return;
                if (!AllowSceneObjects && !AssetDatabase.Contains(DragAndDrop.objectReferences[0])) return;
                if ((DragAndDrop.objectReferences[0] as T) == null) return;
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
                    {
                        if ((dragged_object as T) != null)
                            List.Add(dragged_object as T);
                    }
                }
                break;
        }

        if (LerpColor) GUI.color = Color.Lerp(Color.green, Color.red, (float)List.Count / MaxCount);
        if (List.Count >= MaxCount)
            GUI.Box(position, "", "dockarea");
        else
        {
            GUI.Box(position, "", "dockarea");
            if (List.Count == 0)
                GUI.Label(new Rect(position.width - 100, position.y, 115, position.height), "Drag in " + ObjectName);
            else
                GUI.Label(new Rect(position.width - 100, position.y, 115, position.height), (MaxCount - List.Count) + " more " + ObjectName);
        }
        GUI.color = Color.white;
        for (int i = 0; i < List.Count; i++)
        {
            List[i] = EditorGUI.ObjectField(new Rect(position.x + (position.width / MaxCount) * i, position.y - 1, position.width / MaxCount, position.height - 1), List[i], typeof(T), AllowSceneObjects) as T;
            if ((i + 1) > MaxCount || List[i] == null) List.RemoveAt(i);
        }
    }

    public static long DrawBitMaskField(Rect aPosition, long aMask, System.Type aType, GUIContent aLabel)
    {
        var itemNames = System.Enum.GetNames(aType);
        var itemValues = System.Enum.GetValues(aType) as long[];

        long val = aMask;
        int maskVal = 0;
        for (int i = 0; i < itemValues.Length; i++)
        {
            if (itemValues[i] != 0)
            {
                if ((val & itemValues[i]) == itemValues[i])
                    maskVal |= 1 << i;
            }
            else if (val == 0)
                maskVal |= 1 << i;
        }
 

        int newMaskVal = EditorGUI.MaskField(aPosition, aLabel, maskVal, itemNames);
        int changes = maskVal ^ newMaskVal;

        for (int i = 0; i < itemValues.Length; i++)
        {
            if ((changes & (1 << i)) != 0)            // has this list item changed?
            {
                if ((newMaskVal & (1 << i)) != 0)     // has it been set?
                {
                    if (itemValues[i] == 0)           // special case: if "0" is set, just set the val to 0
                    {
                        val = 0;
                        break;
                    }
                    else
                        val |= itemValues[i];
                }
                else                                  // it has been reset
                {
                    val &= ~itemValues[i];
                }
            }
        }
        return val;
    }
    


    static public bool DrawHeader(string text, string key)
    {
        bool state = EditorPrefs.GetBool(key, false);

        GUILayout.Space(3f);
        if (!state) GUI.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        GUI.changed = false;
#if UNITY_3_5
		if (state) text = "\u25B2 " + text;
		else text = "\u25BC " + text;
		if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f))) state = !state;
#else
        text = "<b><size=11>" + text + "</size></b>";
        if (state) text = "\u25B2 " + text;
        else text = "\u25BC " + text;
        if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f))) state = !state;
#endif
        if (GUI.changed) EditorPrefs.SetBool(key, state);

        GUILayout.Space(2f);
        GUILayout.EndHorizontal();
        GUI.backgroundColor = Color.white;
        if (!state) GUILayout.Space(3f);
        return state;
    }

    static public void BeginContents()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(4f);
        EditorGUILayout.BeginHorizontal("AS TextArea", GUILayout.MinHeight(10f));
        GUILayout.BeginVertical();
        GUILayout.Space(2f);
    }

    static public void EndContents()
    {
        GUILayout.Space(3f);
        GUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(3f);
        GUILayout.EndHorizontal();
        GUILayout.Space(3f);
    }

    static public string[] GetSortingLayerNames()
    {
        System.Type internalEditorUtilityType = typeof(UnityEditorInternal.InternalEditorUtility);
        System.Reflection.PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
        return (string[])sortingLayersProperty.GetValue(null, new object[0]);
    }

    static public int[] GetSortingLayerUniqueIDs()
    {
        System.Type internalEditorUtilityType = typeof(UnityEditorInternal.InternalEditorUtility);
        System.Reflection.PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
        return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
    }
}
#endif