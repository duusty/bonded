﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(UnityDateTime))]
public class UnityDateTimeInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive, position), label);
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        UnityDateTime Date = (HelperParent.GetObject(property) as UnityDateTime);
        if (Date != null)
        {
            GUI.enabled = false;
            EditorGUI.TextField(new Rect(position.x, position.y, position.width-65, 16), Date.Date.ToString());
            GUI.enabled = true;
            
            System.TimeSpan LastUpdateDifference = System.DateTime.Now - Date.Date;

            if (LastUpdateDifference.TotalHours > 1)
            {
                GUI.color = Color.Lerp(Color.green, Color.red, (float)LastUpdateDifference.TotalHours / 168f);
            }
            if (GUI.Button(new Rect(position.x+position.width - 60, position.y, 60, 16), "Update"))
            {
                Date.Date = System.DateTime.Now;
            }
            GUI.color = Color.white;
            if (LastUpdateDifference.TotalHours > 1)
            {
                if(LastUpdateDifference.TotalDays >= 1)
                    EditorGUI.HelpBox(new Rect(position.x, position.y + 16, position.width, position.height - 16), "Last Update was, " + (int)LastUpdateDifference.TotalDays + " Days Ago!", MessageType.Warning);
                else
                    EditorGUI.HelpBox(new Rect(position.x, position.y + 16, position.width, position.height - 16), "Last Update was, " + (int)LastUpdateDifference.TotalHours + " Hours Ago!", MessageType.Info);
            }

        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        UnityDateTime Date = (HelperParent.GetObject(property) as UnityDateTime);
        if (Date != null && (System.DateTime.Now - Date.Date).TotalHours > 1)
        {
            return base.GetPropertyHeight(property, label) * 3;
        }
        return base.GetPropertyHeight(property, label);
    }
}
