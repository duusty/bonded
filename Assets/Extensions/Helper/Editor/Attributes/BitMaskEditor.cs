﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Runtime.InteropServices;

[CustomPropertyDrawer(typeof(BitMaskAttribute))]
public class EnumBitMaskPropertyDrawer : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label)
	{
		var typeAttr = attribute as BitMaskAttribute;



        if (typeAttr.isLongType)
        {
            prop.longValue = Convert.ToInt64(EditorGUI.EnumFlagsField(position, label, (Enum)Enum.ToObject(typeAttr.propType, prop.longValue), false));
        }
        else
        {
            prop.intValue = Convert.ToInt32(EditorGUI.EnumFlagsField(position, label, (Enum)Enum.ToObject(typeAttr.propType, prop.intValue), false));
        }
	}
}