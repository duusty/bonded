﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorCacheEntry
{
    public Editor editor;
    public bool isOpen;
}

public static class EditorCache
{
    private static Dictionary<object, EditorCacheEntry> editorMap = new Dictionary<object, EditorCacheEntry>();

    public static EditorCacheEntry getRefForObject(object obj)
    {
        if (editorMap.TryGetValue(obj, out EditorCacheEntry returnEditor))
            return returnEditor;
        return new EditorCacheEntry();
    }

    public static void ClearEditor(object obj)
    {
        editorMap.Remove(obj);
    }

    public static void AddEditor(object obj, EditorCacheEntry editor)
    {
        editorMap.Add(obj, editor);
    }
}


[CustomPropertyDrawer(typeof(ExposableAttribute))]
public class ExposedAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        if (prop.isArray)
        {
            for (int i = 0, iMax = prop.arraySize; i < iMax; i++)
            {
                SerializedProperty indexArray = prop.GetArrayElementAtIndex(i);
                DrawEntry(position, prop);
            }
        }
        else
            DrawEntry(position, prop);
    }

    public static void DrawEntry(Rect position, SerializedProperty prop, GUIContent label = null)
    {
        if(label!= null)
            EditorGUI.PropertyField(position, prop, label);
        else
            EditorGUI.PropertyField(position, prop);
        // Draw object properties
        EditorGUIUtility.hierarchyMode = true;
        if (prop.objectReferenceValue == null)
            return;

        EditorCacheEntry editorCacheEntry = EditorCache.getRefForObject(prop.objectReferenceValue);
        if (!EditorGUI.Foldout(position, editorCacheEntry.isOpen, GUIContent.none))
        {
            EditorCache.ClearEditor(prop.objectReferenceValue);
        }
        else
        {
            editorCacheEntry.isOpen = true;
            if (editorCacheEntry.editor == null)
            {
                Editor.CreateCachedEditor(prop.objectReferenceValue, null, ref editorCacheEntry.editor);
                EditorCache.AddEditor(prop.objectReferenceValue, editorCacheEntry);
            }
            else if (editorCacheEntry.editor.target != prop.objectReferenceValue)
            {
                EditorCache.ClearEditor(editorCacheEntry.editor.target);
                Editor.CreateCachedEditor(prop.objectReferenceValue, null, ref editorCacheEntry.editor);
                EditorCache.AddEditor(prop.objectReferenceValue, editorCacheEntry);
            }
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(16);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginVertical("box");
            editorCacheEntry.editor.OnInspectorGUI();
            editorCacheEntry.editor.serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel--;
            EditorGUILayout.EndHorizontal();
        }
    }

    public static void DrawObjectEntry(Rect position, UnityEngine.Object prop, GUIContent label = null)
    {
        if (label != null)
            EditorGUI.ObjectField(position, label, prop, prop.GetType(), true);
        else
            EditorGUI.ObjectField(position, prop, prop.GetType(), true);
        // Draw object properties
        EditorGUIUtility.hierarchyMode = true;
        if (prop == null)
            return;

        EditorCacheEntry editorCacheEntry = EditorCache.getRefForObject(prop);
        if (!EditorGUI.Foldout(position, editorCacheEntry.isOpen, GUIContent.none))
        {
            EditorCache.ClearEditor(prop);
        }
        else
        {
            editorCacheEntry.isOpen = true;
            if (editorCacheEntry.editor == null)
            {
                Editor.CreateCachedEditor(prop, null, ref editorCacheEntry.editor);
                EditorCache.AddEditor(prop, editorCacheEntry);
            }
            else if (editorCacheEntry.editor.target != prop)
            {
                EditorCache.ClearEditor(editorCacheEntry.editor.target);
                Editor.CreateCachedEditor(prop, null, ref editorCacheEntry.editor);
                EditorCache.AddEditor(prop, editorCacheEntry);
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(16);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginVertical("box");
            editorCacheEntry.editor.OnInspectorGUI();
            editorCacheEntry.editor.serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel--;
            EditorGUILayout.EndHorizontal();
        }
    }
}
