﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(UpdateUnityGetVersionString))]
public class UpdateUnityGetVersionStringInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive, position), label);
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        UpdateUnityGetVersionString Date = (HelperParent.GetObject(property) as UpdateUnityGetVersionString);
        if (Date != null)
        {
            GUI.enabled = true;
            EditorGUI.SelectableLabel(new Rect(position.x, position.y, position.width - 65, 16), ReleaseInfo.GetVersion);
            GUI.enabled = true;
            
            if (GUI.Button(new Rect(position.x+position.width - 60, position.y, 60, 16), "Update"))
            {
                ReleaseInfo.UpdateVersion();
            }

        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}
