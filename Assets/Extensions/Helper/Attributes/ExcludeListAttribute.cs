﻿using UnityEngine;

public class ExcludeListAttribute : PropertyAttribute
{
	public System.Type propType;
	
	public ExcludeListAttribute(System.Type aType)
	{
		propType = aType;
	}
}
