﻿using UnityEngine;

public class BitMaskAttribute : PropertyAttribute
{
	public System.Type propType;
    public bool isLongType;
	
	public BitMaskAttribute(System.Type aType, bool isLong = false)
	{
        isLongType = isLong;
        propType = aType;
	}
}
