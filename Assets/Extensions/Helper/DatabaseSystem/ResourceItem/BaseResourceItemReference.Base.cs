﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;

[System.Serializable]
public class BaseResourceItemReference<T,T2>
    where T : BaseResourceItem<T2>
    where T2 : Definition
{
    public BaseResourceItemReference() {}

    public BaseResourceItemReference(T2 ObjectDef)
    {
        _definitionID = ObjectDef.DefinitionID;
    }
    public BaseResourceItemReference(T ObjectDef)
    {
        _definitionID = ObjectDef.DefinitionID;
    }

    public bool isLoaded { get { return _RuntimeObject != null; } }
    public virtual ResourceDatabase<T,T2> getDatabase() {return null;}
    [SerializeField()]
    protected int _definitionID;

    //for faster comparison if reference has the same id
    [MethodImpl(MethodImplOptions.AggressiveInlining)] [System.Runtime.TargetedPatchingOptOut("")]
    public int GetDefinitionID() { return _definitionID; }

    public void SetDefinitionID(int id)
    {
        _definitionID = id;
        _RuntimeObject = null;
    }

    [System.NonSerialized()]
    protected T _RuntimeObject;

    public T Object
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get
        {
            if (_RuntimeObject == null)
                _RuntimeObject = getDatabase().Objects[_definitionID];
            return _RuntimeObject;
        }
    }
}