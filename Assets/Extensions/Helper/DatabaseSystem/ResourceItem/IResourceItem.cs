﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourceItem
{
    bool isLoaded { get; }
    void UnloadObject();
    string ToString();
    int GetHashCode();
    int ID { get; }
}
