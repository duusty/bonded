﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class BaseResourceItem<T> : IResourceItem where T : Definition
{
    protected const string ResourcesFolderPrefix = "Resources/";

#if UNITY_EDITOR
    public BaseResourceItem(T ObjectToReference)
    {
        string AssetPath = AssetDatabase.GetAssetPath((UnityEngine.Object)ObjectToReference);
        if (AssetPath.Contains(ResourcesFolderPrefix))
        {
            int Index = AssetPath.LastIndexOf(ResourcesFolderPrefix) + ResourcesFolderPrefix.Length;
            string extension = System.IO.Path.GetExtension(AssetPath);
            Path = AssetPath.Substring(Index, AssetPath.Length - extension.Length - Index);
        }
        else
            Object = ObjectToReference;

        DefinitionID = ObjectToReference.DefinitionID;
        InstanceName = ObjectToReference.name;
    }
    [SerializeField()]
    public T _Object;
#else
    [SerializeField()]
    protected T _Object;
#endif
    protected BaseResourceItem() { DefinitionID = -1; }

    public bool isLoaded
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get { return _RuntimeObject != null; }
    }

    public virtual int ID
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get { return DefinitionID; }
    }

    public int DefinitionID = -1;
    public string InstanceName;

    [System.NonSerialized()]
    protected T _RuntimeObject;

    public string Path;

    public T Object
    {
#if UNITY_EDITOR
        set
        {
            _Object = value;
        }
#endif
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get
        {
            if (_RuntimeObject == null)
            {
                if (_Object != null) _RuntimeObject = _Object;
                else if (string.IsNullOrEmpty(Path)) return null;
                else _RuntimeObject = Resources.Load<T>(Path);
            }
            return _RuntimeObject;
        }
    }

    //Possible Load Callback for drawable worldtiles
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public virtual void UnloadObject()
    {
        if (_RuntimeObject != null)
        {
            // _Object = null;

          //  if (typeof(T) is Component || typeof(T) is GameObject)
          //      return;
          //  Resources.UnloadAsset(_RuntimeObject);
           // _RuntimeObject = null;
        }
    }

    public override string ToString()
    {
        if (_RuntimeObject != null)
            return _RuntimeObject.ToString();
        return InstanceName;
    }

    public override int GetHashCode()
    {
        return DefinitionID;
    }
}
