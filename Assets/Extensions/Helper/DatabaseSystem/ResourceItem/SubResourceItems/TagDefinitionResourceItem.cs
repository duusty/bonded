﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class TagDefinitionResourceItemReference : BaseResourceItemReference<TagDefinitionResourceItem, TagDefinition>
{
    public override ResourceDatabase<TagDefinitionResourceItem, TagDefinition> getDatabase() {return TagDefinitionDatabase.Instance; }
}

[System.Serializable()]
public class TagDefinitionResourceItem : BaseResourceItem<TagDefinition>
{
#if UNITY_EDITOR
    public TagDefinitionResourceItem(TagDefinition ObjectToReference)
        : base(ObjectToReference)
    {
    }
#endif
}
