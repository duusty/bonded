﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class AudioDefinitionResourceItemReference : BaseResourceItemReference<AudioDefinitionResourceItem,AudioDefinition>
{
    public override ResourceDatabase<AudioDefinitionResourceItem,AudioDefinition> getDatabase() {return AudioDefinitionDatabase.Instance; }
}

[System.Serializable()]
public class AudioDefinitionResourceItem : BaseResourceItem<AudioDefinition>
{
#if UNITY_EDITOR
    public AudioDefinitionResourceItem(AudioDefinition ObjectToReference)
        : base(ObjectToReference)
    {
    }
#endif
}
