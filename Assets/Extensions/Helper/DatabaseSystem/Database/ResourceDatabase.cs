﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
public abstract class ResourceDatabase<ResourceItem,T> :
    IDatabase where ResourceItem : BaseResourceItem<T>
    where T : Definition
{
    [HideInInspector] [SerializeField()] public int MaxReservedID = 0;
    [HideInInspector] [SerializeField()] public int RealObjectCount = 0;
    [HideInInspector] [SerializeField()] public ResourceItem[] Objects;

    public abstract void Initialize();
#if UNITY_EDITOR
[MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public virtual ResourceItem CreateObject(T sourceItem)
    {
        Debug.LogError("Not Implemented");
        return null;
    }

    public virtual void FindItems(float CurrentProgress, float ProgressModifier)
    {
        string DatabaseName = this.GetType().ToString() + " Database";
        EditorUtility.DisplayProgressBar(DatabaseName, "Importing", CurrentProgress);
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
        RealObjectCount = guids.Length;
        if (RealObjectCount > MaxReservedID)
            MaxReservedID = RealObjectCount;
        Objects = new ResourceItem[MaxReservedID];
        HashSet<int> usedIDs = new HashSet<int>();
        T[] tempObjects = new T[guids.Length];
        int HighestUsedID = -1;
        for (int i = 0; i < guids.Length; i++)
        {
            EditorUtility.DisplayProgressBar(DatabaseName, "Importing Items", CurrentProgress + (((float)i / (float)guids.Length) * ProgressModifier));
            T definitionObject = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[i]));
            if (definitionObject.DefinitionID > -1)
            {
                if (definitionObject.DefinitionID > MaxReservedID || !usedIDs.Add(definitionObject.DefinitionID))
                    definitionObject.DefinitionID = -1;
            }
            tempObjects[i] = definitionObject;
        }
        int checkedIds = 0;
        for (int i = 0; i < guids.Length; i++)
        {
            if(tempObjects[i].DefinitionID == -1)
            {
                while(usedIDs.Contains(checkedIds))
                    checkedIds++;
                tempObjects[i].DefinitionID = checkedIds;
                checkedIds++;
            }
            Objects[tempObjects[i].DefinitionID] = CreateObject(tempObjects[i]);
            if (tempObjects[i].DefinitionID > HighestUsedID)
                HighestUsedID = tempObjects[i].DefinitionID;
        }
        Array.Resize(ref Objects, HighestUsedID+1);
        MaxReservedID = HighestUsedID + 1;
        EditorUtility.ClearProgressBar();
        AssetDatabase.SaveAssets();
    }
#endif

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public virtual void Sort()
    {
        ByInstanceName.Clear();
        foreach (ResourceItem item in Objects)
        {
            if (!ByInstanceName.ContainsKey(item.InstanceName)) ByInstanceName.Add(item.InstanceName, item);
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public virtual void Unload()
    {
        for (int i = 0, iMax = Objects.Length; i < iMax; i++)
            Objects[i].UnloadObject();
    }

    public int GetLoadedObjects
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get
        {
            int returnValue = 0;
            for (int i = 0, iMax = Objects.Length; i < iMax; i++)
                returnValue += Objects[i].isLoaded ? 1 : 0;
            return returnValue;
        }
    }
    public virtual ResourceItem getFirst
    { 
        [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
        get { return Objects[0];  }
    }
    public Dictionary<string, ResourceItem> ByInstanceName = new Dictionary<string, ResourceItem>(StringComparer.OrdinalIgnoreCase);
}
