﻿using UnityEngine;
using System.Collections;

public interface IDatabase
{
#if UNITY_EDITOR
    void FindItems(float CurrentProgress, float ProgressModifier);
#endif

    //SerializedType getDatabaseType();
    void Initialize();
    void Sort();
    void Unload();

    int GetLoadedObjects { get; }
}
