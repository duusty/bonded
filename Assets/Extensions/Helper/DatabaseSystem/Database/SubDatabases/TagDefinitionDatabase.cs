﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable()]
public class TagDefinitionDatabase : ResourceDatabase<TagDefinitionResourceItem, TagDefinition>
{
#if UNITY_EDITOR
    public override TagDefinitionResourceItem CreateObject(TagDefinition sourceItem)
    {
        return new TagDefinitionResourceItem(sourceItem);
    }
#endif
    public static TagDefinitionDatabase Instance;

    public override void Initialize()
    {
        Instance = this;
    }
}
