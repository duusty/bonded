﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable()]
public class AudioDefinitionDatabase : ResourceDatabase<AudioDefinitionResourceItem, AudioDefinition>
{
#if UNITY_EDITOR
    public override AudioDefinitionResourceItem CreateObject(AudioDefinition sourceItem)
    {
        return new AudioDefinitionResourceItem(sourceItem);
    }
#endif
    public static AudioDefinitionDatabase Instance;

    public override void Initialize()
    {
        Instance = this;
    }
}
