﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Runtime.CompilerServices;

//https://github.com/oluwaseyeayinla/Papae.AudioManager/blob/master/Assets/Scripts/AudioManager.cs
//TODO setup a similiar system with particle effect variants so we can alternate effects for muzzle flashes for example

//dictionary with metainfo for each object, last played time, last played instance, betterlist with all audio objects playing it currently and so we can limit also the max count
//or stop playing all off this type at once
//also for crossfading into another object
public class AudioObjectMeta
{
    public float LastPlayedTime = 0;
    public int LastPlayedSubItemIndex = 0;
    //check that we are not over the instance limit
    public BetterList<AudioObject> CurrentPlayingInstances = new BetterList<AudioObject>();
}

//could maybe have a custom preview to play the sound
[CreateAssetMenu(menuName = HelperParent.AssetMenuPathStringPrefix + "AudioDefinition")]
public class AudioDefinition : Definition {

    [System.NonSerialized]
    public AudioObjectMeta metaInfo = new AudioObjectMeta();

    public enum AudioPlayMode
    {
        Random,
        SequenceWithRandomStart,
        //AllSimultaneously,
        Sequence //Seqence could be interesting for music?
        //unqiueRandom
    }
    public AudioPlayMode playMode;
    public AudioMixerGroup mixerGroup;

    public uint MaxPlayableInstances = 3;
    public bool ByPassEffects = false;

//maybe we can set a priority to check if a subitem has a higher prio? //really necessary? though?
    [MinMaxSlider(0.5f,1.5f)]
    public Vector2 MinMaxPitch = Vector2.one;
    [MinMaxSlider(0.5f,1.5f)]
    public Vector2 MinMaxVolume = Vector2.one;
    [MinMaxSlider(1f,500f)]
    public Vector2 MinMaxDistance = new Vector2(20,200);
    [MinMaxSlider(0f,5f)]
    public Vector2 MinMaxDelay = Vector2.zero;

    public float MinTimeBetweenPlayCalls = 0.1f; //we shouldnt play this sound to often

    public uint LoopCount = 1; //  0 means infinite loops

    //inbetween for loops or if the next subitem should be player and also in the beginning of the first play

    //public float loopSequenceRandomDelay = 0; if looping we will change the pitch ever x seconds
    //TODO: crossfade will be done like that, we start a next audio item playcall withanother pooled object before the first one stops

    [ExposableAttribute()]
    public AudioClip audioClip;

    [ExposableAttribute()]
    public AudioDefinition[] subClips; //wait time in between, crossfade options? // will be usefull for footsteps :) though we can also just use custom pitches

    public float FadeInOutTime = 0.5f; //for crossfade randomized subsounds or music interpolation so overlaps as delay will hinder overlaps just in case

    
    public float spatialBlend = 1; //spatialBlend fully 2d or 3d ?

    //maybe we can also evaluate the bmp to blend them in?
    //spatialBlend fully 2d or 3d ?
    //public float Max/Min Attunation Distance? / falloff

    //we could also make custom prefab pools for each sound object wich are pre configured... this would be probably be more performant
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public AudioDefinition getPossibleSubItemAudioDefinition()
    {
        //TODO optimize casting from float repeat to int
        if(subClips.Length <= 0)
            return this;
        switch (playMode)
        {
            case AudioPlayMode.Random:
                return subClips[Random.Range(0,subClips.Length)].getPossibleSubItemAudioDefinition();
            case AudioPlayMode.SequenceWithRandomStart:
                if(metaInfo.LastPlayedSubItemIndex == 0)
                    metaInfo.LastPlayedSubItemIndex = Random.Range(0,subClips.Length);
                return subClips[(int)Mathf.Repeat(metaInfo.LastPlayedSubItemIndex++,subClips.Length)].getPossibleSubItemAudioDefinition();
            case AudioPlayMode.Sequence:
                return subClips[(int)Mathf.Repeat(metaInfo.LastPlayedSubItemIndex++,subClips.Length)].getPossibleSubItemAudioDefinition();
            default:
                return this;
        }

        //we should in some way make sure to not get into an endless loop if our sublclip consists of only one audioDefinition and its the same as we are
        //this way we will traverse infinitly through our random sound pool (for example different footsteps)
    }
}
