﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[CreateAssetMenu(menuName = HelperParent.AssetMenuPathStringPrefix + "TagDefinition")]
public class TagDefinition : Definition
{
    public TagDefinition ParentTag;
    public TagDefinition[] ChildTags;
}