﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.CompilerServices;

public class Definition : ScriptableObject, IEquatable<Definition>
{
    [ReadOnly] [SerializeField()] public int DefinitionID = -1;

    protected virtual void OnEnable () {
#if UNITY_EDITOR
       // if (EditorApplication.isPlayingOrWillChangePlaymode) {
        //    if (string.IsNullOrEmpty (name))
        //        Debug.Log (this.GetType ().ToString () + "(Clone)");
        //    else
        //        Debug.Log (name);
      //  }
#endif
        name = name.Replace ("(Clone)", "");
    }

#if UNITY_EDITOR
    [ContextMenu ("Show|Copy DefinitionID")]
    protected void ShowID () {
        Debug.LogFormat ("DefinitionID: {0}, InstanceID: {1}",DefinitionID,GetInstanceID());
        EditorGUIUtility.systemCopyBuffer = string.Format ("[{0}]", DefinitionID);
    }

    [ContextMenu ("To|Json")]
    public virtual void ToJson () {
        Debug.Log (JsonUtility.ToJson (this, true));
    }
#endif

    public virtual string DefinitionName { get { return name; } }

    public bool Equals(Definition other)
    {
        return DefinitionID == other.DefinitionID;
    }

    public override int GetHashCode()
    {
        return DefinitionID;
    }

    public static implicit operator int (Definition i) {
        return i.DefinitionID;
    }
}

public static class RuntimeDefinitionHelper {
    public static RuntimeDefType GetCopy<RuntimeDefType, DefinitionType> (this RuntimeDefType @object)
        where RuntimeDefType : RuntimeDefinition<DefinitionType>
        where DefinitionType : Definition
    {
        return null;
//        return BobSerializer.getSerializedCopy (@object);
    }
}

//we could generally implement pooling here
[System.Serializable ()]
public class RuntimeDefinition<T> where T : Definition
{
    public T Parent;

    public RuntimeDefinition (T _parent)
    {
        Parent = _parent;
    }

  /*  public virtual RuntimeDefinition<T> GetCopy(bool exact = false) //if not ecact
    {
        return new RuntimeDefinition<T>(this.Parent);
    }*/
}

public class DefinitionEqualityComparer : IEqualityComparer<Definition>
{
    public readonly static DefinitionEqualityComparer instance;

    static DefinitionEqualityComparer()
    {
        instance = new DefinitionEqualityComparer();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public bool Equals(Definition x, Definition y)
    {
        return x.DefinitionID == y.DefinitionID;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public int GetHashCode(Definition obj)
    {
        return (int)obj.DefinitionID;
    }
}

