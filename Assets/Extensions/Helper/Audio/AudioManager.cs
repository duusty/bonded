﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using UnityEngine.Audio;

//we could use this for music as well with a dedicated corotine
public class CrossFadeMetaInfo
{
    public AudioObject object1;
    public AudioObject object2;

    public Coroutine coroutine; //use mec timing for this :)
}

public enum CrossFadeOption
{
    Immediate,
    Linear
}

public enum AudioSoundVolumeCategory
{
    VolumeMaster,
    VolumeMusic,
    VolumeSFX
}

[System.Serializable]
public class BackgroundMusic
{
    public AudioClip CurrentClip;
    public AudioClip NextClip;
    public float TransitionTime = 1f;

    [HideInInspector()]
    public AudioObject musicObject1, musicObject2; //we need two to crossfade
    private int CurrentLeadMusicObject = 0; //0 = 1, 1 = 2

    public void Initialize()
    {
         musicObject1 = AudioObject.Instantiate(AudioManager.Instance.audioObjectPrefab);
         musicObject2 = AudioObject.Instantiate(AudioManager.Instance.audioObjectPrefab);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void PlayMusic(AudioDefinition sound, CrossFadeOption crossfade)
    {

    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void PauseMusic()
    {

    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void StopMusic()
    {
        musicObject1.StopPlaying(false);
        musicObject2.StopPlaying(false);
    }

    public AudioObject getCurrentMusic()
    {
        return musicObject1;
    }
}

public class AudioManager : SingletonMonoBehaviour<AudioManager>
{
    public AudioMixer masterMixer;
    public BackgroundMusic MusicManager, AmbientManager;

	public AudioObject audioObjectPrefab;
    public FastestPool<AudioObject> audioObjectPool;  //getter should call dont destory on 
    
    public BetterList<AudioObject> activeAudioObjects = new BetterList<AudioObject>();
    public BetterList<CrossFadeMetaInfo> crossFadeMetaInformations = new BetterList<CrossFadeMetaInfo>();

    //keep a list of all active audio objects so we can stop them and return them to the pool just in case

    protected override void Awake()
    {
        base.Awake();
	    if(audioObjectPrefab)
        {
            audioObjectPool = new FastestPool<AudioObject>(50, () => {
                return AudioObject.Instantiate(audioObjectPrefab);
            }, audioObjectPrefab); 
            //Set audio Settings for 2d sounds
            MusicManager.Initialize();
            AmbientManager.Initialize();
        }
        DontDestroyOnLoad(this);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void SetVolume(AudioSoundVolumeCategory mixers, float Volume)
    {
        masterMixer.SetFloat(CachedEnumTypes<AudioSoundVolumeCategory>.valueToString[mixers],Mathf.Log(Mathf.Clamp(Volume,0.001f,1)) * 20);
        //https://stackoverflow.com/questions/46529147/how-to-set-a-mixers-volume-to-a-sliders-volume-in-unity
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public float GetVolume(AudioSoundVolumeCategory mixers)
    {
        float Volume = 0;
        masterMixer.GetFloat(CachedEnumTypes<AudioSoundVolumeCategory>.valueToString[mixers], out Volume);
		return Volume;
    }

    //Pause all //Resume All //Pause Mixer Category // Resume Mixer Category
    //if null we stop all sounds otherwise nur of the specified type
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void StopAllSounds(AudioDefinition definition = null)
    {
        if(definition != null)
            StopAllInstancesFromSoundDefinition(definition);
        else
        {
            foreach(AudioObject obj in activeAudioObjects)
                obj.StopPlaying();
        }
    }

    //return the instance to stop the sound earlier on?
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public AudioObject PlaySound(AudioDefinition definition, Vector3 position, Transform parent = null)
    {
        return audioObjectPool.Get(position,Quaternion.identity,parent).Play(definition, position, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public AudioObject PlaySound(AudioDefinition definition, Vector3 position, Vector2 pitch, Vector2 volume, uint LoopCount, Transform parent = null)
    {
        return audioObjectPool.Get(position,Quaternion.identity,parent).Play(definition, position, pitch, volume, LoopCount, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void StopAllInstancesFromSoundDefinition(AudioDefinition definition)
    {
        foreach(AudioObject obj in definition.metaInfo.CurrentPlayingInstances)
            obj.StopPlaying();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void Crossfade(AudioDefinition nextdefinition)
    {
    //    Timing.RunCouroutine();//fade in coroutine or fadeout coroutine?
        //we need to play another object for this
    }
}

public static class AudioManagerHelper
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static AudioObject Play2DSound(this AudioDefinitionResourceItemReference definition)
    {
        if (definition.Object == null)
            return null;
        return AudioManager.Instance.PlaySound(definition.Object.Object, Vector2.zero);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void PlayMusic(this AudioDefinitionResourceItemReference definition, float StartTime = 0, CrossFadeOption crossfade = CrossFadeOption.Linear)
    {
        if (definition.Object != null)
            AudioManager.Instance.MusicManager.PlayMusic(definition.Object.Object, crossfade);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static AudioObject PlaySound(this AudioDefinitionResourceItemReference definition, Vector3 position, Transform parent = null)
    {
        if (definition.Object == null)
            return null;
        return AudioManager.Instance.PlaySound(definition.Object.Object, position, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static AudioObject PlaySound(this AudioDefinitionResourceItemReference definition, Vector3 position, Vector2 pitch, Vector2 volume, uint LoopCount, Transform parent = null)
    {
        if (definition.Object == null)
            return null;
        return AudioManager.Instance.PlaySound(definition.Object.Object, position, pitch, volume, LoopCount, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void StopAllInstancesFromSoundDefinition(this AudioDefinitionResourceItemReference definition)
    {
        if (definition.Object == null)
            return;
        AudioManager.Instance.StopAllInstancesFromSoundDefinition(definition.Object.Object);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static AudioObject PlaySound(this AudioDefinition definition, Vector3 position, Transform parent = null)
    {
        if (definition == null)
            return null;
        return AudioManager.Instance.PlaySound(definition, position, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static AudioObject PlaySound(this AudioDefinition definition, Vector3 position, Vector2 pitch, Vector2 volume, uint LoopCount, Transform parent = null)
    {
        if (definition == null)
            return null;
        return AudioManager.Instance.PlaySound(definition, position, pitch, volume, LoopCount, parent);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public static void StopAllInstancesFromSoundDefinition(this AudioDefinition definition)
    {
        if (definition == null)
            return;
        AudioManager.Instance.StopAllInstancesFromSoundDefinition(definition);
    }
}