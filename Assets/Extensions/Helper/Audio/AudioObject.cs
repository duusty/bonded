﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using UnityEngine.Audio;
using MEC;

//TODO !!! player movement ( rigidbody moveposition)

//after play is finished we need to select the next subitem
//so no loop == true
[RequireComponent(typeof(AudioSource))]
public class AudioObject : BaseObject, IFastestPoolable
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void Store()
    {
        AudioManager.Instance.audioObjectPool.Store(this);
    }

    private AudioDefinition currentAudioDefinition;
    private CoroutineHandle _playingCoroutineHandle;
    public AudioSource audioSource;
    public uint RestLoops = 999999;

    //public float StartedPlayingAtTime = 0;
    //public float TimeUntilEnd (getter)

    public System.Action<AudioObject> onFinishedPlaying  = (AudioObject obj) => {};

    protected override void Awake()
    {
        base.Awake();
        audioSource = GetComponent<AudioSource>();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void OnPoolGet()
    {
        currentAudioDefinition = null;
        onFinishedPlaying = (AudioObject obj) => {};
        myGameObject.SetActive(true);
        AudioManager.Instance.activeAudioObjects.Add(this);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void OnPoolStore()
    {
        currentAudioDefinition.metaInfo.CurrentPlayingInstances.Remove(this);
        AudioManager.Instance.activeAudioObjects.Remove(this);
        myGameObject.SetActive(false);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void StopPlaying(bool shallStore = true)
    {
        onFinishedPlaying(this);
        //TODO this shouldnt be called for music
        if(shallStore)
            Store();
    }

    //TODO add start time, fade should still work with it.. as fade just increases slowly to volume..

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public AudioObject Play(AudioDefinition definition, Vector3 position, Transform parent = null)
    {
        currentAudioDefinition = definition; //here we can set up cache data for sequencing or randomizing to check depth and index
        AudioDefinition selectedSubItemAudioDefinition = currentAudioDefinition.getPossibleSubItemAudioDefinition();
        currentAudioDefinition.metaInfo.CurrentPlayingInstances.Add(this);
        myTransform.position = position;
        if(parent)
            myTransform.SetParent(parent,true);
        PlayInternalRealItem(selectedSubItemAudioDefinition, selectedSubItemAudioDefinition.MinMaxPitch, selectedSubItemAudioDefinition.MinMaxVolume, selectedSubItemAudioDefinition.LoopCount, selectedSubItemAudioDefinition.FadeInOutTime);
        return this;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public AudioObject Play(AudioDefinition definition, Vector3 position, Vector2 pitch, Vector2 volume, uint LoopCount, Transform parent = null)
    {
        currentAudioDefinition = definition; //here we can set up cache data for sequencing or randomizing to check depth and index
        AudioDefinition selectedSubItemAudioDefinition = currentAudioDefinition.getPossibleSubItemAudioDefinition();
        currentAudioDefinition.metaInfo.CurrentPlayingInstances.Add(this);
        myTransform.position = position;
        if(parent)
            myTransform.SetParent(parent,true);
        PlayInternalRealItem(selectedSubItemAudioDefinition, pitch, volume, LoopCount, selectedSubItemAudioDefinition.FadeInOutTime);
        return this;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    private void PlayInternalRealItem(AudioDefinition selectedSubItemAudioDefinition, Vector2 pitch, Vector2 volume, uint LoopCount, float fadeInOutTime)
    {
        audioSource.outputAudioMixerGroup = selectedSubItemAudioDefinition.mixerGroup;

        if(fadeInOutTime > 0)
        {
            audioSource.volume = 0;
            //Timing.RunCoroutine(_moveMyButton().CancelWith(gameObject));
            Timing.RunCoroutine(FadeRoutine(fadeInOutTime,Random.Range(volume.x,volume.y)));
        }
        else
            audioSource.volume = Random.Range(volume.x,volume.y);
        audioSource.pitch = Random.Range(pitch.x,pitch.y);
        audioSource.time = 0f;
        audioSource.clip = selectedSubItemAudioDefinition.audioClip;
        RestLoops = LoopCount;
        //ein call delayed würde es auch tun wenn wir nicht die coroutine auch unterbrechen könnten
      //  Timing.RunCouroutine(); //play sound and check if we should play it a gain because of the loop? or actually start the coroutine with loop set active by loopcount * pitch * AudioClip length
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void Pause()
    {
        Timing.PauseCoroutines(_playingCoroutineHandle);
        //pause our playing coroutine
        audioSource.Pause();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)][System.Runtime.TargetedPatchingOptOut("")]
    public void Resume()
    {
        Timing.ResumeCoroutines(_playingCoroutineHandle);
        //resume our playing coroutine
        audioSource.Play();
    }

    private IEnumerator<float> FadeRoutine(float fadeTime, float targetVolume)
    {
        if(fadeTime < 0.1f)
        {
            audioSource.volume = targetVolume;
            yield break;
        }
        float startVolume = audioSource.volume;
        float fadespeed = 1f / (fadeTime / Time.deltaTime);
        for(float t = 0f; t < 1f; t += fadespeed)
        {
            audioSource.volume = HelperParent.OptimizedSmoothStep(startVolume, targetVolume, t);
            yield return 0;
        }
        audioSource.volume = targetVolume;
    }

    private IEnumerator<float> playingCoroutine()
    {
        yield return 0;
    }

    private IEnumerator<float> StopCoroutine(float fadeOutTime)
    {
        Timing.KillCoroutines(_playingCoroutineHandle);
        yield return 0;
    }
  /*
        private IEnumerator StopRoutine()
        {
            StopCoroutine(_playingRoutine);
            yield return StartCoroutine(FadeRoutine(_fadeOutTime, 0f));
            _source.Stop();
            _source.clip = null;
            _playingRoutine = null;
            _isFree = true;
            _volume = 0f;
            _source.time = 0f;
            _source.pitch = 1f;

            if (OnFinishedPlaying != null)
            {
                OnFinishedPlaying(this);
            }
        }
*/
}