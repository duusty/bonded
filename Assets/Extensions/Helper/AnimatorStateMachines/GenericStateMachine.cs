﻿using UnityEngine;
using System.Collections;

public class GenericStateMachine : StateMachineBehaviour
{
    public delegate void StateMachineEventHandler(Animator animator, int stateMachinePathHash);
    public delegate void StateEventHandler(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);

    public event StateMachineEventHandler OnStateMachineEntered;
    public event StateMachineEventHandler OnStateMachineExited;

    public event StateEventHandler OnStateEntered;
    public event StateEventHandler OnStateExited;
    public event StateEventHandler OnStateUpdated;

    //public PlayerWorldTile owner;

    public void Initialize(/*PlayerWorldTile worldTile*/)
    {
       // owner = worldTile;
    }

    //public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);

    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
    }

    override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
