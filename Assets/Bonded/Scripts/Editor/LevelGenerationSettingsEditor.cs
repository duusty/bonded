﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelGenerationParameters))]
public class LevelGenerationParametersEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        DrawDefaultInspector();

        if (EditorGUI.EndChangeCheck() && Application.isPlaying)
        {
            Bonded game = Bonded.Instance;
            if (game != null)
            {
                game.RegenerateLevel();
            }
        }
    }
}
