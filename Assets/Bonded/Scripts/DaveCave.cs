﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;
using DG.Tweening;

public class DaveCave : MonoBehaviour
{
    public BezierSpline chickCurve;
    public BezierSpline owlCurve;

    public void Animate(float owlDuration, float chickDuration)
    {
        chickCurve[0].position = Chick.Instance.transform.position;
        owlCurve[0].position = OwlMovement.Instance.transform.position;

        SmoothCurve(chickCurve);
        SmoothCurve(owlCurve);

        StartCoroutine(Animate(chickCurve, Chick.Instance.transform, chickDuration));
        StartCoroutine(Animate(owlCurve, OwlMovement.Instance.transform, owlDuration));

        OwlMovement.Instance.OwlBodyTransform.GetComponentInChildren<Light>().DOIntensity(0, Mathf.Max(0.1f, owlDuration - 1));
        MEC.Timing.CallDelayed(owlDuration - 1, () => OwlMovement.Instance.animation.CrossFade(OwlMovement.Instance.Flapping.name, 0.05f));
        OwlMovement.Instance.OwlBodyTransform.DOLocalMove(Vector3.zero, owlDuration - 1);

        Chick.Instance.enabled = false;
        OwlMovement.Instance.enabled = false;
    }

    IEnumerator Animate(BezierSpline spline, Transform target, float duration)
    {
        float t = 0;
        while (t < duration)
        {
            t += Time.deltaTime;
            target.position = spline.GetPoint(Mathf.Clamp01(t / duration));
            yield return null;
        }
    }

    void SmoothCurve(BezierSpline spline)
    {
        BezierPoint p1 = spline[0];
        BezierPoint p2 = spline[1];

        float distance = Vector3.Distance(p1.position, p2.position);
        p1.followingControlPointLocalPosition = new Vector3(0, 0, distance * 0.3f);
        p2.precedingControlPointLocalPosition = new Vector3(0, 0, -distance * 0.3f);
    }
}
