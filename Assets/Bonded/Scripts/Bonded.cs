﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonded : MonoBehaviour
{
    static Bonded instance;
    public static Bonded Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Bonded>();
            }

            return instance;
        }
    }

    public AudioSource mainAudioSource;

    public LevelGenerator levelGenerator;

    public GlobalSettings settings;
    public IntroState introState;
    public TitleScreenState titleScreenState;
    public LevelState levelState;
    public LevelBuildingState levelBuildingState;
    public GameWinState gameWinState;
    public GameFailState gameFailState;

    public ShaderGlobals shaderGlobals;

    public AudioSource sadMusic;
    public AudioSource battleMusic;
    public AudioSource winMusic;    

    public GameObject owlPrefab;
    [HideInInspector]
    public GameObject gameInstance;

    public float maxDistanceToChick = 30;

    IGameState activeGameState = null;

    void Awake()
    {
        levelBuildingState = new LevelBuildingState(this);

        shaderGlobals.Apply();         
    }

    void Start()
    {
        SetActiveState(introState);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        Debug.Log("Start Game");

        // play intro sequence

        SetActiveState(levelState);
    }

    public void SetActiveState(IGameState state)
    {
        Debug.Assert(state != null);

        if (activeGameState != null)
        {
            activeGameState.LeaveGameState();
        }

        activeGameState = state;
        activeGameState.EnterGameState();
    }

    void Update()
    {
#if UNITY_EDITOR
        shaderGlobals.Apply();
#endif

        if (activeGameState != null)
        {
            activeGameState.UpdateGameState();
        }            
    }

#if UNITY_EDITOR
    private void OnGUI()
    {
        float width = 400;
        GUILayout.BeginArea(new Rect(Screen.width - 400, 0, 400, Screen.height));
        GUILayout.BeginVertical(GUI.skin.box);

        GUILayout.Label($"Current GameState: {activeGameState?.GetType()}");
        if (activeGameState != null)
            activeGameState.DebugDrawGameState();        

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
#endif

    public void RegenerateLevel()
    {
        if (activeGameState == levelBuildingState)
        {
            levelGenerator.Regenerate(1337, 500);
        }
        else
        {
            levelGenerator.Regenerate();
        }
    }
}

public interface IGameState
{
    void EnterGameState();
    void LeaveGameState();
    void UpdateGameState();
    void DebugDrawGameState();
}

public class LevelBuildingState : IGameState
{
    Bonded game;
    public LevelBuildingState(Bonded game)
    {
        this.game = game;
    }

    public void EnterGameState()
    {
        game.RegenerateLevel();
    }

    public void LeaveGameState()
    {
    }

    public void UpdateGameState()
    {
        
    }

    public void DebugDrawGameState()
    {

    }
}

public class RandGenerator
{
    System.Random rng = new System.Random();
    public void Seed(int seed)
    {
        rng = new System.Random(seed);
    }

    public float RandRange(Vector2 range)
    {
        return RandRange(range.x, range.y);
    }

    public float RandRange(float min, float max)
    {
        return (float)rng.NextDouble() * (max - min) + min;
    }

    public float RandAngle()
    {
        return RandRange(0, 360);
    }
}