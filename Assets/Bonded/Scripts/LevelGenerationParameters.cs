﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class LevelGenerationParameters : ScriptableObject
{
    [Header("General")]
    public float generationStartOffset;
    public float generationEndOffset;

    public GroundGeneratorParams groundParams;
    public DefaultGeneratorParams[] generators = new DefaultGeneratorParams[0];
}
