﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Button playButton;
    public Button exitButton;

    void Awake()
    {
        playButton.onClick.AddListener(Bonded.Instance.StartGame);
        exitButton.onClick.AddListener(Bonded.Instance.QuitGame);
    }
}
