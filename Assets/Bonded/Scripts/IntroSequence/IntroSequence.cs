﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class IntroSequence : SingletonMonoBehaviour<IntroSequence>
{
    public Cinemachine.CinemachineVirtualCamera virtualCamera;
    public UnityEngine.UI.Button IntroButton;

    public UnityEngine.UI.Button QuitButton, Creditsbutton;

    public CanvasGroup alphaFadeGroup;

    public EnviroSky sky;

    public GameObject OwlGameObject;
    public OwlMovement OwlMovementScript;
    public Chick chick;

    public Transform CinematicOwlTransform;
    public Canvas canvas;
    public Animation owlAnim;

    public bool isFadingOut = false;

    public BezierSolution.BezierSpline bezierSpline;

    protected override void Awake()
    {
        base.Awake();
        OwlMovementScript.enabled = false;
        chick.enabled = false;
        sky = FindObjectOfType<EnviroSky>();
        sky.currentHour = 0;
        sky.ForceWeatherUpdate();
        sky.moonCamera.Render();
    }

    private void Start()
    {
        sky.currentHour = 0;
        sky.ForceWeatherUpdate();
        sky.moonCamera.Render();
        sky.currentHour = 0;
        sky.ForceWeatherUpdate();
        sky.moonCamera.Render();
        sky.Update();
        IntroButton.onClick.AddListener(() =>
        {
            FadeOut();
        });
        QuitButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }

    void Update()
    {
        if(Input.anyKeyDown)
        {
            FadeOut();
        }
    }
    float hour = 1;
    void FadeOut()
    {
        if (isFadingOut)
            return;

        enabled = false;
        isFadingOut = true;
        owlAnim.CrossFade("Flying");
        owlAnim.CrossFadeQueued("Glide");
        chick.enabled = true;

        alphaFadeGroup.DOFade(0, 1f).OnComplete(() => { OwlMovementScript.enabled = true; canvas.enabled = false; });
        MEC.Timing.CallDelayed(0.5f, () => {
            virtualCamera.enabled = false;
            owlAnim.CrossFade("Flying"); });
        DOTween.To(() => { return hour; }, (float f) => { hour = f; }, 0, 1f).OnUpdate(() => CinematicOwlTransform.position = bezierSpline.GetPoint(hour));

        Bonded.Instance.StartGame();
    }
}
