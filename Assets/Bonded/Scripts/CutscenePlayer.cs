﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CutscenePlayer : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public Image img1;
    public Image img2;
    public TextMeshProUGUI skipText;
    public TextMeshProUGUI subtitleText;
    public float skipTextDuration = 2.0f;

    [HideInInspector]
    public Cutscene cutscene;
    [HideInInspector]
    public float curT;
    [HideInInspector]
    public int curImageIndex;
    [HideInInspector]
    public int curAudioIndex;

    float skipTime;

    public bool IsFinished => curT >= cutscene.duration;

    public void StartCutscene(Cutscene cutscene)
    {
        this.cutscene = cutscene;

        img1.sprite = null;
        img1.color = Color.black;

        img2.sprite = null;
        img2.color = Color.black;

        curT = 0;
        curImageIndex = 0;
        curAudioIndex = 0;

        skipText.enabled = false;
        subtitleText.enabled = false;
    }

    public void UpdateCutscene(float deltaTime)
    {       
        curT += deltaTime;

        if (curImageIndex < cutscene.images.Length)
        {
            CutsceneImage image = cutscene.images[curImageIndex];
            if (curT >= image.at)
            {
                img1.sprite = img2.sprite;
                img1.color = img2.color;

                img2.sprite = image.sprite;
                Color c = image.color;
                c.a = 0;
                img2.color = c;
                img2.DOFade(1, 2.0f).SetEase(Ease.InOutCubic);

                curImageIndex++;
            }
        }

        if (curAudioIndex < cutscene.audioClips.Length)
        {
            CutsceneAudio audio = cutscene.audioClips[curAudioIndex];
            if (curT >= audio.at)
            {
                Bonded.Instance.mainAudioSource.PlayOneShot(audio.clip);
                subtitleText.enabled = true;
                subtitleText.text = audio.subtitle;
                MEC.Timing.CallDelayed(audio.clip.length, () =>
                {
                    subtitleText.enabled = false;
                });

                curAudioIndex++;
            }
        }

        if (Input.anyKeyDown)
        {
            if (skipText.enabled)
            {
                curT = cutscene.duration;
            }
            else
            {
                skipText.enabled = true;
                skipTime = Time.time;
            }
        }

        if (skipText.enabled && Time.time - skipTime > skipTextDuration)
        {
            skipText.enabled = false;
        }
    }
}
