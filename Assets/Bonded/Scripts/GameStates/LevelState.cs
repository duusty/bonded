﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[Serializable]
public class VoiceLine
{
    public AudioClip[] clips;

    public float minDelayToNextVoiceLine;

    [NonSerialized]
    public int currentVoiceLine;

    [NonSerialized]
    public float lastT;

    public void Play()
    {
        if (Time.time > lastT + minDelayToNextVoiceLine)
        {
            MEC.Timing.CallDelayed(0.5f, () =>
            {
                Bonded.Instance.mainAudioSource.PlayOneShot(clips[currentVoiceLine]);
            });

            currentVoiceLine = (currentVoiceLine + 1) % clips.Length;
            lastT = Time.time;
        }
    }
}

public class LevelState : MonoBehaviour, IGameState
{
    public float maxDistanceToChick = 30;
    public int numChick2Lives = 5;

    [Tooltip("Level duration in seconds")]
    public float levelDuration = 120;

    public float minMusicVolume = 0.1f;
    public float maxMusicVolume = 1.0f;

    public VoiceLine onChickHitVoiceLine;    
    public VoiceLine onPredatorKillVoiceLine;

    public float caveOffset = 100;
    public DaveCave daveCave;

    float currentT;
    public int curNumLives;

    public float winGameDuration = 10;
    public bool isWon = false;

    Bonded game;
    void Awake()
    {
        game = Bonded.Instance;
    }

    public void TakeDamage()
    {
        curNumLives--;

        if (curNumLives != 0)
            onChickHitVoiceLine.Play();
    }

    public void EnterGameState()
    {
        NormalizedTime = 0;
        game.levelGenerator.ResetLevel((int)DateTime.Now.Ticks * 1337);
        game.levelGenerator.UpdateLevel(0, 0);

        Chick.Instance.myGameObject.SetActive(true);
        Chick.Instance.ResetChick();

        OwlMovement.Instance.myGameObject.SetActive(true);

        currentT = 0;
        curNumLives = numChick2Lives;

        game.battleMusic.volume = 0;
      //  game.battleMusic.DOFade(1, minMusicVolume);
        game.battleMusic.Play();

        onChickHitVoiceLine.lastT = -100000000;
        //onChickHitVoiceLine.currentVoiceLine = UnityEngine.Random.Range(0, onChickHitVoiceLine.clips.Length);
        onChickHitVoiceLine.currentVoiceLine = 0;

        onPredatorKillVoiceLine.lastT = -100000000;
        //onPredatorKillVoiceLine.currentVoiceLine = UnityEngine.Random.Range(0, onPredatorKillVoiceLine.clips.Length);
        onPredatorKillVoiceLine.currentVoiceLine = 0;

        isWon = false;
    }

    public void LeaveGameState()
    {       
        foreach (var predator in Predator.predatorsHashset.ToDynList())
        {
            if (predator != null)
                predator.InstaKill();
        }
    }

    public float NormalizedTime = 0;

    public void UpdateGameState()
    {
        currentT += Time.deltaTime;

        NormalizedTime = Mathf.Clamp01(currentT / levelDuration);
        game.levelGenerator.UpdateLevel(OwlMovement.Instance.transform.position.z, NormalizedTime);        

        if (currentT >= levelDuration && !isWon)
        {
            WinGame();
        }

        if (curNumLives <= 0)
        {
            LooseGame();
        }

        if (!isWon)
        {
            game.battleMusic.volume = Mathf.Lerp(game.battleMusic.volume, Bonded.Instance.settings.MusicVolumeCurve.Evaluate(Predator.predators.size * (6 - curNumLives)), Time.deltaTime * 25);
            game.battleMusic.pitch = Mathf.Lerp(game.battleMusic.pitch, Bonded.Instance.settings.MusicVolumeCurve.Evaluate(Predator.predators.size * (6 - curNumLives)), Time.deltaTime * 25);
            //game.battleMusic.volume = Mathf.Lerp(minMusicVolume, maxMusicVolume, NormalizedTime);
        }

        if (isWon)
        {
            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            // KILL THEM ALL
            foreach (var predator in Predator.predatorsHashset.ToDynList())
            {
                if (predator != null)
                    predator.InstaKill();
            }
        }
    }

    void LooseGame()
    {
        game.battleMusic.DOFade(0, 1.0f);
        game.SetActiveState(game.gameFailState);
    }

    void WinGame()
    {
        if (isWon)
            return;

        isWon = true;
        foreach (var predator in Predator.predatorsHashset.ToDynList())
        {
            if (predator != null)
                predator.InstaKill();
        }

        game.battleMusic.DOFade(0, 1.0f);
        game.winMusic.volume = 0;
        game.winMusic.DOFade(1, 2.0f);
        game.winMusic.Play();

        DaveCave cave = Instantiate(daveCave);
        cave.transform.position = new Vector3(0, 0, OwlMovement.Instance.transform.position.z + caveOffset);
        cave.Animate(winGameDuration, winGameDuration * 0.7f);

        MEC.Timing.CallDelayed(winGameDuration, () =>
        {
            Destroy(cave.gameObject, 2);
            game.SetActiveState(game.gameWinState);
        });
    }

    public void DebugDrawGameState()
    {
        GUILayout.Label($"Distance to Chick: {DistanceToChick:f2}");
        if (GUILayout.Button("Win Game"))
        {
            WinGame();
        }

        if (GUILayout.Button("Loose Game"))
        {
            LooseGame();
        }
    }

    public float DistanceToChick => Chick.Instance.myTransform.position.z - OwlMovement.Instance.myTransform.position.z;
}
