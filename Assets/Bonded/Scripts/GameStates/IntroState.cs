﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class IntroState : MonoBehaviour, IGameState
{
    public Cutscene cutscene;
    public CutscenePlayer player;

    Bonded game;
    void Awake()
    {
        game = Bonded.Instance;
    }

    public void EnterGameState()
    {       
        player.StartCutscene(cutscene);
        player.canvasGroup.alpha = 1;
    }

    public void LeaveGameState()
    {
        player.canvasGroup.DOFade(0, 1.0f).OnComplete(() => player.gameObject.SetActive(false));        
    }

    public void UpdateGameState()
    {
        player.UpdateCutscene(Time.deltaTime);
        if (player.IsFinished)
        {            
            game.SetActiveState(game.titleScreenState);
        }
    }

    public void DebugDrawGameState()
    {
        GUILayout.Label($"Current Time: {player.curT}");
        GUILayout.Label($"Current Image: {player.curImageIndex}");
        GUILayout.Label($"Current Audio: {player.curAudioIndex}");

        if (GUILayout.Button("Restart"))
        {
            player.StartCutscene(cutscene);
        }

        if (GUILayout.Button("Skip"))
        {
            game.SetActiveState(game.titleScreenState);
        }
    }
}
