﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TitleScreenState : MonoBehaviour, IGameState
{
    Bonded game;

    void Awake()
    {
        game = Bonded.Instance;
    }

    public void EnterGameState()
    {
        if (game.gameInstance != null)
            DestroyImmediate(game.gameInstance);

        game.gameInstance = Instantiate(game.owlPrefab);

        EnviroSky.instance.Player = OwlMovement.Instance.gameObject;
        EnviroSky.instance.PlayerCamera = Camera.main;

        Chick.Instance.myGameObject.SetActive(false);
        OwlMovement.Instance.myGameObject.SetActive(false);

        game.sadMusic.volume = 0;
        game.sadMusic.DOFade(0.5f, 1.0f);
        game.sadMusic.Play();
    }

    public void LeaveGameState()
    {
        
        game.sadMusic.DOFade(0.0f, 30.0f);
    }

    public void UpdateGameState()
    {
    }

    public void DebugDrawGameState()
    {

    }
}
