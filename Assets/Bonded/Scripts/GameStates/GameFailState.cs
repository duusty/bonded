﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameFailState : MonoBehaviour, IGameState
{
    public Cutscene cutscene;
    public CutscenePlayer player;

    Bonded game;
    void Awake()
    {
        game = Bonded.Instance;
        player.gameObject.SetActive(false);        
    }

    public void EnterGameState()
    {
        player.gameObject.SetActive(true);
        player.canvasGroup.alpha = 0;
        player.canvasGroup.DOFade(1, 1.0f);
        player.StartCutscene(cutscene);
    }

    public void LeaveGameState()
    {
        player.canvasGroup.DOFade(0, 1.0f).OnComplete(() => player.gameObject.SetActive(false));
    }

    public void UpdateGameState()
    {
        player.UpdateCutscene(Time.deltaTime);
        if (player.IsFinished)
        {
            game.SetActiveState(game.titleScreenState);
        }
    }

    public void DebugDrawGameState()
    {
        GUILayout.Label($"Current Time: {player.curT}");
        GUILayout.Label($"Current Image: {player.curImageIndex}");
        GUILayout.Label($"Current Audio: {player.curAudioIndex}");

        if (GUILayout.Button("Restart"))
        {
            player.StartCutscene(cutscene);
        }

        if (GUILayout.Button("Skip"))
        {
            game.SetActiveState(game.titleScreenState);
        }
    }
}
