﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class GlobalSettings : ScriptableObject
{
    [Header("General")]
    public float MaxHorizontalMovement = 6, MinHorizontalMovement  = -2;

    [Header("General")]
    public float ChickMaxHorizontalMovement = 6, ChickMinHorizontalMovement = -2;

    public GameObject[] Predators;

    public AudioClip[] branchHitSounds;
    public Vector2 minMaxPitchBranchHitSounds = new Vector2(0.9f, 1.1f);

    public AnimationCurve MusicVolumeCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 1));
    public AnimationCurve MusicVolumePitchCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 1));
}
