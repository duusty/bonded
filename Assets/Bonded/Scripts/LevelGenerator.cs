﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Profiling;
using static UnityEngine.ParticleSystem;

public class LevelGenerator : MonoBehaviour
{
    public LevelGenerationParameters generationParams;

    public Transform levelRoot;

    GroundGenerator groundGenerator;
    DefaultGenerator[] generators = new DefaultGenerator[0];

    int seed;
    float levelProgress;
    float currentDistance;    

    private void Start()
    {
        levelRoot = new GameObject("LevelRoot").transform;
    }

    public void ResetLevel(int seed)
    {
        this.seed = seed;

        if (groundGenerator == null)
            groundGenerator = new GroundGenerator("Ground", this);

        groundGenerator.Reset(seed, generationParams.groundParams);

        if (generators.Length != generationParams.generators.Length)
        {
            Array.Resize(ref generators, generationParams.generators.Length);
        }

        for (int i = 0; i < generators.Length; i++)
        {
            if (generators[i] == null)
            {
                generators[i] = new DefaultGenerator("Gen" + i, this);
            }

            generators[i].Reset(seed + i, generationParams.generators[i]);
        }

        currentDistance = 0;
        levelProgress = 0;
    }

    public void UpdateLevel(float distance, float levelProgress)
    {
        this.levelProgress = levelProgress;
        currentDistance = distance;
        Generate(true);
    }

    public void Regenerate(int seed, float distance)
    {
        ResetLevel(seed);
        currentDistance = distance;
        Generate(false);
    }

    public void Regenerate()
    {
        ResetLevel(seed);
        Generate(true);
    }

    void Generate(bool withDeletion)
    {
        float currentGenerationDistance = currentDistance + generationParams.generationStartOffset;
        float currentDeletionDistance = currentDistance + generationParams.generationEndOffset;

        Profiler.BeginSample("LevelGenerator.Generate");

        groundGenerator.Generate(currentGenerationDistance);
        foreach (var generator in generators)
        {
            generator.Generate(currentGenerationDistance, levelProgress);
        }

        if (withDeletion)
        {
            groundGenerator.DestroyObjects(currentDeletionDistance);
            foreach (var generator in generators)
            {
                generator.DestroyObjects(currentDeletionDistance);
            }
        }

        Profiler.EndSample();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(new Vector3(0, 1, currentDistance), new Vector3(1.0f, 1.0f, 1.0f));

        float extent = generationParams.generationStartOffset - generationParams.generationEndOffset;

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(
            new Vector3(
                0, 
                1,
                currentDistance + generationParams.generationStartOffset - extent * 0.5f
            ),
            new Vector3(
                generationParams.groundParams.groundPlaneSize, 
                1.0f,
                extent
            )
        );
    }
}

// XXX: class because we don't have default values for struct
[Serializable]
public class DefaultGeneratorParams
{
    public string name;
    public PrefabInfo[] prefabs = new PrefabInfo[0]; 
    public Vector2 spacing = new Vector2(4, 5);
    public Vector2 centerOffset = new Vector2(2, 3);
    public Vector2 size = new Vector2(0.8f, 1.2f);
    public Vector2 rotation = new Vector2(0, 360);
    public float RotationOffsetMax = 0.25f;

    public AnimationCurve densityCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 1));

    [Serializable]
    public class PrefabInfo
    {
        public GameObject prefab = null;
        public float chance = 1;
    }
}

public class DefaultGenerator
{
    ObjectPool pool;
    LevelGenerator levelGenerator;
    DefaultGeneratorParams param;
    List<ObjectInstance> instances = new List<ObjectInstance>();
    float currentDistance;
    float nextOffset;
    System.Random rng;
    string name;
    float sign;
    float totalChance;

    public DefaultGenerator(string name, LevelGenerator levelGenerator)
    {
        this.name = name;
        this.levelGenerator = levelGenerator;
    }

    public void Reset(int seed, DefaultGeneratorParams param)
    {
        this.param = param;
        rng = new System.Random(seed);

        GameObject[] prefabArray = param.prefabs.Select(x => x.prefab).ToArray();
        if (pool == null)
        {
            pool = new ObjectPool(name + "_Pool", prefabArray, 20);
        }

        pool.RecycleAll(instances);
        pool.SetPrefabs(prefabArray);
        currentDistance = 0;
        nextOffset = RandRange(param.spacing);
        sign = rng.NextDouble() >= 0.5 ? -1 : 1;
        totalChance = param.prefabs.Sum(x => x.chance);
    }

    public void Generate(float currentGenerationDistance, float levelProgress)
    {
        Profiler.BeginSample("DefaultGenerator.Generate");

        while (true)
        {
            float density = param.densityCurve.Evaluate(levelProgress);
            float nextDistance = currentDistance + nextOffset * density;
            if (nextDistance > currentGenerationDistance)
                break;

            Vector3 position;
            position.x = RandRange(param.centerOffset);
            position.y = 0;
            position.z = nextDistance;

           // RandRange(param.rotation)
            float angle = RandRange(param.rotation);

            float sign = RandSign();
            position.x *= sign;

            // XXX: if we swap sides rotate the object by 180°
            // This way the objects primary axis always points towards the center of the level
            // Need this to align branch obstacles
            if (sign < 0)
            {
                angle += 180;
            }
           // RandAngle
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.Lerp(Vector3.up,new Vector3(1-((float)rng.NextDouble()*2), 0, 1 - ((float)rng.NextDouble() * 2)), (float)rng.NextDouble()*param.RotationOffsetMax/ density));
           // rotation.x *= RandRange(param.rotationX);

            float sample = RandRange(new Vector2(0, totalChance));
            int modelIndex = 0;
            for (int i = 0; i < param.prefabs.Length; i++)
            {
                sample -= param.prefabs[i].chance;
                if (sample <= 0)
                {
                    modelIndex = i;
                    break;
                }
            }

            ObjectInstance rock = pool.Instantiate(modelIndex, position, rotation, levelGenerator.levelRoot);
            float scale = RandRange(param.size);
            rock.gameObject.transform.localScale = new Vector3(scale, scale, scale);

            instances.Add(rock);

            currentDistance = nextDistance;
            nextOffset = RandRange(param.spacing);
        }        

        Profiler.EndSample();
    }

    public void DestroyObjects(float currentDeletionDistance)
    {
        while (instances.Count > 0)
        {
            ObjectInstance instance = instances[0];
            if (instance.gameObject.transform.localPosition.z < currentDeletionDistance)
            {
                pool.Recycle(instance);
                instances.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }

    float RandRange(Vector2 range)
    {
        return (float)rng.NextDouble() * (range.y - range.x) + range.x;
    }

    float RandSign()
    {
        //return rng.NextDouble() >= 0.5 ? 1.0f : -1.0f;
        float result = sign;
        sign = -sign;
        return result;
    }

    float RandAngle()
    {
        return (float)rng.NextDouble() * 360.0f;
    }
}

[Serializable]
public struct GroundGeneratorParams
{
    public GameObject prefab;
    public Mesh mesh;
    public float groundPlaneSize;
}

public class GroundGenerator
{
    ObjectPool pool;
    LevelGenerator levelGenerator;
    GroundGeneratorParams param;
    List<ObjectInstance> instances = new List<ObjectInstance>();
    string name;
    float nextGroundPlaneDistance;

    public GroundGenerator(string name, LevelGenerator levelGenerator)
    {
        this.name = name;
        this.levelGenerator = levelGenerator;
    }

    public void Reset(int seed, GroundGeneratorParams param)
    {
        this.param = param;

        if (pool == null)
        {
            pool = new ObjectPool(name + "_Pool", new[] { param.prefab }, 20);
        }

        pool.RecycleAll(instances);
        pool.SetPrefabs(new[] { param.prefab } );
        nextGroundPlaneDistance = 0;
    }

    public void Generate(float currentGenerationDistance)
    {
        Profiler.BeginSample("DefaultGenerator.Generate");

        while (nextGroundPlaneDistance < currentGenerationDistance)
        {
            Vector3 groundPlanePosition;
            groundPlanePosition.x = 0;
            groundPlanePosition.y = 0;
            groundPlanePosition.z = nextGroundPlaneDistance + param.groundPlaneSize * 0.5f;

            ObjectInstance ground = pool.Instantiate(0, groundPlanePosition, Quaternion.identity, levelGenerator.levelRoot);
            ground.gameObject.transform.localScale = Vector3.one * param.groundPlaneSize * 0.5f;

            instances.Add(ground);

            nextGroundPlaneDistance += param.groundPlaneSize;
        }

        Profiler.EndSample();
    }

    public void DestroyObjects(float currentDeletionDistance)
    {
        while (instances.Count > 0)
        {
            ObjectInstance instance = instances[0];
            if (instance.gameObject.transform.localPosition.z + param.groundPlaneSize < currentDeletionDistance)
            {
                pool.Recycle(instance);
                instances.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }
}

// TODO: Add variants
public struct ObjectInstance
{
    public int index;
    public GameObject gameObject;

    public ObjectInstance(int index, GameObject gameObject)
    {
        this.index = index;
        this.gameObject = gameObject;
    }
}

public class ObjectPool : IDisposable
{
    Transform poolParent;
    GameObject[] prefabs;
    List<GameObject>[] objectPool;
    string name;
    int initialSize;

    public int NumVariants => prefabs.Length;

    public ObjectPool(string name, GameObject[] prefabs, int initialSize)
    {
        this.name = name;
        this.initialSize = initialSize;

        Initialize(prefabs);
    }

    void Initialize(GameObject[] prefabs)
    {
        this.prefabs = (GameObject[])prefabs.Clone();
        poolParent = new GameObject(name).transform;
        poolParent.gameObject.SetActive(false);

        objectPool = new List<GameObject>[prefabs.Length];

        for (int j = 0; j < prefabs.Length; j++)
        {
            List<GameObject> pool = new List<GameObject>();
            for (int i = 0; i < initialSize; i++)
            {
                pool.Add(GameObject.Instantiate(prefabs[j], poolParent));
            }
            objectPool[j] = pool;
        }
    }

    public void SetPrefabs(GameObject[] prefabs)
    {
        if (!ArrayContentEquals(this.prefabs, prefabs))
        {
            GameObject.Destroy(poolParent.gameObject);
            Initialize(prefabs);
        }
    }

    static bool ArrayContentEquals(GameObject[] a, GameObject[] b)
    {
        if (a == null && b == null)
            return true;

        if ((a == null && b != null) || (a != null && b == null))
            return false;

        if (a.Length != b.Length)
            return false;

        for (int i = 0; i < a.Length; i++)
        {
            if (a[i] != b[i])
                return false;
        }

        return true;
    }

    public void Dispose()
    {
        GameObject.Destroy(poolParent.gameObject);
    }

    public void Recycle(ObjectInstance obj)
    {
        objectPool[obj.index].Add(obj.gameObject);
        obj.gameObject.transform.SetParent(poolParent);
    }

    public void RecycleAll(List<ObjectInstance> objs)
    {
        for (int i = 0; i < objs.Count; i++)
        {
            Recycle(objs[i]);
        }

        objs.Clear();
    }

    public ObjectInstance Instantiate(int index, Vector3 position, Quaternion rotation, Transform parent)
    {
        GameObject result;

        List<GameObject> pool = objectPool[index];
        if (pool.Count > 0)
        {
            result = pool[pool.Count - 1];
            pool.RemoveAt(pool.Count - 1);
        }
        else
        {
            result = GameObject.Instantiate(prefabs[index]);            
        }

        Transform transform = result.transform;
        transform.SetParent(parent, false);
        transform.localPosition = position;
        transform.localRotation = rotation;

        return new ObjectInstance(index, result);
    }
}
