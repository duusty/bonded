﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class Cutscene : ScriptableObject
{
    public float duration;
    public CutsceneImage[] images = new CutsceneImage[0];
    public CutsceneAudio[] audioClips = new CutsceneAudio[0];
}

[Serializable]
public struct CutsceneImage
{
    public float at;
    public Sprite sprite;
    public Color color;
}

[Serializable]
public struct CutsceneAudio
{
    public float at;
    public AudioClip clip;
    public string subtitle;
}