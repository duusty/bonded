﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Chick : SingletonMonoBehaviour<Chick>
{
    public float RunSpeed = 1, RollAnimationSpeed = 1;
    public const int GlideAnimationLayer = 0;
    public Animation animation;

    public AnimationClip HighJump, Roll, PlayDead, Run;

    public float speed = 10;

    public Vector3 position;
    public float distance;

    public Material material;
    public Renderer renderer;

    public Vector3 RaycastDirection;
    public RaycastHit rayCastHit;
    public Transform SphereCastOrigin;
    public float SphereCastRadius = 0.1f;
    public LayerMask GroundCheckJumpLayerMask;
    public ParticleSystem footStepsParticles;


    public BezierSolution.BezierSpline bezierCurve;

    public bool isInJump = false;
    public float RollDuration = 2;

    public bool isDead = false;

    public AudioClip[] OnHitAudioClips;
    public AudioClip JumpAudioClip, DieAudioClip;
    public AudioSource audioSource;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        material = renderer.sharedMaterial;
        renderer.material = material;
        material.SetColor("_EmissionColor", Color.white*1.5f);
        animation.AddClip(HighJump, HighJump.name);
        animation.AddClip(Roll, Roll.name);
        animation.AddClip(PlayDead, PlayDead.name);
        animation.AddClip(Run, Run.name);

        animation[Run.name].layer = GlideAnimationLayer;
        animation[Run.name].speed = RunSpeed;
        animation[Run.name].weight = 1;
        animation[Run.name].blendMode = AnimationBlendMode.Blend;
        animation[Run.name].wrapMode = WrapMode.Loop;
        animation[Run.name].enabled = true;

        animation[Roll.name].layer = GlideAnimationLayer;
        animation[Roll.name].speed = RollAnimationSpeed;
        animation[Roll.name].wrapMode = WrapMode.Loop;

        animation[PlayDead.name].layer = GlideAnimationLayer;
        animation[PlayDead.name].speed = 1;
        animation[PlayDead.name].wrapMode = WrapMode.ClampForever;

        animation[HighJump.name].layer = GlideAnimationLayer;
        animation[HighJump.name].speed = 1;
        animation[HighJump.name].wrapMode = WrapMode.Default;

        animation.Play(Run.name, PlayMode.StopAll);
        footStepsParticles.Play();
    }

    public void ResetChick()
    {
        position = Vector3.zero;
    }

    public float MovementSinusScale = 2;
    public float MovementSinusSpeed = 2;
    public float MovementCosinusScale = 2;
    public float MovementCosinusSpeed = 2;

    public float PreSinusScale = 1f;
    public float PreSinusSpeed = 1f;

    public GlobalSettings settings;

    float SpeedModifierCollisionNear = 1;
    float SpeedBoostOnHit = 0;

    public void TakeDamage()
    {
        if (isDead)
            return;

        OwlCamera.Instance.scanner.ScannerOrigin = myTransform;
        OwlCamera.Instance.scanner.ScanDistance = 0;
        animation.CrossFadeQueued(Roll.name);
        isInJump = true;
        audioSource.pitch = Random.Range(0.95f, 1.05f);
        audioSource.PlayOneShot(OnHitAudioClips[Random.Range(0, OnHitAudioClips.Length)],0.5f);

        DG.Tweening.DOTween.To(() =>
        {
            return OwlCamera.Instance.scanner.ScanDistance;
        }, (float s) =>
         {
             SpeedBoostOnHit = 0.5f - (s / 600f);
             OwlCamera.Instance.scanner.ScanDistance = s;
         }, 300, 5).OnComplete(() => 
         {
             OwlCamera.Instance.scanner.ScanDistance = 0;
             SpeedBoostOnHit = 0;
             animation.CrossFade(Run.name);
             SpeedModifierCollisionNear = 1;
             isInJump = false;
         });

        Bonded.Instance.levelState.TakeDamage();
        material.SetColor("_EmissionColor", Color.red*4);
        material.DOColor(Color.white * 1.5f, "_EmissionColor", 0.1f).SetLoops(2).OnComplete(() => material.SetColor("_EmissionColor", Color.white * 1.5f));
        if (Bonded.Instance.levelState.curNumLives <= 0)
            Die();
    }

    public void Die()
    {
        if (isDead)
            return;
        audioSource.pitch = 1f;
        audioSource.PlayOneShot(DieAudioClip, 0.8f);

        isDead = true;
        animation.CrossFade(PlayDead.name);
        enabled = false;
    }

    void Update()
    {
        if (isDead)
            return;
        distance += (speed * Time.deltaTime * SpeedModifierCollisionNear)+ SpeedBoostOnHit;

        float PreSinus = (1+Mathf.Sin(Time.time * PreSinusSpeed)) * PreSinusScale;

        position.x = (Mathf.Sin(Time.time * MovementSinusSpeed* PreSinus) * MovementSinusScale) + (Mathf.Cos(Time.time * MovementCosinusSpeed* PreSinus) * MovementCosinusScale);
        position.x = Mathf.Clamp(position.x, settings.ChickMinHorizontalMovement, settings.ChickMaxHorizontalMovement);
        position.z = distance;

        if(isInJump)
            position.y = bezierCurve.GetPoint(animation[HighJump.name].normalizedTime).y;

        myTransform.localPosition = position;
    }

    private void FixedUpdate()
    {
        if (isDead)
            return;
        if(Physics.SphereCast(SphereCastOrigin.position,SphereCastRadius, RaycastDirection, out rayCastHit, 3f, GroundCheckJumpLayerMask.value))
        {
            if (isInJump)
            {
                SpeedModifierCollisionNear = (rayCastHit.distance / 3f);
            }
            else
            {
                Jump();
            }
        }
    }

    void Jump()
    {
        if (isInJump)
            return;
        isInJump = true;
        audioSource.pitch = Random.Range(0.95f, 1.05f);
        audioSource.PlayOneShot(JumpAudioClip, 0.12f);

        animation.CrossFade(HighJump.name);
        animation.CrossFadeQueued(Roll.name);

        MEC.Timing.CallDelayed(animation[HighJump.name].length + RollDuration, () =>
         {
             isInJump = false;
             animation.CrossFade(Run.name);
             SpeedModifierCollisionNear = 1;
         });
    }
}
