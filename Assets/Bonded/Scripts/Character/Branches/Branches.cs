﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Branches : BaseObject
{
    public Collider collision;
    public MaterialPropertyBlock propertyBlock;
    public Renderer Renderer;

    public Vector3 bounceAxisMultiplier = new Vector3(1, 1, 1);
    public float PunchDuration = 1;
    public int PunchVibro = 4;
    public float PunchElasticy = 4;

    public Transform RotationTransform;

    public float StunDuration = 2;

    public float SnowFactor = 0.24f;
    public float SnowFadeOutDuration = 0.1f;

    public ParticleSystem SnowParticleSystem;
    public Cinemachine.CinemachineImpulseSource cinemachineImpulseSource;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        propertyBlock = new MaterialPropertyBlock();
        propertyBlock.SetFloat("_UpFactor", SnowFactor);
        Renderer.SetPropertyBlock(propertyBlock);
    }

    private void OnTriggerEnter(Collider other)
    {
        OwlMovement owl = other.GetComponentInParent<OwlMovement>();
        if(owl != null && !owl.inAttackSequence)
        {
            Debug.Log("Collision" + myGameObject.name, myGameObject);
            Debug.Log("Collision2" + other.gameObject.name, other.gameObject);

            collision.enabled = false;
            enabled = false;

            Vector3 direction = (RotationTransform.position - owl.myTransform.position);
            direction = Vector3.Lerp(direction, owl.myRigidbody.velocity,0.7f);


            direction.x *= bounceAxisMultiplier.x;
            direction.y *= bounceAxisMultiplier.y;
            direction.z *= bounceAxisMultiplier.z;

            RotationTransform.DOPunchRotation(direction, PunchDuration, PunchVibro, PunchElasticy);
            cinemachineImpulseSource.GenerateImpulseAt(owl.myTransform.position, direction);
            owl.SetStunTime(StunDuration);

            audioSource.Play();
            SnowParticleSystem.Play();
            DOTween.To(() => { return SnowFactor; }, (float f) =>
            {
                SnowFactor = f;
            }, 0, SnowFadeOutDuration).OnUpdate(() =>
            {
                propertyBlock.SetFloat("_UpFactor", SnowFactor);
                Renderer.SetPropertyBlock(propertyBlock);
            });
            
        }
    }


}
