﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAimTrigger : BaseObject
{
    public BetterList<Predator> targetList = new BetterList<Predator>();
    private HashSet<Predator> targetHashset = new HashSet<Predator>();

    private void Start()
    {
        targetList.Clear();
        targetHashset.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Anything Entered");
        Predator pre = other.GetComponent<Predator>();
        if (pre == null)
            return;
        Debug.Log("Test Entered");
        if (targetHashset.Add(pre))
            targetList.Add(pre);
    }

    private void OnTriggerExit(Collider other)
    {
        Predator pre = other.GetComponent<Predator>();
        if (pre == null)
            return;
        if (targetHashset.Remove(pre))
            targetList.Remove(pre);
    }

    public Vector3 getRandomPointInArea(out Predator lastPredator)
    {
        if(targetList.size > 0)
        {
            lastPredator = targetList[Random.Range(0, targetList.size)];
            return lastPredator.myTransform.position;
        }
        else
        {
            lastPredator = null;
            return myTransform.position;
        }
    }

    public void Remove(Predator pre)
    {
        if (targetHashset.Remove(pre))
            targetList.Remove(pre);
    }
}
