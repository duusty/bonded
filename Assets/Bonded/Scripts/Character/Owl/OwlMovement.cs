﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OwlMovement : SingletonMonoBehaviour<OwlMovement>
{

    public const int GlideAnimationLayer = 0;
    public const int FlappingAnimationLayer = 1;
    public const int AttackAnimationLayer = 2;

    public GlobalSettings settings;
    public float OutOfBoundsCounterVelocity = 100;


    public float FlappingAnimationSpeed = 1;
    public float GlideAnimationSpeed = 1;
    public float AttackAnimationSpeed = 1;
    public float HitAnimationSpeed = 1;

    public AnimationClip Glide, Flapping, Attack, Hit, BarrelRollAnimation;

  //  public Bezier3DSpline attackBezierCurve;

    public Animation animation;
    public AutoAimTrigger targetTrigger;
    public Rigidbody myRigidbody;
    public Transform OwlBodyTransform, OwlRendererTransform;

    public float MinSpeedVertical = 2;
    public float VerticalSpeedAccel = 0.5f;

    public float HorizontalSpeed = 2;


    public float SpeedDecreasment = 1;
    public Vector3 newVelocity;

    public bool inAttackSequence = false;

    public AnimationCurve flySpeedInAttackAnim = new AnimationCurve(new Keyframe(2, 200));

    public float NextTimeFlyingAnim;

    public BezierSolution.BezierSpline bezierSpline;
    public BezierSolution.BezierPoint bezierGroundPoint;

    public float RandomTimeForFlying = 1f;
    public float TimeForFlyingMin = 4f;

    public ParticleSystem GlitterParticleSystem;
    public ParticleSystem SnowBurstParticlesOnHit;

    public float StunDuration = 0;
    public AnimationCurve StunSpeedMultiplierCurve = new AnimationCurve(new Keyframe(0f, 1f, 2f, 0f));
    public AnimationCurve StunBloodPulseCurve = new AnimationCurve(new Keyframe(0f, 0f, 2f, 1f));

    public Vector3 lastAttackPoint;

    public AnimationCurve SampleCurveBezier = new AnimationCurve();
    public AnimationCurve SampleCurveSpeedBezier = new AnimationCurve();

    public Transform RatAttachmentSocket;
    public Predator LastPredator;

    public Collider collision;

    public AudioSource flappingAudioSource;
    public AudioClip BarrelRollAudioClip, SnowImpactAudioClip;

    public bool playedSound = false;

    protected override void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        animation.AddClip(Glide, Glide.name);
        animation.AddClip(Flapping, Flapping.name);
        animation.AddClip(Hit, Hit.name);
        animation.AddClip(Attack, Attack.name);
        animation.AddClip(BarrelRollAnimation, BarrelRollAnimation.name);

        animation[Glide.name].layer = GlideAnimationLayer;
        animation[Glide.name].speed = GlideAnimationSpeed;
        animation[Glide.name].weight = 1;
        animation[Glide.name].blendMode = AnimationBlendMode.Blend;
        animation[Glide.name].wrapMode = WrapMode.Loop;
        animation[Glide.name].enabled = true;

        animation[Flapping.name].layer = GlideAnimationLayer;
        animation[Flapping.name].speed = FlappingAnimationSpeed;
        animation[Flapping.name].wrapMode = WrapMode.Default;

        animation[Hit.name].layer = GlideAnimationLayer;
        animation[Hit.name].speed = FlappingAnimationSpeed;
        animation[Hit.name].wrapMode = WrapMode.Default;

        animation[Attack.name].layer = GlideAnimationLayer;
        animation[Attack.name].speed = FlappingAnimationSpeed;
        animation[Attack.name].wrapMode = WrapMode.Default;

        animation[BarrelRollAnimation.name].layer = GlideAnimationLayer;
        animation[BarrelRollAnimation.name].speed = 1;
        animation[BarrelRollAnimation.name].wrapMode = WrapMode.Default;

        NextTimeFlyingAnim = Time.time+2;
        animation.Play(Glide.name, PlayMode.StopAll);
    }
    

    private Predator Carried;
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            StartCharge();
            return;
        }
        if (Input.GetButtonDown("Fire2"))
        {
            StartBarrelRoll();
            return;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            return;
        }

        if(Time.time> NextTimeFlyingAnim)
        {
            NextTimeFlyingAnim = Time.time + TimeForFlyingMin + Random.Range(-RandomTimeForFlying, RandomTimeForFlying);
            animation.CrossFade(Flapping.name,0.05f);
            MEC.Timing.CallDelayed(0.12f, () => GlitterParticleSystem.Emit(35));
            animation.CrossFadeQueued(Glide.name);
            PlayFlappingSound();
        }
       // if(animation[Flapping.name].normalizedTime > 0.99f)
       //     animation.CrossFade(Glide.name);


        Vector3 axes = new Vector3(Input.GetAxisRaw("Horizontal"),0, Input.GetAxisRaw("Vertical"));
        axes = axes.normalized;
        
        

        newVelocity.x = axes.x * HorizontalSpeed;

        if (myTransform.localPosition.z > settings.MaxHorizontalMovement)
            newVelocity.x = -OutOfBoundsCounterVelocity;
        if (myTransform.localPosition.z < settings.MinHorizontalMovement)
            newVelocity.x = OutOfBoundsCounterVelocity;


        newVelocity.z = (axes.z * VerticalSpeedAccel) + MinSpeedVertical;
        Vector3 localRot = OwlRendererTransform.localEulerAngles;
        if (StunDuration > 0)
        {
            NextTimeFlyingAnim = Time.time + StunDuration;
            StunDuration -= Time.deltaTime;
            float evaluatedSpeed = StunSpeedMultiplierCurve.Evaluate(StunDuration);
            myRigidbody.velocity *= evaluatedSpeed;
            newVelocity *= evaluatedSpeed;
            inAttackSequence = false;
            OwlCamera.Instance.postDmgProcessVolume.weight = StunBloodPulseCurve.Evaluate(StunDuration);
        }

        if (inAttackSequence)
        {
            if (LastPredator != null && Vector3.Distance(LastPredator.myTransform.position, myTransform.position) > 100)
            {
                targetTrigger.Remove(LastPredator);
                LastPredator = null;
            }

            if (LastPredator != null)
            {
                bezierGroundPoint.transform.position = LastPredator.myTransform.position;
                if(animation[Attack.name].normalizedTime > 0.6f && animation[Attack.name].normalizedTime < 0.7f)
                {
                    Bonded.Instance.levelState.onPredatorKillVoiceLine.Play();

                    targetTrigger.Remove(LastPredator);
                    Carried = LastPredator.GetComponent<Predator>();
                    LastPredator = null;
                    Carried.enabled = false;
                    Carried.PlayDieClip();
                    DG.Tweening.DOTween.Kill(Carried);
                    Carried.myRigidbody.isKinematic = true;
                    Carried.myCollider.enabled = false;
                    Carried.myTransform.SetParent(RatAttachmentSocket,true);
                    Carried.myTransform.localPosition = Vector3.zero;
                    Carried.myTransform.localEulerAngles = Vector3.zero;
                    MEC.Timing.CallDelayed(0.3f, () =>
                     {
                         Carried.myTransform.parent = null;
                         Carried.myRigidbody.isKinematic = false;
                         Carried.myCollider.enabled = true;
                         Carried.myRigidbody.freezeRotation = false;
                         Carried.myRigidbody.useGravity = true;
                         Carried.myRigidbody.AddForceAtPosition((Vector3.up * 1.6f + myRigidbody.velocity) * 10,RatAttachmentSocket.position, ForceMode.VelocityChange);
              
                         Carried.death.enabled = true;
                     });
                }
            }

            // bezierGroundPoint.transform.position = lastAttackPoint;
            NextTimeFlyingAnim = Time.time + 0.2f;
            OwlBodyTransform.position = bezierSpline.GetPoint(SampleCurveBezier.Evaluate(animation[Attack.name].normalizedTime));

            if (OwlBodyTransform.position.y <= 2)
            {
                if (!playedSound)
                {
                    playedSound = true;
                    flappingAudioSource.PlayOneShot(SnowImpactAudioClip, 1);
                }
                
                SnowBurstParticlesOnHit.Emit(15);
            }

            newVelocity.x = 0;
            newVelocity.z = SampleCurveSpeedBezier.Evaluate(animation[Attack.name].normalizedTime) * MinSpeedVertical;

            //localRot.z = 0;
            OwlRendererTransform.localEulerAngles = new Vector3(-90,0,-90);
            localRot = OwlRendererTransform.localEulerAngles;
        }


        Vector3 counterVelocity = myRigidbody.velocity * SpeedDecreasment;
        counterVelocity *= Mathf.Min(axes.magnitude, (axes - newVelocity.normalized).magnitude);
        newVelocity -= counterVelocity;

        Vector3 localPos = OwlRendererTransform.localPosition;
        localPos.x = Mathf.Sin(Time.time * SinSpeed) * SinScale;
        OwlRendererTransform.localPosition = localPos;

        
        localRot.z = (myRigidbody.velocity.x * LocalRotScale);
        OwlRendererTransform.localEulerAngles = localRot;
    }

    public float SinScale = 2;
    public float SinSpeed = 2;

    public float LocalRotScale = 2;

    private void FixedUpdate()
    {
        myRigidbody.AddForce(newVelocity, ForceMode.Acceleration);
    }

    public float AttackDuration = 2;
    public float FlyUpAgainDuration = 1;
    public PathType pathType = PathType.CatmullRom;
    private void StartCharge()
    {
        if (inAttackSequence || isInBarrelRoll)
            return;
        playedSound = false;
        collision.enabled = false;
        inAttackSequence = true;
        lastAttackPoint = targetTrigger.getRandomPointInArea(out LastPredator);

        bezierGroundPoint.transform.position = lastAttackPoint;
        animation.CrossFade(Attack.name, 0.05f);

        animation.CrossFadeQueued(Flapping.name, 0.3f);
        MEC.Timing.CallDelayed(animation[Attack.name].length - 0.4f, PlayFlappingSound);
        MEC.Timing.CallDelayed(animation[Attack.name].length, () => { inAttackSequence = false; collision.enabled = true; PlayFlappingSound(); });
    }


    private bool isInBarrelRoll = false;

    private void StartBarrelRoll()
    {
        if (isInBarrelRoll ||inAttackSequence)
            return;
        flappingAudioSource.pitch = Random.Range(0.95f, 1.05f);
        flappingAudioSource.PlayOneShot(BarrelRollAudioClip, 0.7f);
        isInBarrelRoll = true;
        collision.enabled = false;
        animation.CrossFade(BarrelRollAnimation.name, 0.05f);
        MEC.Timing.CallDelayed(animation[BarrelRollAnimation.name].length, PlayFlappingSound);
        animation.CrossFadeQueued(Flapping.name,0.05f);
        NextTimeFlyingAnim = Time.time + animation[BarrelRollAnimation.name].length;
        MEC.Timing.CallDelayed(animation[BarrelRollAnimation.name].length, () =>
         {
             isInBarrelRoll = false;
             collision.enabled = true;
         });
    }

    public void SetStunTime(float time)
    {
        StunDuration = time;
        GlitterParticleSystem.Emit(40);
        animation.CrossFade(Hit.name, 0.05f);

        animation.CrossFadeQueued(Flapping.name,0.1f);
        MEC.Timing.CallDelayed(animation[Hit.name].length, PlayFlappingSound);
        NextTimeFlyingAnim = Time.time + animation[Flapping.name].length;
    }


    void PlayFlappingSound()
    {
        flappingAudioSource.pitch = Random.Range(0.65f, 0.78f);
        flappingAudioSource.Play(4100);
    }
}
