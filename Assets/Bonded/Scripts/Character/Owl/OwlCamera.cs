﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class OwlCamera : SingletonMonoBehaviour<OwlCamera>
{
    public PostProcessVolume postDmgProcessVolume;
    public ScannerEffectDemo scanner;
    // public Pos
    private EnviroSky enviro;
    private void Start()
    {
        enviro = FindObjectOfType<EnviroSky>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.KeypadPlus))
            enviro.SetTime(enviro.GameTime.Years, enviro.GameTime.Days, enviro.GameTime.Hours, enviro.GameTime.Minutes+(int)(300f*Time.deltaTime), enviro.GameTime.Seconds);
        else if (Input.GetKey(KeyCode.KeypadMinus))
            enviro.SetTime(enviro.GameTime.Years, enviro.GameTime.Days, enviro.GameTime.Hours , enviro.GameTime.Minutes-(int)(300f*Time.deltaTime), enviro.GameTime.Seconds);
    }
}
