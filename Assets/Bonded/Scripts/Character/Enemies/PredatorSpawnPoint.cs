﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorSpawnPoint : SingletonMonoBehaviour<PredatorSpawnPoint>
{
    public Transform SpawnArea;

    public AnimationCurve SpawnAmountWaitMultiplierCurve = new AnimationCurve(new Keyframe(1, 1), new Keyframe(1, 1));
    public Vector2 WaitMinMax = new Vector2(8, 12);

    public void Start()
    {
        float WaitTime = Random.Range(WaitMinMax.x, WaitMinMax.y);
        WaitTime *= SpawnAmountWaitMultiplierCurve.Evaluate(Bonded.Instance.levelState.NormalizedTime);
        MEC.Timing.CallDelayed(WaitTime, SpawnEnemy);
        

        
    }

    public void SpawnEnemy()
    {
        if (Bonded.Instance.levelState.isWon)
            return;

        if (this == null)
            return;
        GameObject prefabPredator = Bonded.Instance.settings.Predators[Random.Range(0, Bonded.Instance.settings.Predators.Length)];

        GameObject go = GameObject.Instantiate(prefabPredator,PickRandomSpawnPoint(),Quaternion.identity, Bonded.Instance.gameInstance.transform);
        go.transform.localScale = Vector3.one * Random.Range(0.9f, 1.4f);

        go.GetComponent<Predator>().Speed *= ((Bonded.Instance.levelState.NormalizedTime*0.2f)+1);

        float WaitTime = Random.Range(WaitMinMax.x, WaitMinMax.y);
        WaitTime *= SpawnAmountWaitMultiplierCurve.Evaluate(Bonded.Instance.levelState.NormalizedTime);
        MEC.Timing.CallDelayed(WaitTime, SpawnEnemy);
    }

    public Vector3 PickRandomSpawnPoint()
    {
        Vector3 rndPosWithin = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        rndPosWithin = SpawnArea.TransformPoint(rndPosWithin * 0.5f);

        return rndPosWithin;
    }

}
