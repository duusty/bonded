﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Predator : BaseObject
{
    public const int GlideAnimationLayer = 0;

    public static BetterList<Predator> predators = new BetterList<Predator>();
    public static HashSet<Predator> predatorsHashset = new HashSet<Predator>();

    public float Speed = 20;
    public float SinSpeedScale = 5;
    private float SinSpeed = 3;
    public float SinSpeedMax = 6;
    public float SInSpeedMin = 2;
    public Vector3 WorldUp = new Vector3(1, 0, 0);

    public AnimationClip Run, Attack;
    public Animation animation;
    public ParticleSystem footStepsParticles;

    public AudioClip[] RatAttackSounds;
    public AudioClip DieAudioClip;
    public AudioSource audioSource;

    public bool isCharging = false;

    public float ChargeDistance = 2;
    public float ChargeDuration = 0.5f;

    public bool CanCharge = true;

    public float ChargeCoolDown = 1f;

    public float AttackDistance = 0.5f;
    public float ChargeDistanceForce = 6;

    public PredatorDeath death;
    public Rigidbody myRigidbody;
    public Collider myCollider;
    private void Start()
    {
        SinSpeed = Random.Range(SInSpeedMin, SinSpeedMax);

        animation.AddClip(Attack, Attack.name);
        animation.AddClip(Run, Run.name);

        animation[Run.name].layer = GlideAnimationLayer;
        animation[Run.name].speed = 1;
        animation[Run.name].weight = 1;
        animation[Run.name].blendMode = AnimationBlendMode.Blend;
        animation[Run.name].wrapMode = WrapMode.Loop;
        animation[Run.name].enabled = true;

        animation[Attack.name].layer = GlideAnimationLayer;
        animation[Attack.name].speed = 1;
        animation[Attack.name].wrapMode = WrapMode.Once;

        animation.Play(Run.name, PlayMode.StopAll);
        footStepsParticles.Play();
    }

    private void OnEnable()
    {
        if (predatorsHashset.Add(this))
            predators.Add(this);
    }

    private void OnDisable()
    {
        if (predatorsHashset.Remove(this))
            predators.Remove(this);
        footStepsParticles.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        
    }

    

    private void Update()
    {
        if (isCharging)
        {
            if (AttackDistance > Vector3.Distance(Chick.Instance.myTransform.position, myTransform.position))
            {
                Chick.Instance.TakeDamage();
                PlayAttackSound();
                isCharging = false;
            }
            return;
        }
        else if (CanCharge && ChargeDistance > Vector3.Distance(Chick.Instance.myTransform.position, myTransform.position))
        {
            Charge();
        }
        else
        {
            Speed += Time.deltaTime * 0.1f;
            myTransform.LookAt(Chick.Instance.myTransform.position + (Vector3.right * Mathf.Sin(Time.time * SinSpeed*0.4f) * SinSpeedScale*1.5f), WorldUp);
            myTransform.position += myTransform.forward * (Speed + Mathf.Sin(Time.time * SinSpeed) * SinSpeedScale) * Time.deltaTime;
        }
    }

    void Charge()
    {
        if (isCharging || !CanCharge)
            return;
        isCharging = true;
        CanCharge = false;
        animation.CrossFade(Attack.name);
        animation.CrossFadeQueued(Run.name);
        myTransform.DOMove(myTransform.position + myTransform.forward* ChargeDistanceForce, ChargeDuration).OnComplete(() =>
        {
            MEC.Timing.CallDelayed(ChargeCoolDown, () => CanCharge = true);
            isCharging = false;
        }).SetId(this);
    }

    public void InstaKill()
    {
        enabled = false;
        death.Instakill();
    }

    public void PlayDieClip()
    {
        audioSource.pitch = Random.Range(0.92f, 1.08f);
        audioSource.PlayOneShot(DieAudioClip, 0.4f);
    }

    public void PlayAttackSound()
    {
        audioSource.pitch = Random.Range(0.92f, 1.08f);
        audioSource.PlayOneShot(RatAttackSounds[Random.Range(0, RatAttackSounds.Length)],1);
    }
}
