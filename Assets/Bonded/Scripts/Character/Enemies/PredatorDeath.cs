﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorDeath : BaseObject
{
    public ParticleSystem particleSystem;

    // Update is called once per frame
    void Update()
    {
        if(myTransform.position.y <= 1.6f)
        {
            Instakill();
        }
    }

    public void Instakill()
    {
        particleSystem.Play();
        this.enabled = false;
        Destroy(myGameObject, 2);
    }
}
