﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class ShaderGlobals : ScriptableObject
{
    public Texture2D groundTexture;
    public Texture2D noiseTexture;
    public Vector2 groundTextureScale = new Vector2(1, 1);
    public Vector2 noiseTextureScale = new Vector2(1, 1);

    [ContextMenu("Apply Shader Globals")]
    public void Apply()
    {
        Shader.SetGlobalTexture("_NoiseTexture", noiseTexture);
        Shader.SetGlobalTexture("_GroundTexture", groundTexture);
        Shader.SetGlobalVector("_TextureScale",
            new Vector4(groundTextureScale.x, groundTextureScale.y,
            noiseTextureScale.x, noiseTextureScale.y));
    }

#if UNITY_EDITOR
    [InitializeOnLoadMethod]
    static void Init()
    {
        EditorApplication.update += ApplyProperties;
    }

    static ShaderGlobals instance;
    static void ApplyProperties()
    {
        if (instance == null)
        {
            instance = AssetDatabase.LoadAssetAtPath<ShaderGlobals>("Assets/Bonded/ShaderGlobals.asset");
        }

        if (instance != null)
        {
            instance.Apply();
        }
    }
#endif
}
